<?php if(count($errors) >0): ?>

   <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
     <div class="alert alert-danger  alert-dismissable fade in">
      <?php echo e($error); ?>

    </div>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   
<?php endif; ?>

<?php if(session('messages')): ?>
  <div class="alert alert-success  alert-dismissable fade in">
      <?php echo e(session('messages')); ?>

  </div>
<?php endif; ?>

<?php $__env->startSection('content'); ?>

  <div class="overview contentContainer card" >

      <ul class="nav nav-tabs" >
      <li
          <?php if($view_id ==0): ?>
             class="active"
          <?php endif; ?>
      ><a href="0">Overview</a></li>
      <li
          <?php if($view_id ==1): ?>
             class="active"
          <?php endif; ?>
      ><a href="1">Summary Keaktifan</a></li>
      <li
          <?php if($view_id ==2): ?>
             class="active"
          <?php endif; ?>
      ><a href="2">Detil Jadwal</a></li>
    </ul>

   <div class="overview">
     <div style="width:100%;margin-top: 20px;"> </div>
   	<?php if($view_id == 0): ?>


    <table class="tableData">
         <tr>
        <th>Kelas</th><th>Jumlah Peserta </th><th>Total Point </th><th>Persentase keakftifan </th>
      </tr>
    <?php $__currentLoopData = $report; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

      <tr>
        <td class="table_cell">
          <?php echo e($entry->SubjectName . " " . $entry->ClassNumber); ?>

        </td>
        <td class="table_cell">
          <?php echo e($entry->JmlUser); ?>

        </td>
        <td class="table_smaller_cell">
          <?php echo e($entry->engagementValue); ?>

        </td>
        <td class="table_smaller_cell">
          <?php echo e(($entry->engagementValue != 0)?$entry->engagementValue/$entry->JmlUser : 0); ?>

        </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>

   <?php elseif($view_id == 1): ?>

       <form class='form-inline' method="get" action="1" align="right">
        <label>filter Keaktifan :</label>
            <select class= 'form-control' name="select_kehadiran_class_2"  id="select_kehadiran_class_2" >
              <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if( $chosen_class == $entry->Id ): ?>
                 <option value="<?php echo e($entry->Id); ?>" selected ><?php echo e($entry->SubjectName ."-". $entry->ClassNumber); ?> </option>
                <?php else: ?>
                <option value="<?php echo e($entry->Id); ?>"><?php echo e($entry->SubjectName ."-". $entry->ClassNumber); ?> </option>
                <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          <input type=submit value='Filter' class='form-control form-control-sm'>
      </form>

      <table class="tableData">
      <tr>
          <th>Mata Kuliah</th><th>Kelas</th><th>NRP</th><th>NaMA</th><th>point Keaktifan</th>
        </tr>
        <?php $__currentLoopData = $report_sum; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
          <td class="table_cell">
            <?php echo e($entry->SubjectName); ?>

          </td>
          <td class="table_cell">
            <?php echo e($entry->ClassNumber); ?>

          </td>
          <td class="table_cell">
            <?php echo e($entry->UserId); ?>

          </td>
          <td class="table_cell">
            <?php echo e($entry->Name); ?>

          </td>
          <td class="table_smaller_cell">
            <?php echo e($entry->total_engagement); ?>

          </td>

        </tr>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
     </table>

     <?php if($report_sum != null): ?>
      <?php echo e($report_sum->appends($_GET)->links()); ?>

    <?php endif; ?>

   <?php elseif($view_id == 2): ?>

   		 <form class='form-inline' method="get" action="2" align="right">
        <label>filter Keaktifan :</label>
            <select class= 'form-control' name="select_kehadiran_class"  id="select_kehadiran_class" >
              <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if( $chosen_class == $entry->Id ): ?>
                  <option value="<?php echo e($entry->Id); ?>" selected ><?php echo e($entry->SubjectName ."-". $entry->ClassNumber); ?> </option>
                <?php else: ?>
                  <option value="<?php echo e($entry->Id); ?>"><?php echo e($entry->SubjectName ."-". $entry->ClassNumber); ?> </option>
                <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          <input type=submit value='Filter' class='form-control form-control-sm'>
      </form>

       <table class="tableData">
         <tr>
          <th>Kelas</th><th>Nama </th><th>Point </th><th>Persentase keakftifan </th>
        </tr>
        <?php $__currentLoopData = $report_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
          <td class="table_cell">
            <?php echo e($entry->SubjectName . " " . $entry->ClassNumber); ?>

          </td>
          <td class="table_cell">
            <?php echo e($entry->UserId . " " . $entry->Name); ?>

          </td>
          <td class="table_smaller_cell">
            <?php echo e($entry->engagement_name); ?>

          </td>
          <td class="table_smaller_cell">
            <button class="btn" onclick="DeleteEngagement(<?php echo e($entry->IdEng); ?>)"> Delete </button>
          </td>
        </tr>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>

    <?php if($report_detail != null): ?>
      <?php echo e($report_detail->appends($_GET)->links()); ?>

    <?php endif; ?>

   	<?php endif; ?>

   </div>

  </div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
  <div class="well">
    <h4> Engagement Information : </h4>
      <form action="../../Engagements/InsertService" id="engagementForm" method='POST'>
        <label>Class : </label>
      <br>
      <select class="form-control selectpicker" name="select_engagement_classes" id="select_engagement_classes"  data-live-search="true">
        <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

          <?php if($entry->Id == $chosen_class): ?>
            <option value="<?php echo e($entry->Id); ?>" selected ><?php echo e($entry->Lecturer . " " . $entry->Subjects . " " .$entry->ClassNumber); ?></option>
          <?php else: ?>
            <option value="<?php echo e($entry->Id); ?>"><?php echo e($entry->Lecturer . " " . $entry->Subjects . " " .$entry->ClassNumber); ?></option>
          <?php endif; ?>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </select>
      <br>
      <label>Schedule :</label>

    <div class="container" style="width:100%;margin:0px;padding:0px;">
      <div class="col-md-7" style="margin:0px;padding:0px;">
        <select class="form-control selectpicker" name="select_engagement_schedule" id="select_engagement_schedule" data-live-search="true">
          <option value =-1> Please select a class </option>
        </select>
      </div>
      <div class="col-md-1" style="margin:0px;padding:0px;">
        &nbsp;
      </div>
      <div class="col-md-4" style="margin:0px;padding:0px;">
        <input type=button class="form-control button btn-primary" value="add new" />
      </div>
      <br>
    </div>

    <label>Student Id : </label>
    <select class="form-control selectpicker" name="select_engagement_user" id="select_engagement_user" data-live-search="true">
      <option value = -1> please select a class </option>
    </select>
    <br>

    <script>
      $(".selectpicker").select2();
    </script>

    <br>
    <input type="hidden" name="_token"  value="<?php echo e(csrf_token()); ?>">
    <input type="hidden" name="_id_eng" id="_id_eng" value="0">


  </form>

    <div class="container" style="width:100%;padding:0px;margin:auto;">
      <div class="col-md-3 col-xs-3" style="padding:0px; width:20%; margin:0 5;">
        <button class="button form-control btn-success" id="btnRight"> Right </button>
      </div>
      <div class="col-md-3 col-xs-3" style="padding:0px;width:20%;margin:0 5;">
        <button class="button form-control btn-danger" id="btnWrong">Wrong</button>
      </div>
      <div class="col-md-3 col-xs-3" style="padding:0px;width:20%;margin:0 5;">
        <button class="button form-control btn-info" id="btnAsk">Ask</button>
      </div>
      <div class="col-md-3 col-xs-3" style="padding:0px;width:20%;margin:0 5;">
        <button class="button form-control btn-warning" id="btnAct">Act</button>
      </div>
    </div>
  </div>

   </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('extra_script'); ?>
  <link rel="stylesheet" href="<?php echo e(URL::to('css/common.css')); ?>" />
  <script src="<?php echo e(URL::to('js/lecturer_keaktifan.js')); ?>" > </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('lecturer_main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
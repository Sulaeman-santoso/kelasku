

<?php $__env->startSection('content'); ?>
  <form action="Classes" method="get" class='form-inline' align="right">
    <!--<input type=checkbox class="form-check-input" id='check_active' >
    <label class='form-check-label' for="check_active" >active only </label>-->
    Filter :
    <select name='select_classes_semester' class='form-control'>
      <option value=0>All </option>
      <?php $__currentLoopData = $semesters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value=<?php echo e($names->Id); ?>  <?php echo e(($old->input('select_classes_semester') == $names->Id)?'Selected':''); ?> > <?php echo e($names->semestername->Name ."-" . $names->Year."/". ($names->Year+1)); ?>  </option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
	<select name='select_classes_class' class='form-control'>
      <option value=0>All </option>
      <?php $__currentLoopData = $lecturers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dosen): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value=<?php echo e($dosen->Id); ?>  <?php echo e(($old->input('select_classes_class') == $dosen->Id)?'Selected':''); ?>   > <?php echo e($dosen->Name); ?>  </option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <input type=submit value=Filter class='form-control form-control-sm'>
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Lesson</th>
      <th>Lecturer</th>
      <th>Class Number</th>

      <th></th>
    </tr>

  <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>

       <td><?php echo e($entry->Id); ?></td>
       <td><?php echo e($entry->lesson->Subject->SubjectName ."-". $entry->lesson->semester->semestername->Name ." ". $entry->lesson->semester->Year . "/". ($entry->lesson->semester->Year + 1)); ?></td>
       <td><?php echo e($entry->lecturer->Name); ?></td>
       <td> <?php echo e($entry->ClassNumber); ?> </td>

       <td>
         <form action="Classes/Delete/<?php echo e($entry->Id); ?>" method='POST'>
           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 </table>
 <?php echo e($classes->appends($_GET)->links()); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
   <div class="well">
	  <h4> Class Information : </h4>
      <form action="Classes\Insert", method='POST'>
        <label>Semester : </label>
        <select name='select_classes_lesson' class='form-control' >
            <?php $__currentLoopData = $lessons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value=<?php echo e($entry->Id); ?>> <?php echo e($entry->semester->semestername->Name ."-" .$entry->semester->Year . "/" . ($entry->semester->Year+1) . " ". $entry->subject->SubjectName); ?>  </option>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <label>Subject : </label>
        <select name='select_classes_lecturer' class='form-control' >
            <?php $__currentLoopData = $lecturers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value=<?php echo e($entry->Id); ?>> <?php echo e($entry->Name); ?>   </option>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <label>Class Number</label>
        <input type=text class='form-control' name='input_class_classNumber'>

        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <br>
        <input type=submit value="Insert New Lesson" class="button form-control">
    </form>
   </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
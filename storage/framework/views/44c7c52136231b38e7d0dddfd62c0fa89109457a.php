

<?php $__env->startSection('content'); ?>

  <div id="vue_content">
    <!--Menu-->
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#kelas">Kelas</a></li>
      <li><a data-toggle="tab" href="#student">Murid</a></li>
      <li><a data-toggle="tab" href="#menu2">Lainnya</a></li>
    </ul>

    <!-- Content -->
    <div class="tab-content">
      <div id="kelas" class="tab-pane fade in active">
        <lreport :mydata="<?php echo e(json_encode($classes)); ?>" :chosen_option="<?php echo e($chosen_option); ?>" ></lreport>
      </div>
      <div id="student" class="tab-pane fade">
        <p>Some content in menu 1.</p>
      </div>
      <div id="menu2" class="tab-pane fade">
        <h3>Menu 2</h3>
        <p>Some content in menu 2.</p>
      </div>
    </div>

  </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
  <div id="vue_sidebar" >

    <div class="well">
      <import ></import>
    </div>
    <div class=well>

    </div>
  </div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('extra_script'); ?>
  <link rel="stylesheet" href="<?php echo e(URL::to('css/common.css')); ?>" />
  <script type="module" src="<?php echo e(URL::to('js/lecturer_setting.js')); ?>" > </script>
  <style>
    #vue_content {
      background: white;
      border: 2px solid black;
      padding : 15px;
    }
  </style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('lecturer_main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
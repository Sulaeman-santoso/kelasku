

<?php $__env->startSection('content'); ?>
  <form action="Users" method="get" class="form-inline" align="right">
	<label>Filter : </label>
   <select name="select_role_filter" class="form-control form-control-sm">
        <option value=0> All </option>
      <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value=<?php echo e($entry->Id); ?> > <?php echo e($entry->roles->Name); ?>  </option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
	<input type=submit value=Filter class='form-control form-control-sm'>
	
    <input type=text name="input_user_search" class='form=control' placeholder="NRP atau Nama" align="right">
    <input type=submit value='Search' class='form-control' align="right">
	
	
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>User Id</th>
      <th>Name</th>

      <th></th>
      <th></th>
    </tr>

  <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>

       <td><?php echo e($entry->Id); ?></td>
       <td> <?php echo e($entry->UserId); ?> </td>
       <td><?php echo e($entry->Name); ?></td>
       <td><?php echo e($entry->roles->Name); ?></td>



       <td>
         <form action="Users/Delete/<?php echo e($entry->Id); ?>" method='POST'>
           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 </table>
 <?php echo e($users->links()); ?>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
   <div class="well">
	  <h4> User Informations : </h4>
      <form action="Users\Insert", method='POST'>
        <label>User Id : </label>
        <input type=text name='input_user_id' class='form-control form-control-sm'>

    	  <label>User Name : </label>
    		<input type=text name='input_user_name' class='form-control form-control-sm'>

    		<label>Password : </label>
    		<input type=password name='input_user_pass' class='form-control form-control-sm'>

		<label>Role : </label>
        <select class="form-control form-control-sm" name="select_user_role" >
          <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($item->Id); ?>" ><?php echo e($item->Name); ?> </option>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <br>
        <input type=submit value="Insert New User" class="button form-control form-control-sm">
      </form> 

      Bulk Insert :
	  <br>
		format :  nrp / name / password	/ roles (3 untuk student) 
	  <br>
	<form action='Users/Import' method='POST' enctype="multipart/form-data">
		<input type=file name='input_file_excel' class='form-control-file form-control-sm'>
	    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

		<input type=submit value='import user from excel' class='form-control form-control-sm'>
	  </form>
   </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('/main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
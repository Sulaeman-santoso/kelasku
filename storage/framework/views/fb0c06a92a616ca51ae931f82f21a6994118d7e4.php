<?php
  session_start();
  if(!isset($_SESSION['authenticated'])) {
    header("Location: /index.php");
    exit;
}
?>
<html>
 <head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KelasKu	</title>
	 <link rel="shortcut icon" href="<?php echo e(URL::to('images/favicon.ico')); ?>" />

	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

   <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script src=" <?php echo e(URL::to('js/image-picker.min.js')); ?> ">  </script>
    <link rel="stylesheet" href="<?php echo e(URL::to('css/image-picker.css')); ?>" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <style type="text/css">

		div#header {
			width:100%;
		}

		ul#nav-top {
			width:100%;

			float:none;
			margin:0 auto;
			display: block;
			text-align: center
		}

		.navbar-nav > li {
			display: inline-block;
			float:none;
		}

	   .thumbnails li img{
		   width: 100px;
	   }

		tr:nth-child(even){
			background-color:#e8e8e8;
		}
		tr:nth-child(odd){
			background-color:white;
		}


    </style>

    <?php echo $__env->yieldContent('extra_script'); ?>
 </head>

 <body>
	<div style="width:100%;margin:0 auto;text-align:center;">
   <?php if(Session::get('lecturer') == 'Yes'): ?>
     <?php echo $__env->make('inc.navtop_lecturer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php elseif(Session::get('student') == 'Yes'): ?>
    <?php echo $__env->make('inc.navtop_student', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php else: ?>
    <?php echo $__env->make('inc.navtop2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endif; ?>
	</div>


   <div class="container">
     <div class="col-lg-8 col-md-8">
       <?php echo $__env->make('inc.messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       <?php echo $__env->yieldContent('content'); ?>
     </div>
     <div class="col-lg-4 col-md-4">
       <?php echo $__env->yieldContent('sidebar'); ?>
     </div>
   </div>
   <?php echo $__env->make('inc.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   <?php echo $__env->yieldContent('modals'); ?>
 </body>

</html>



<?php $__env->startSection('content'); ?>
  <form action='Enrollments' method="get" class='form-inline' align="right">
    Filter :
	<select name="input_semester_filter" class='form-control form-control-sm' style="display:none;">
      <option value=0 >All </option>
      <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value=<?php echo e($names->Id); ?>> <?php echo e($names->lesson->semester->Year ."/". ($names->lesson->semester->Year+1)); ?>  </option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <select name="input_enrollment_filter" class='form-control form-control-sm'>
      <option value=0 >All </option>
      <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value=<?php echo e($names->Id); ?>> <?php echo e($names->lesson->subject->SubjectName . "-". $names->ClassNumber . "-" . $names->lesson->semester->Year ."/" . ($names->lesson->semester->Year + 1)); ?>  </option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <input type=submit value=Filter  class='form-control form-control-sm'>
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Class Name</th>
	    <th>Student Name</th>
     <th></th>
    </tr>

  <?php $__currentLoopData = $enrollments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
       <td><?php echo e($entry->Id); ?></td>
       <td><?php echo e($entry->classes->lesson->subject->SubjectName . "-" . 
        $entry->classes->lesson->semester->Year ."/". ($entry->classes->lesson->semester->Year+1) ." ".
        $entry->classes->ClassNumber); ?></td>
	     <td><?php echo e($entry->student->Name); ?></td>
       <td>
         <form action="Enrollments/Delete/<?php echo e($entry->Id); ?>" method='POST'>
           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 </table>
 <?php echo e($enrollments->appends($_GET)->links()); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
   <div class="well">
	  <h4> Enrollment Information : </h4>
      <form action="Enrollments\Insert", method='POST'>
        <label>Classes Name : </label>
		<select name='select_enrollment_class' class='form-control' >
		    <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			  <option value=<?php echo e($names->Id); ?>> <?php echo e($names->lesson->semester->semestername->Name . "-". 
          $names->lesson->semester->Year . "/". ($names->lesson->semester->Year+1). 
         "-". $names->lesson->subject->SubjectName . "-". $names->ClassNumber); ?>  </option>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</select>
        <label>Student Name : </label>
       <select name='select_enrollment_student' class='form-control' >
		    <?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			  <option value=<?php echo e($names->Id); ?>> <?php echo e($names->Name); ?>  </option>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</select>
		<br>
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <input type=submit value="Insert New enrollment" class="button form-control">
    </form>

    <form action='Enrollments/Import' method="post" enctype='multipart/form-data' >
	  <div style=" box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding:10px; border:1px solid black;">
      <label>Import Enrollment</label>
	  <br>
	    <span >format : nrp /	id_class</span>
	  <br>
	  <select name='select_enrollment_class_import' class='form-control' style="margin-bottom:10px;display:none;">
		    <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			  <option value=<?php echo e($names->Id); ?>> <?php echo e($names->lesson->subject->SubjectName . "-". $names->ClassNumber); ?>  </option>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</select>
	  <br>
	  <input type='file' class="form-control-file form-control-sm" name="input_file_excel" > <br>

	  
      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
      <input type='submit' class='form-control form-control-sm' value='Submit Import' >
	  </div>
	</form>
	
	
	<form action='Enrollments/BulkImport' method="post" enctype='multipart/form-data' >
	  <div style=" box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); padding:10px; border:1px solid black;">
      <label>Bulk Import Data</label>

	  <select name='select_enrollment_semester' class='form-control selectpicker' style="margin-bottom:10px;display:block;">
			<?php $__currentLoopData = $Semester; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			  <option value=<?php echo e($names->Id); ?>> <?php echo e($names->semestername->Name . "-" . $names->Year . "/" . ($names->Year+1)); ?>  </option>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</select>
	  <?php $__currentLoopData = $Semester; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
	  
	  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<br>
	  <input type='file' class="form-control-file form-control-sm" name="input_bulk_import" > <br>

	  
      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
      <input type='submit' class='form-control form-control-sm' value='Submit Import' >
	  </div>
	</form>
	
	
   </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
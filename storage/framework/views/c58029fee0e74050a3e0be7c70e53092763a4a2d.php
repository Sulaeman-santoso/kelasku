

<?php $__env->startSection('content'); ?>

  <h1>Selamat datang  </h1>
  <div>
    Halaman ini adalah halaman awal dari web untuk pengajar

  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
  <div class="well">
   <h4> Info Pengajar :  <?php echo e($users->Name); ?> </h4>
   <h5>Kelas yang diajar semester ini : </h5> 

	<ul style="list-style-type: none;
  margin: 0;
  padding: 0;"> 
   	<?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
   		<li><?php echo e($entry->SubjectName ."-". $entry->ClassNumber); ?> </li>
   	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
   	</ul>

  </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('extra_script'); ?> 
  <link rel="stylesheet" href="<?php echo e(URL::to('css/common.css')); ?>" />
<?php $__env->stopSection(); ?>

<?php echo $__env->make('lecturer_main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
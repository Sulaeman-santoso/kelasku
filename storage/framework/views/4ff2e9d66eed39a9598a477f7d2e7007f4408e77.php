

<?php $__env->startSection('content'); ?>
  <form action='SchedulePhotos' method="get" class="form-inline" align="right">
    <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
    <label>Filter : </label>
	<select name="select_class_filter" id="select_class_filter" class="form-control form-control-sm">
        <option value=0> All </option>
        <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <?php if(empty($chosen_class)): ?>
              <option value='<?php echo e($entry->Id); ?>'> <?php echo e($entry->SubjectName . "-"  . $entry->ClassNumber); ?> </option>
            <?php else: ?>
              <?php if($entry->Id == $chosen_class): ?>
                 <option value='<?php echo e($entry->Id); ?>' selected> <?php echo e($entry->SubjectName . "-"  . $entry->ClassNumber); ?> </option>
              <?php else: ?>
                 <option value='<?php echo e($entry->Id); ?>'> <?php echo e($entry->SubjectName . "-"  . $entry->ClassNumber); ?> </option>
              <?php endif; ?>
            <?php endif; ?>
        
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <select name="select_schedule_filter"  id="select_schedule_filter" class="form-control form-control-sm">
       <option value=0> All Schedule </option>
        <?php $__currentLoopData = $schedules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if(empty($chosen_class)): ?>
            <?php else: ?>
              <?php if($entry->Id_Class == $chosen_class): ?>
                 <option value='<?php echo e($entry->Id); ?>' > <?php echo e($entry->scheduleDate); ?> </option>
              <?php else: ?>
             
              <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <input type=submit value=Filter class='form-control form-control-sm'>
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Schedule</th>

      <th></th>
      <th></th>
    </tr>

  <?php $__currentLoopData = $schedulePhotos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>

       <td><?php echo e($entry->Id); ?></td>
       <td><?php echo e($entry->schedule->classes->lesson->Subject->SubjectName   . " " .
        $entry->schedule->classes->ClassNumber . " -  " .  
        $entry->schedule->classes->lesson->semester->Year . "/" . ($entry->schedule->classes->lesson->semester->Year +1) . " " .  date_format(date_create($entry->schedule->scheduleDate),'d-m-Y H:i:s') . " "); ?></td>

       <td>
        <form action="SchedulePhotos/ShowPhoto/<?php echo e($entry->Id_Photo); ?>" method='POST'>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
          <input type=submit value='Show' >
        </form>
       </td>
       <td>
         <form action="SchedulePhotos/Delete/<?php echo e($entry->Id); ?>" method='POST'>
           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 </table>
 <?php echo e($schedulePhotos->appends($_GET)->links()); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
   <div class="well">
	  <h4> Schedule Photo Information : </h4>
      <form action="SchedulePhotos\Insert", method='POST' enctype="multipart/form-data">
        <label> Class </label>
        <select name='select_class_name' id='select_class_name' class='form-control' onchange="filter(this.value)">
          <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value='<?php echo e($entry->Id); ?>'> <?php echo e($entry->SubjectName . "-" . $entry->ClassNumber); ?> </option>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <label>Schedule </label>
        <select name='select_schedule_name' id='select_schedule_name' class='form-control' >
          <?php $__currentLoopData = $schedules; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value='<?php echo e($entry->Id); ?>'  ><?php echo e(date_format(date_create($entry->scheduleDate),'d-m-Y H:i:s')); ?></option>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>
          function filter(value) {
            var id = $("#select_class_name").find(":selected").val();
            var tokens = '<?php echo e(csrf_token()); ?>';
            //arrghh.. isn't there a beter way to do this ?
            $.post('Schedules/Filter/'+id,{
              Id : id,
              _token : tokens
            },function(data,status){
               // alert(data);
                $('#select_schedule_name').html(data);
            });
          }
        </script>
        <label>Photo Name : </label>

        <?php
          echo Form::file('input_schedulePhotos_photos');

         ?>
      <!--  <input type=file name='input_schedulePhotos_photos' class='form-control'>-->
       <br>
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <br>
        <input type=submit value="Insert New Photos" class="button form-control">
    </form>
   </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('extra_script'); ?> 
  
  <script src="<?php echo e(URL::to('js/schedule_photo_view.js')); ?>" > </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
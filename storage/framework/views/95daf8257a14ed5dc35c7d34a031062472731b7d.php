

<?php $__env->startSection('content'); ?>
  <form action='Lessons' method="get" class="form-inline" align="right">
    <label>Filter : </label>
    <select name="select_lesson_filter" class="form-control form-control-sm">
      <option value=0> All </option>
      <?php $__currentLoopData = $Semesters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value=<?php echo e($names->Id); ?>> <?php echo e($names->semestername->Name ."-" . $names->Year); ?>  </option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <input type=submit value=Filter class='form-control form-control-sm'>
  </form>
  
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Semester</th>
      <th>Subject</th>

      <th></th>
    </tr>

  <?php $__currentLoopData = $Lessons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>

       <td><?php echo e($entry->Id); ?></td>
       <td><?php echo e($entry->semester->semestername->Name . "-" . $entry->semester->Year . "/" .
            ($entry->semester->Year +1 )); ?></td>
       <td><?php echo e($entry->subject->SubjectName); ?></td>

     <td>
         <form action="Lessons/Delete/<?php echo e($entry->Id); ?>" method='POST'>
           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 </table>
 <?php echo e($Lessons->appends($_GET)->links()); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
   <div class="well">
	  <h4> Lesson Information : </h4>
      <form action="Lessons\Insert", method='POST'>
        <label>Semester : </label>
        <select name='select_lesson_semester' class='form-control' >
            <?php $__currentLoopData = $Semesters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value=<?php echo e($names->Id); ?>> <?php echo e($names->semestername->Name ."-" . $names->Year); ?>  </option>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <label>Subject : </label>
        <select name='select_lesson_subject' class='form-control' >
            <?php $__currentLoopData = $Subjects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value=<?php echo e($entry->Id); ?>> <?php echo e($entry->SubjectName); ?>   </option>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>

        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <br>
        <input type=submit value="Insert New Lesson" class="button form-control">
    </form>
   </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
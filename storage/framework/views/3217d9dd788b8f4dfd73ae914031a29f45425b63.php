

<?php $__env->startSection('content'); ?>
  <form action='Semesters' method="get" class="form-inline" align="right">
    <label>Filter : </label>
    <select name="select_year_filter" class="form-control form-control-sm">
        <option value=0> All </option>
      <?php $__currentLoopData = $semesterall; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value=<?php echo e($entry->Year); ?>> <?php echo e($entry->Year); ?>  </option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <input type=submit value=Filter class='form-control form-control-sm'>
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Semester Name</th>
	  <th>Semester Year</th>
	  <th>Start Date</th>
	  <th>End Date</th>

      <th></th>
    </tr>

  <?php $__currentLoopData = $semesters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>

       <td><?php echo e($entry->Id); ?></td>
       <td><?php echo e($entry->semestername->Name); ?></td>
	     <td><?php echo e($entry->Year); ?></td>
	     <td><?php echo e(date_format(date_create($entry->StartDate),'d-m-Y H:i:s')); ?></td>
	     <td><?php echo e(date_format(date_create($entry->EndDate),'d-m-Y H:i:s')); ?></td>


       <td>
         <form action="Semesters/Delete/<?php echo e($entry->Id); ?>" method='POST'>
           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 </table>
 <?php echo e($semesters->appends($_GET)->links()); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
   <div class="well">
	  <h4> Semester Information : </h4>
      <form action="Semesters\Insert", method='POST'>
        <label>Semester Name : </label>
		<select name='select_semestername' class='form-control' >
		    <?php $__currentLoopData = $semesternames; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $names): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			  <option value=<?php echo e($names->Id); ?>> <?php echo e($names->Name); ?>  </option>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</select>
        <label>Semester Year : </label>
        <input type=text name='input_semester_year' class='form-control'>
		<div class="row">
			<div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
				<label>Start Date : </label>
				<input type='date' name='input_semester_startDate' class='form-control' />
			</div>
			<div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
				<label>End Date : </label>
				<input type='date' name='input_semester_endDate' class='form-control' />
			</div>
		</div>
		<br>
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <input type=submit value="Insert New Semester" class="button form-control">
    </form>
   </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
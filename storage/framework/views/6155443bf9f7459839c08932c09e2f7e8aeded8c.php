<?php $__env->startSection('content'); ?>
<form action="Subjects" method="get" class="form-inline" align="right">

    <input type=text name="input_subject_search" class='form=control' placeholder="subject id or name">
    <input type=submit value='Search' class='form-control'>
	
    
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Subject Id</th>
      <th>Subject Name</th>

      <th></th>
    </tr>

  <?php $__currentLoopData = $Subjects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $entry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>

       <td><?php echo e($entry->Id); ?></td>
       <td><?php echo e($entry->SubjectId); ?></td>
       <td><?php echo e($entry->SubjectName); ?></td>

       <td>
         <form action="Subjects/Delete/<?php echo e($entry->Id); ?>" method='POST'>
           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 </table>
 <?php echo e($Subjects->appends($_GET)->links()); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
   <div class="well">
	  <h4> Subjects Information : </h4>
      <form action="Subjects\Insert", method='POST'>
        <label>Subject Id : </label>
        <input type=text name='input_subject_id' class='form-control'>
        <br>
        <label>Subject Name : </label>
        <input type=text name='input_subject_name' class='form-control'>
        <br>
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <br>
        <input type=submit value="Insert New Subject" class="button form-control">
    </form>
   </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
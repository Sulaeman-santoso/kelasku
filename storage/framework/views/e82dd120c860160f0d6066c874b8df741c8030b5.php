<link rel="stylesheet" href="<?php echo e(URL::to('css/style.css')); ?>" />
<link rel="stylesheet" href="<?php echo e(URL::to('css/common.css')); ?>" />

<nav class="navbar " class='topNavbar' style="background-image: linear-gradient(rgba(0,150,255),rgba(80,46,212)); ">
  <div class="container-fluid" class="topFluidBar" style="margin:0 auto; width: 80%;text-align: center; ">
    <ul class="nav headerbar navbar-nav" style="width:100%; text-align: center;" >
      <li  >
        <img src="<?php echo e(URL::to('Assets/kelasku_logo_long.png')); ?>" style="width:70px;height: 30px;" />
      </li>
      <li class="active"><a href="\laravel\public\Lecturer">Kembali</a></li>
	    <li><a href="\laravel\public\Enrollments">Jadwal Kelas</a></li>
	    <li><a href="\laravel\public\Lecturer\Kehadiran\0">Kehadiran</a></li>
      <li><a href="\laravel\public\Lecturer\Keaktifan\0">Keaktifan</a></li>

      <li><a href="\laravel\public\Lecturer\Pengumuman\0">Pengumuman</a></li>
      <li><a href="\laravel\public\Lecturer\Pengaturan\0">Pengaturan Kelas</a></li>
    </ul>
  </div>
</nav>

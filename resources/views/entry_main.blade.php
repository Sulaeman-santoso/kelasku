@extends('main')

@section('content')

  <h1>IBats- Image-Based attendance system   </h1>  
  
  <div>
    iBats is a research product of 5 researcher at Maranatha Christian university.
    the research is conducted to be a preliminary research unto digital attendance.
    ibats also maintains open source classroom dataset

  </div>
@endsection

@section('sidebar')
  <div class="well">
   <h4> please enter your login credential : </h4>
     <form action="/laravel/public/Users/Login", method='POST'>
       <label>User Id : </label>
       <input type=text name='input_user_id' class="form-control" >
       <label>User Passwords : </label>
       <input type=password name='input_user_password' class="form-control" >
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <br>
       <input type=submit value="Login" class="button form-control">  
     </form>
     <form action="Register", method='POST'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
        <input type=submit value="Register" class="button form-control">
     </form>

  </div>

@endsection

@extends('lecturer_main')

@section('content')

  <div id="vue_content">
    <!--Menu-->
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#kelas">Kelas</a></li>
      <li><a data-toggle="tab" href="#student">Murid</a></li>
      <li><a data-toggle="tab" href="#menu2">Lainnya</a></li>
    </ul>

    <!-- Content -->
    <div class="tab-content">
      <div id="kelas" class="tab-pane fade in active">
        <lreport :mydata="{{ json_encode($classes) }}" :chosen_option="{{$chosen_option}}" ></lreport>
      </div>
      <div id="student" class="tab-pane fade">
        <p>Some content in menu 1.</p>
      </div>
      <div id="menu2" class="tab-pane fade">
        <h3>Menu 2</h3>
        <p>Some content in menu 2.</p>
      </div>
    </div>

  </div>

@endsection

@section('sidebar')
  <div id="vue_sidebar" >

    <div class="well">
      <import ></import>
    </div>
    <div class=well>

    </div>
  </div>

@endsection


@section('extra_script')
  <link rel="stylesheet" href="{{URL::to('css/common.css')}}" />
  <script type="module" src="{{URL::to('js/lecturer_setting.js')}}" > </script>
  <style>
    #vue_content {
      background: white;
      border: 2px solid black;
      padding : 15px;
    }
  </style>
@endsection

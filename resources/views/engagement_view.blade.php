@extends('main')


@section('extra_script') 





	<script src="{{URL::to('js/EngagementPage.js')}}"> </script>

@endsection

@section('content')
  <form action='Engagements' method="get" class='form-inline' align="right">
    Filter :
	<select name="filter_engagement_class" class='form-control form-control-sm' ">
      <option value=0 >All Class</option>
      @foreach($classes as $entry)
				@if(empty($chosen_class))
					@if ($entry->Id == $chosen_class)
						<option value="{{$entry->Id}}" selected >{{$entry->Lecturer . " " . $entry->Subjects . " " .$entry->ClassNumber}}</option>
					@else 
						<option value="{{$entry->Id}}">{{$entry->Lecturer . " " . $entry->Subjects . " " .$entry->ClassNumber}}</option>		
					@endif
				@endif
	  @endforeach
    </select>
    <select name="input_enrollment_schedule" style="display:none;" class='form-control form-control-sm'>
      <option value= 0>All Schedule </option>
	</select>
    <input type=submit value=Filter  class='form-control form-control-sm'>
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Class Name</th>
	    <th>Date </th>
		<th>Id</th>
	    <th>Name</th>
		<th>engagement</th>
     <th></th>
    </tr>

  @foreach($engagements as $entry)
    <tr>
        <td>{{$entry->Id}}</td>
        <td>{{ $entry->SubjectName ."-". $entry->Class}}</td>
	    <td>{{$entry->ScheduleDate}}</td>
	    <td>{{$entry->StudentId}}</td>
		<td>{{$entry->StudentName}} </td>
		<td>{{$entry->Interaksi . "-" . $entry->Value }}</td>
		
       <td>
         <form action="Engagements/Delete/{{$entry->Id}}" method='POST'>
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  @endforeach
 </table>
 {{$engagements->appends($_GET)->links()}}
@endsection  

@section('sidebar')
   <div class="well">
	  <h4> Engagement Information : </h4>
      <form action="Engagements\Insert" id="engagementForm" method='POST'>
        <label>Class : </label>
			<br>
			<select class="form-control selectpicker" name="select_engagement_classes" id="select_engagement_classes"  data-live-search="true">
			  @foreach($classes as $entry)
				@if(empty($chosen_class))
					@if ($entry->Id == $chosen_class)
						<option value="{{$entry->Id}}" selected >{{$entry->Lecturer . " " . $entry->Subjects . " " .$entry->ClassNumber}}</option>
					@else 
						<option value="{{$entry->Id}}">{{$entry->Lecturer . " " . $entry->Subjects . " " .$entry->ClassNumber}}</option>		
					@endif
				@endif
			  @endforeach
			</select>
			<br>
			<label>Schedule :</label> 
			
		<div class="container" style="width:100%;margin:0px;padding:0px;">
			<div class="col-md-7" style="margin:0px;padding:0px;">
				<select class="form-control selectpicker" name="select_engagement_schedule" id="select_engagement_schedule" data-live-search="true">
					<option value =-1> Please select a class </option>
				</select>
			</div>
			<div class="col-md-1" style="margin:0px;padding:0px;">
				&nbsp;
			</div>
			<div class="col-md-4" style="margin:0px;padding:0px;">
				<input type=button class="form-control button btn-primary" value="add new" />	
			</div>
			<br>
		</div>
		<label>Student Id : </label>
		<select class="form-control selectpicker" name="select_engagement_user" id="select_engagement_user" data-live-search="true">
			<option value = -1> please select a class </option>
		</select>
		<br>
		<!--
		<select name="coba" class="form-control selectpicker" data-live-search="true">
			<option value="1"> satu </option>
			<option value="2"> dua </option>
			<option value="3"> tiga </option>
			<option value="4" selected > empat </option>
			<option value="5"> lima </option>
		</select>
		-->
		
		<script>
			$(".selectpicker").select2();
		</script>
	
		<br>  
		<input type="hidden" name="_token"  value="{{ csrf_token() }}">
		<input type="hidden" name="_id_eng" id="_id_eng" value="0">
		
		<div class="container" style="width:100%;padding:0px;margin:auto;">
			<div class="col-md-3 col-xs-3" style="padding:0px; width:20%; margin:0 5;">
				<button class="button form-control btn-success" id="btnRight"> Right </button>
			</div>
			<div class="col-md-3 col-xs-3" style="padding:0px;width:20%;margin:0 5;">
				<button class="button form-control btn-danger" id="btnWrong">Wrong</button>
			</div>
			<div class="col-md-3 col-xs-3" style="padding:0px;width:20%;margin:0 5;">
				<button class="button form-control btn-info" id="btnAsk">Ask</button>
			</div>
			<div class="col-md-3 col-xs-3" style="padding:0px;width:20%;margin:0 5;">
				<button class="button form-control btn-warning" id="btnAct">Act</button>
			</div>
			
		</div>
		
</div>
		
	
		
      
        
    </form>


   </div>
@endsection

@extends('main')

@section('content')
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Semester Name</th>
      <th>Created_at</th>
      <th>Updated_at</th>
      <th></th>
    </tr>

  @foreach($semesterNames as $entry)
    <tr>

       <td>{{$entry->Id}}</td>
       <td>{{$entry->Name}}</td>
       <td>{{$entry->created_at}}</td>
       <td>{{$entry->updated_at}}</td>
       <td>
         <form action="SemesterNames/Delete/{{$entry->Id}}" method='POST'>
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  @endforeach
 </table>
@endsection

@section('sidebar')
   <div class="well">
	  <h4> Semester name Information : </h4>
      <form action="SemesterNames\Insert", method='POST'>
        <label>Semester Name : </label>
        <input type=text name='input_semestername_name' class='form-control'>
        <br>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <br>
        <input type=submit value="Insert New Subject" class="button form-control">
    </form>
   </div>
@endsection

@extends('main')

@section('content')
  <form class='form-inline' method="get" action="Schedules" align="right">
    <label>
      filter schedules :
    </label>
    <select name="input_schedules_filter"  class="form-control form-control-sm" >
      <option value=0> All </option>
      @foreach ($classes as $entry)
        <option value={{$entry->Id}}> {{ $entry->Lesson->semester->semesterName->Name . "-". $entry->Lesson->Subject->SubjectName . "-" . $entry->ClassNumber}}  </option>
      @endforeach
    </select>
    <input type=submit value='Filter' class='form-control form-control-sm'>
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Class</th>
      <th>Date</th>
      <th></th>
    </tr>
    @foreach($schedules as $entry)
      <tr>
         <td>{{$entry->Id}}</td>
         <td>{{$entry->classes->Lesson->semester->semesterName->Name . "-". 
          $entry->classes->lesson->semester->Year . "/" . ($entry->classes->lesson->semester->Year+1) . " " .
          $entry->classes->lesson->Subject->SubjectName . "-" . $entry->classes->ClassNumber}}</td>
         <td>{{date_format(date_create($entry->scheduleDate),'d-m-Y H:i:s') }}</td>
         <td>
           <form action="Schedules/Delete/{{$entry->Id}}" method='POST'>
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
             <input type=submit value=delete >
           </form>
         </td>
     </tr>
    @endforeach
 </table>
 {{$schedules->appends($_GET)->links()}}
@endsection

@section('sidebar')
   <div class="well">
	  <h4> Schedule Information : </h4>
      <form action="Schedules\Insert", method='POST'>
        <label>Class : </label>
        <select name='select_schedule_classes' class='form-control' >
            @foreach ($classes as $entry)
            <option value={{$entry->Id}}> {{$entry->Lesson->semester->semesterName->Name . "-". $entry->Lesson->Subject->SubjectName . "-" . $entry->ClassNumber}}  </option>
          @endforeach
        </select>
        <label>Date : </label>
        <input name='input_schedule_date' class='form-control'  type=datetime-local />
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <br>
        <input type=submit value="Insert New Schedule" class="button form-control">
    </form>
   </div>
@endsection

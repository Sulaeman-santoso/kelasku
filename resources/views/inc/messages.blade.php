@if(count($errors) >0)

   @foreach ($errors->all() as $error)
     <div class="alert alert-danger  alert-dismissable fade in">
      {{$error}}
    </div>
   @endforeach
   
@endif

@if (session('messages'))
  <div class="alert alert-success  alert-dismissable fade in">
      {{session('messages')}}
  </div>
@endif

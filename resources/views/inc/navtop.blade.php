<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Kelas-<span>Ku</span></a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="\laravel\public\">Home</a></li>
      <!--<li><a href="\laravel\public\Roles">Roles</a></li>-->
      <li><a href="\laravel\public\Users">User</a></li> 
      <li><a href="\laravel\public\Subjects">Subject</a></li>
	   <!-- <li><a href="\laravel\public\SemesterNames">Semester Names</a></li> -->
	    <li><a href="\laravel\public\Semesters">Semester</a></li>
      <li><a href="\laravel\public\Lessons">Lessons</a></li>
      <li><a href="\laravel\public\Classes">Class</a></li>
	    <li><a href="\laravel\public\Enrollments">Enrollment</a></li>
	    <li><a href="\laravel\public\Schedules">Schedule</a></li>
      <li><a href="\laravel\public\Attendances">Attendance</a></li>
	    <li><a href="\laravel\public\SchedulePhotos">Schedule Photos</a></li>
    </ul>
  </div>
</nav>

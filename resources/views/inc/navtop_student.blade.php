<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Kelas-Ku </a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="\laravel\public\">Home</a></li>
	    <li><a href="\laravel\public\Enrollments">Enrollment</a></li>
	    <li><a href="\laravel\public\Schedules">Schedule</a></li>
      <li><a href="\laravel\public\Attendances">Attendance</a></li>
      <li><a href="\laravel\public\EngagementStudent">Engagement</a></li>
      <li><a href="\laravel\public\Announcement">Announcement</a></li>
    </ul> 
  </div>
</nav>

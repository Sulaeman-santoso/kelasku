@extends('lecturer_main')

@section('content')

  <h1>Selamat datang  </h1>
  <div>
    Halaman ini adalah halaman awal dari web untuk pengajar

  </div>
@endsection

@section('sidebar')
  <div class="well">
   <h4> Info Pengajar :  {{ $users->Name }} </h4>
   <h5>Kelas yang diajar semester ini : </h5> 

	<ul style="list-style-type: none;
  margin: 0;
  padding: 0;"> 
   	@foreach ($classes as $entry) 
   		<li>{{  $entry->SubjectName ."-". $entry->ClassNumber }} </li>
   	@endforeach
   	</ul>

  </div>

@endsection

@section('extra_script') 
  <link rel="stylesheet" href="{{URL::to('css/common.css')}}" />
@endsection

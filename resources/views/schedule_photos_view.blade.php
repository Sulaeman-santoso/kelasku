@extends('main')

@section('content')
  <form action='SchedulePhotos' method="get" class="form-inline" align="right">
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    <label>Filter : </label>
	<select name="select_class_filter" id="select_class_filter" class="form-control form-control-sm">
        <option value=0> All </option>
        @foreach( $classes as $entry)

            @if (empty($chosen_class))
              <option value='{{$entry->Id}}'> {{$entry->SubjectName . "-"  . $entry->ClassNumber}} </option>
            @else
              @if ($entry->Id == $chosen_class)
                 <option value='{{$entry->Id}}' selected> {{$entry->SubjectName . "-"  . $entry->ClassNumber}} </option>
              @else
                 <option value='{{$entry->Id}}'> {{$entry->SubjectName . "-"  . $entry->ClassNumber}} </option>
              @endif
            @endif
        
        @endforeach
    </select>
    <select name="select_schedule_filter"  id="select_schedule_filter" class="form-control form-control-sm">
       <option value=0> All Schedule </option>
        @foreach($schedules as $entry)
            @if (empty($chosen_class))
            @else
              @if ($entry->Id_Class == $chosen_class)
                 <option value='{{$entry->Id}}' > {{$entry->scheduleDate}} </option>
              @else
             
              @endif
            @endif
        @endforeach
    </select>
    <input type=submit value=Filter class='form-control form-control-sm'>
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Schedule</th>

      <th></th>
      <th></th>
    </tr>

  @foreach($schedulePhotos as $entry)
    <tr>

       <td>{{$entry->Id}}</td>
       <td>{{
        $entry->schedule->classes->lesson->Subject->SubjectName   . " " .
        $entry->schedule->classes->ClassNumber . " -  " .  
        $entry->schedule->classes->lesson->semester->Year . "/" . ($entry->schedule->classes->lesson->semester->Year +1) . " " .  date_format(date_create($entry->schedule->scheduleDate),'d-m-Y H:i:s') . " "}}</td>

       <td>
        <form action="SchedulePhotos/ShowPhoto/{{$entry->Id_Photo}}" method='POST'>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type=submit value='Show' >
        </form>
       </td>
       <td>
         <form action="SchedulePhotos/Delete/{{$entry->Id}}" method='POST'>
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  @endforeach
 </table>
 {{$schedulePhotos->appends($_GET)->links()}}
@endsection

@section('sidebar')
   <div class="well">
	  <h4> Schedule Photo Information : </h4>
      <form action="SchedulePhotos\Insert", method='POST' enctype="multipart/form-data">
        <label> Class </label>
        <select name='select_class_name' id='select_class_name' class='form-control' onchange="filter(this.value)">
          @foreach( $classes as $entry)
              <option value='{{$entry->Id}}'> {{$entry->SubjectName . "-" . $entry->ClassNumber}} </option>
          @endforeach
        </select>
        <label>Schedule </label>
        <select name='select_schedule_name' id='select_schedule_name' class='form-control' >
          @foreach ($schedules as $entry)
            <option value='{{$entry->Id}}'  >{{date_format(date_create($entry->scheduleDate),'d-m-Y H:i:s') }}</option>
          @endforeach
        </select>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>
          function filter(value) {
            var id = $("#select_class_name").find(":selected").val();
            var tokens = '{{csrf_token()}}';
            //arrghh.. isn't there a beter way to do this ?
            $.post('Schedules/Filter/'+id,{
              Id : id,
              _token : tokens
            },function(data,status){
               // alert(data);
                $('#select_schedule_name').html(data);
            });
          }
        </script>
        <label>Photo Name : </label>

        <?php
          echo Form::file('input_schedulePhotos_photos');

         ?>
      <!--  <input type=file name='input_schedulePhotos_photos' class='form-control'>-->
       <br>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <br>
        <input type=submit value="Insert New Photos" class="button form-control">
    </form>
   </div>
@endsection


@section('extra_script') 
  
  <script src="{{URL::to('js/schedule_photo_view.js')}}" > </script>
@endsection

@extends('/main')

@section('content')
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Name</th>
      <th>Created_at</th>
      <th>Updated_at</th>
      <th></th>
    </tr>
  @foreach($list as $entry)
    <tr>
       <td>{{$entry->Id}}</td>
       <td>{{$entry->Name}}</td>
       <td>{{$entry->created_at}}</td>
       <td>{{$entry->updated_at}}</td>
       <td>
         <form action="Roles/Delete/{{$entry->Id}}" method='POST'>
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  @endforeach
 </table>
@endsection

@section('sidebar')
   <div class="well">
	  <h4> Role Information : </h4>
      <form action="Roles\Insert", method='POST'>
        <div class="input-group">
          <label class="input-group-addon">Insert Name </label>
          <input type=text class="form-control" id='input_role_name' name='input_role_name'>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div></div>
        <input type=submit value="Insert New Role" class="button form-control">
      </form>
   </div>
@endsection

  @extends('lecturer_main')

@section('content')
<meta name="token" id="token" content="{{ csrf_token() }}">
  <div class="overview contentContainer card"  >

      <ul class="nav nav-tabs" >
      <li
          @if ($view_id ==0)
             class="active"
          @endif
      ><a href="0">Overview</a></li>
      <li
          @if ($view_id ==1)
             class="active"
          @endif
      ><a href="1">Tags</a></li>
      <li
          @if ($view_id ==2)
             class="active"
          @endif
      ><a href="2">Detil Pengumuman</a></li>
      <li>

      </li>
    </ul>


 <div class="overview" >

   <div id="card_vue_announcement"  data="data baru" >

   <modal name="hello-world">
     <img v-bind:src="img_src" id="image_view" style="width:100%; height:100%;" />
   </modal>
   <div style="width:100%;margin-top: 20px;"> </div> <!--Divider -->
   @if ($view_id == 0)
   <!-- Overview Pengumuman mungkin keluarin satu atau dua pengumuman terakhir atau pengumuman sacara umum gitu-->

   <div class=flex-container style="justify-content: center;">
       @foreach ($announcements as $entry)
         <div class="coba" style="margin: 5px; width:23%; height: 300px; padding: 2px; border:2px solid black;" >
         <div class="card" style="overflow:wrap;" @click="seeDetail({{$entry->Id}}, '{{ $entry->photos->Path  }}')">
           <img class="card-img-top" src="{{$entry->photos->Path}}" alt="Card image"
                  style="width:100%; height:50px;object-fit: cover;">
           <div class="card-body" style=" height: 180px;overflow:auto;">
               <h4 class="card-title">{{$entry->Title}}&nbsp</h4>
               <div >
                 {{$entry->Content}}
               </div>
          </div>
         </div>
         <footer>
         <div><button @click="deleteService({{$entry->Id}})">Delete Announcement</button></div>
          </footer>
       </div>
       @endforeach
   </div>
   {{$announcements->appends($_GET)->links()}}

   @elseif($view_id == 1)
    <div id='vue_tag_app'>
      <div>
        <tag_view></tag_view>
      </div>
   </div>
  <!-- Ini buat edit Tag Yang ada.. atau mending langsung aja di sidebar ya jadi satu page aja -->
   @elseif($view_id == 2)

   @endif
 </div> <!-- end of vue_app -->
 </div><!--End of Overview -->

</div> <!-- End of ContainerCard -->


@endsection

@section('sidebar')
 <div class="well" id="announcement_add_module"  chosen_user={{$chosen_user}} >

   <form id="vueForm" enctype="multipart/form-data"  v-on:submit.prevent >
    <h4>Add Announcement </h4>
    <div class="input-group mb-3">
      <div class="input-group-addon">
        <span class="input-group-text">Judul : </span>
      </div>
        <input class="form-control" type=text  v-model="title" />
    </div>
    <textarea class="form-control" type=text  v-model="content" >

    </textarea>
    <span class="input-group-text" id="basic-addon1">Tags</span>
    <input type="text" class="form-control" placeholder="tags"  v-model="tags">

    <input type=file @change='fileUpload' id=foto />
    <div>
      <span>Publish Date : </span>
      <Datetime v-model="publish_date" format="yyyy-LL-dd hh:mm" type="datetime"  title="Publish Date" input-style="width:100%;"> </Datetime>
    </div>
    <div>
      <span>Expire Date : </span>
      <Datetime v-model="expire_date" format="yyyy-LL-dd hh:mm"  type="datetime" title="Expire Date" input-style="width:100%;"> </Datetime>
   </div>

    <button  class='form-control' v-on:click='AddAnnouncement' >Add Announcement </button>

 </div>
</form>

<div class="well" id="tag_add_module">
     @{{message}}
</div>

@endsection


@section('extra_script')
  <link rel="stylesheet" href="{{URL::to('css/common.css')}}" />
  <link rel="stylesheet" href="{{URL::to('css/app.css')}}" />
  <script>
    var chosen_data = {!! $chosen_user !!}
  </script>
  <script type="module" src="{{URL::to('js/lecturer_pengumuman.js')}}" > </script>
@endsection

@extends('main')

@section('extra_script')
  <script>
      var token = '{{ csrf_token()}}' ;
  </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
  <script src="{{URL::to('js/image-picker.min.js')}}"> </script>

  <script src="{{URL::to('js/AttPageController.js')}}"> </script>

@endsection

@section('content')

   <div class='container' style='margin-right:50px;padding:0px'>
     <div class='row' style=' padding:0px;margin-right:50px;'>
       <div class='well  col-sm-12 col-xs-12 col-md-8 col-lg-8' style='margin:5px; padding:5px;'>
         <form action='Attendances' method='GET' enctype="multipart/form-data">
            		  <div class=' col-md-6 col-lg-6 col-xs-6 col-sm-6' style='padding:0px; margin:0px;'>
            			  <div class='input-group'>
            			  <label class='input-group-addon'>Pick Class :</label>
            			  <select class= 'form-control ' name='select_attendance_class' 
								  id='select_attendance_class' onchange="filter(this.value,'{{csrf_token()}}')">
            				@foreach($classes as $entry)
								@if ($id_class == $entry->Id)
								  <option value='{{$entry->Id}}' selected >
									  {{$entry->lesson->Subject->SubjectName . " " . $entry->ClassNumber}}*
								  </option>
								@else
									<option value='{{$entry->Id}}'>
									  {{$entry->lesson->Subject->SubjectName . " " . $entry->ClassNumber}}
									</option>
								@endif
            				@endforeach
            			  </select >
            			</div>
            		</div>
            		<div class='col-lg-6 col-md-6 col-sm-6 col-xs-6' style='padding:0px; margin:0px;' >
                      <div class='input-group'>
                          <label class='input-group-addon'>Pick Schedule :  (  {{ $id_schedule }})</label>
                              <select class= 'form-control' name='input_attendance_IdSchedule' id='select_attendance_schedule' onchange="getAttendancePhoto(this.value,'{{ csrf_token() }}')">

                                @foreach($schedules as $entry)
                                  @if ( $id_schedule == $entry->Id)
                                    <option value='{{$entry->Id}}' selected >
                                      {{ $entry->scheduleDate   }}*
                                    </option>
                                  @else
                                    <option value='{{$entry->Id}}'>
                                      {{ $entry->scheduleDate}}  
                                    </option>
                                  @endif
                                @endforeach
                              </select>
                       </div>
            		</div>
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
			 <input type=submit value='Open Attendance' class='form-control'>
	
        </form>
		  <select class='well image_picker' id='picker_image' style='display:flex;flex-wrap:wrap;' >
          @foreach($photolist as $entry) 
			
			  @if($entry->isValid != 0)
			  <option style='display:inline-block;margin-bottom:8px;width:calc(50%-4px)' value='{{$entry->Id_Photos}}' 
				data-img-src='{{ $entry->Path }}' > 
				{{$entry->UserId}} 
			  </option>
			  @else 
			   <option style='background-color:red;display:inline-block;margin-bottom:8px;width:calc(50%-4px)' value='{{$entry->Id_Photos}}' 
				data-img-src='{{ $entry->Path }}' > 
				{{"Invalid"}} 
			  </option>
			  @endif
			
		  @endforeach
    	  </select>
	     
       </div>
     </div>
       @if(count($photolist) >0)
	       {{  $photolist->appends($_GET)->links() }}
      @endif
   </div>
	
   <div class="container" style='margin-right:50px;padding:0px'> 


   </div>

@endsection

@section('sidebar')
   <div class='well'>
	Add Attendance :
	<form action='Attendances/InsertWeb' method='POST' class='form-horizontal' id='AttendanceForm' >
	        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<label>Id_Student : </label>
    <input type=text name='input_attendance_IdStudent' id='input_id_autocomplete' class='form-control'>
    <script>
        var otocomplete = [
          'test'
        ];

        $("#input_id_autocomplete").autocomplete({
          source : otocomplete
        });
    </script>
    <label class='.checkbox-inline'>
      <input type=checkbox  value='withoutpic' name='input_attendance_without_picture'>
      Attendance without picture
   </label>
   <input type=hidden id='input_hidden_idSchedule' name='input_hidden_idSchedule' value='{{$id_schedule}}'>
   <input type=hidden id='input_hidden_idPhotos' name='input_hidden_idPhotos'>
   

    <!-- <input type=submit value='Add Attendance' class='form-control'> -->
    <input type=button value='Add Attendance Revised' class='form-control' id='btnAddAttendance'>
	<!--
	<input type=button value='Tester' class='form-control' id='tester'>
	-->
	
	</form>
	Update Attendance
	<form action='UpdateAttendance' method='POST'>
		<input type=submit value='Update Attendance' class='form-control'>
	</form>



   </div>
@endsection

<html>
 <head>
    <title>KelasKu - Live</title>

    <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


	  <!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
 

	 <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	

	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>

    <script src=" {{ URL::to('js/moment.js') }} ">  </script>
    <script src=" {{ URL::to('js/image-picker.min.js') }} ">  </script>
   
    <link rel="stylesheet" href="{{URL::to('css/image-picker.css')}}" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />

    <style type="text/css">
  
    div#header {
      width:100%;
    }
    
    ul#nav-top {
      width:100%;
  
      float:none;
      margin:0 auto;
      display: block;
      text-align: center
    }

    .navbar-nav > li {
      display: inline-block;
      float:none;
    }
    
     .thumbnails li img{
       width: 100px;
     }
     
    tr:nth-child(even){
      background-color:#e8e8e8;
    }
    tr:nth-child(odd){
      background-color:white;
    }
        
  
    </style>
    @yield('extra_script')
 </head>

 <body>
 <div class=backgroundimago > </div>
   @include('inc.navtop_lecturer')
     
   <div class="container" style="padding: 0px;">
     <div class="col-lg-8 col-md-8" style="padding:0;">
       @include('inc.messages')
       @yield('content')
     </div>
     <div class="col-lg-4 col-md-4" style="padding: 0 10px;">
       @yield('sidebar')
     </div>
   </div>
   @include('inc.footer')

 </body>

</html>

@extends('lecturer_main')

@section('content')

  <div class="overview contentContainer card" >

      <ul class="nav nav-tabs" >
      <li
          @if ($view_id ==0)
             class="active"
          @endif
      ><a href="0">Overview</a></li>
      <li
          @if ($view_id ==1)
             class="active"
          @endif
      ><a href="1">Summary Keaktifan</a></li>
      <li
          @if ($view_id ==2)
             class="active"
          @endif
      ><a href="2">Detil Jadwal</a></li>
    </ul>

   <div class="overview">
     <div style="width:100%;margin-top: 20px;"> </div>
   	@if ($view_id == 0)


    <table class="tableData">
         <tr>
        <th>Kelas</th><th>Jumlah Peserta </th><th>Total Point </th><th>Persentase keakftifan </th>
      </tr>
    @foreach ($report as $entry)

      <tr>
        <td class="table_cell">
          {{$entry->SubjectName . " " . $entry->ClassNumber}}
        </td>
        <td class="table_cell">
          {{$entry->JmlUser }}
        </td>
        <td class="table_smaller_cell">
          {{ $entry->engagementValue}}
        </td>
        <td class="table_smaller_cell">
          {{  ($entry->engagementValue != 0)?$entry->engagementValue/$entry->JmlUser : 0 }}
        </td>
      </tr>
    @endforeach
    </table>

   @elseif($view_id == 1)

       <form class='form-inline' method="get" action="1" align="right">
        <label>filter Keaktifan :</label>
            <select class= 'form-control' name="select_kehadiran_class_2"  id="select_kehadiran_class_2" >
              @foreach ($classes as $entry)
                @if ( $chosen_class == $entry->Id )
                 <option value="{{$entry->Id}}" selected >{{  $entry->SubjectName ."-". $entry->ClassNumber }} </option>
                @else
                <option value="{{$entry->Id}}">{{  $entry->SubjectName ."-". $entry->ClassNumber }} </option>
                @endif
              @endforeach
            </select>
          <input type=submit value='Filter' class='form-control form-control-sm'>
      </form>

      <table class="tableData">
      <tr>
          <th>Mata Kuliah</th><th>Kelas</th><th>NRP</th><th>NaMA</th><th>point Keaktifan</th>
        </tr>
        @foreach ($report_sum as $entry)
        <tr>
          <td class="table_cell">
            {{ $entry->SubjectName }}
          </td>
          <td class="table_cell">
            {{$entry->ClassNumber}}
          </td>
          <td class="table_cell">
            {{$entry->UserId }}
          </td>
          <td class="table_cell">
            {{ $entry->Name}}
          </td>
          <td class="table_smaller_cell">
            {{$entry->total_engagement }}
          </td>

        </tr>
      @endforeach
     </table>

     @if ($report_sum != null)
      {{$report_sum->appends($_GET)->links()}}
    @endif

   @elseif($view_id == 2)

   		 <form class='form-inline' method="get" action="2" align="right">
        <label>filter Keaktifan :</label>
            <select class= 'form-control' name="select_kehadiran_class"  id="select_kehadiran_class" >
              @foreach ($classes as $entry)
                @if ( $chosen_class == $entry->Id )
                  <option value="{{$entry->Id}}" selected >{{  $entry->SubjectName ."-". $entry->ClassNumber }} </option>
                @else
                  <option value="{{$entry->Id}}">{{  $entry->SubjectName ."-". $entry->ClassNumber }} </option>
                @endif
              @endforeach
            </select>
          <input type=submit value='Filter' class='form-control form-control-sm'>
      </form>

       <table class="tableData">
         <tr>
          <th>Kelas</th><th>Nama </th><th>Point </th><th>Persentase keakftifan </th>
        </tr>
        @foreach ($report_detail as $entry)
        <tr>
          <td class="table_cell">
            {{$entry->SubjectName . " " . $entry->ClassNumber}}
          </td>
          <td class="table_cell">
            {{$entry->UserId . " " . $entry->Name}}
          </td>
          <td class="table_smaller_cell">
            {{$entry->engagement_name }}
          </td>
          <td class="table_smaller_cell">
            <button class="btn" onclick="DeleteEngagement({{$entry->IdEng}})"> Delete </button>
          </td>
        </tr>
      @endforeach
    </table>

    @if ($report_detail != null)
      {{$report_detail->appends($_GET)->links()}}
    @endif

   	@endif

   </div>

  </div>



@endsection

@section('sidebar')
  <div class="well">
    <h4> Engagement Information : </h4>
      <form action="../../Engagements/InsertService" id="engagementForm" method='POST'>
        <label>Class : </label>
      <br>
      <select class="form-control selectpicker" name="select_engagement_classes" id="select_engagement_classes"  data-live-search="true">
        @foreach($classes as $entry)

          @if ($entry->Id == $chosen_class)
            <option value="{{$entry->Id}}" selected >{{$entry->Lecturer . " " . $entry->Subjects . " " .$entry->ClassNumber}}</option>
          @else
            <option value="{{$entry->Id}}">{{$entry->Lecturer . " " . $entry->Subjects . " " .$entry->ClassNumber}}</option>
          @endif

        @endforeach
      </select>
      <br>
      <label>Schedule :</label>

    <div class="container" style="width:100%;margin:0px;padding:0px;">
      <div class="col-md-7" style="margin:0px;padding:0px;">
        <select class="form-control selectpicker" name="select_engagement_schedule" id="select_engagement_schedule" data-live-search="true">
          <option value =-1> Please select a class </option>
        </select>
      </div>
      <div class="col-md-1" style="margin:0px;padding:0px;">
        &nbsp;
      </div>
      <div class="col-md-4" style="margin:0px;padding:0px;">
        <input type=button class="form-control button btn-primary" value="add new" />
      </div>
      <br>
    </div>

    <label>Student Id : </label>
    <select class="form-control selectpicker" name="select_engagement_user" id="select_engagement_user" data-live-search="true">
      <option value = -1> please select a class </option>
    </select>
    <br>

    <script>
      $(".selectpicker").select2();
    </script>

    <br>
    <input type="hidden" name="_token"  value="{{ csrf_token() }}">
    <input type="hidden" name="_id_eng" id="_id_eng" value="0">


  </form>

    <div class="container" style="width:100%;padding:0px;margin:auto;">
      <div class="col-md-3 col-xs-3" style="padding:0px; width:20%; margin:0 5;">
        <button class="button form-control btn-success" id="btnRight"> Right </button>
      </div>
      <div class="col-md-3 col-xs-3" style="padding:0px;width:20%;margin:0 5;">
        <button class="button form-control btn-danger" id="btnWrong">Wrong</button>
      </div>
      <div class="col-md-3 col-xs-3" style="padding:0px;width:20%;margin:0 5;">
        <button class="button form-control btn-info" id="btnAsk">Ask</button>
      </div>
      <div class="col-md-3 col-xs-3" style="padding:0px;width:20%;margin:0 5;">
        <button class="button form-control btn-warning" id="btnAct">Act</button>
      </div>
    </div>
  </div>

   </div>
@endsection


@section('extra_script')
  <link rel="stylesheet" href="{{URL::to('css/common.css')}}" />
  <script src="{{URL::to('js/lecturer_keaktifan.js')}}" > </script>
@endsection

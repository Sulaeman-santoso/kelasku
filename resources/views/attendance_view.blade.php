@extends('main')

@section('extra_script')
  <script>
      var token = '{{ csrf_token()}}' ;
  </script>
  <script
			  src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
			  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
			  crossorigin="anonymous"></script>
  <script src="{{URL::to('js/AttPageController.js')}}"> </script>

@endsection

@section('content')

   <div class='container' style='margin-right:50px;padding:0px'>
     <div class='row' style=' padding:0px;margin-right:50px;'>
       <div class='well  col-sm-12 col-xs-12 col-md-8 col-lg-8' style='margin:5px; padding:5px;'>
	   
         <form action='Attendances' method='POST' enctype="multipart/form-data">

            		  <div class=' col-md-6 col-lg-6 col-xs-6 col-sm-6' style='padding:0px; margin:0px;'>
            			  <div class='input-group'>
            			  <label class='input-group-addon'>Pick Class :</label>
            			  <select class= 'form-control ' name='select_attendance_class' 
								  id='select_attendance_class' onchange="filter(this.value,'{{csrf_token()}}')">
            				@foreach($classes as $entry)
								@if (empty($chosen_class))
								  <option value='{{$entry->Id}}' >
									  {{$entry->lesson->Subject->SubjectName}}
								  </option>
								@else
								  @if ($entry->Id == $chosen_class)
									<option value='{{$entry->Id}}' selected >
									  {{$entry->lesson->Subject->SubjectName}}
									</option>
								  @else
									<option value='{{$entry->Id}}'>
									  {{$entry->lesson->Subject->SubjectName}}
									</option>
								  @endif
								@endif
            				@endforeach
            			  </select >
            			</div>
            		</div>
            		<div class='col-lg-6 col-md-6 col-sm-6 col-xs-6' style='padding:0px; margin:0px;' >
                      <div class='input-group'>
                          <label class='input-group-addon'>Pick Schedule :</label>
                              <select class= 'form-control' name='input_attendance_IdSchedule' id='select_attendance_schedule' onchange="getAttendancePhoto(this.value,'{{ csrf_token() }}')">

                                @foreach($schedules as $entry)

                                  @if (empty($chosen_schedules))
                                    <option value='{{$entry->Id}}' >
                                      {{ substr($entry->scheduleDate,0,10)}}
                                    </option>
                                  @else
                                    <option value='{{$entry->Id}}'>
                                      {{ substr($entry->scheduleDate,0,10) }}
                                    </option>
                                  @endif
                                @endforeach
                              </select>
                       </div>
            		</div>
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
	
        </form>
          <select name='input_attendance_IdPhoto' id=picker_image class="image-picker show-labels show-html" 
				onchange="photoPickerChange(this.value,'{{ csrf_token() }}');">
          </select>
         
       </div>
     </div>

   </div>

@endsection

@section('sidebar')
   <div class='well'>
	Add Attendance :
	<form action='Attendances/InsertWeb' method='POST' class='form-horizontal' id='AttendanceForm' >
	        <input type="hidden" name="_token" value="{{ csrf_token() }}">
		<label>Id_Student : </label>
    <input type=text name='input_attendance_IdStudent' id='input_id_autocomplete' class='form-control'>
    <script>
        var otocomplete = [
          'test'
        ];

        $("#input_id_autocomplete").autocomplete({
          source : otocomplete
        });
    </script>
    <label class='.checkbox-inline'>
      <input type=checkbox  value='withoutpic' name='input_attendance_without_picture'>
      Attendance without picture
   </label>
   <input type=hidden id='input_hidden_idSchedule' name='input_hidden_idSchedule' >
   <input type=hidden id='input_hidden_idPhotos' name='input_hidden_idPhotos'>
   
    <hr>
    <input type=submit value='Add Attendance' class='form-control'>
	<input type=button value='Add Attendance revisi' class='form-control' id='btnAddAttendance'>

	</form>
	Updated Attendances
	<form action='UpdateAttendance' method='POST'>
		<input type=submit value='Update Attendance' class='form-control'>
	</form>
	
	<div>
	  <div class="card" >
		 Manual Attendance Markingz
		 
	  </div>
	
	</div>


   </div>
@endsection

@extends('lecturer_main')

@section('content')


    <div class="contentContainer card" style="background-color: white;  ">

    <ul class="nav nav-tabs" >
      <li
          @if ($view_id ==0)
             class="active"
          @endif
      ><a href="0">Overview</a></li>
      <li
          @if ($view_id ==1)
             class="active"
          @endif
      ><a href="1">Detil Kehadiran</a></li>
      <li
          @if ($view_id ==2)
             class="active"
          @endif
      ><a href="2">Detil Jadwal</a></li>
    </ul>
    <div>
       <br>
    </div>

  <div class="overview" >

   @if ($view_id == 0)
   	<div style="width:100%;"> </div>

    <table class="tableData">
         <tr>
        <th></th><th>Hadir </th><th>tdk hadir</th><th>Total </th><th>Persentase keakftifan </th>
      </tr>
    @foreach ($report as $entry)

      <tr>
      	<td class="table_cell">
      		{{$entry["SubjectName"]}}
      	</td>
      	<td class="table_super_small_cell"> <span class="emphasis_good"> {{$entry["NoOfTaggedPhoto"]}}</span> </td>
        <td class="table_super_small_cell"> <span class="emphasis_bad">{{$entry["NoOfUntaggedPhoto"]}} </span> </td>
      	<td class="table_super_small_cell">	{{ ($entry["NoOfTaggedPhoto"]+$entry["NoOfUntaggedPhoto"]) }} </td>

        <td class="table_smaller_cell">
          {{ $entry["TotalAverageEngagement"]}}
        </td>
      </tr>
    @endforeach
    </table>

   @elseif ($view_id == 1)

     <form class='form-inline' method="get"  action="1" align="right">

      <select class= 'form-control'  id="filter_class" name="filter_class" >
        @foreach ($classes as $entry)
         @if ($classid == $entry->Id)
           <option value='{{ $entry->Id }}' selected >{{  $entry->SubjectName ."-". $entry->ClassNumber }} </option>
         @else
           <option value='{{ $entry->Id }}'>{{  $entry->SubjectName ."-". $entry->ClassNumber }} </option>
         @endif
        @endforeach
      </select>
      <input type=hidden id='scheduleid' value='{{$scheduleid}}' />

      <select class='form-control' id='filter_schedule' name="filter_schedule">
        <option> Silahkan pilih kelas </option>
      </select>
      <input type=submit value='Filter' class='form-control form-control-sm'>
    </form>


    <div  class=flex-container>
      @foreach ($photolist as $entry)
        <div class="cardItem coba">
        <div class="card" >
          <img class="card-img-top" src="{{$entry->Path}}" alt="Card image" style="width:100%;">
          <div class="card-body">
              <h4 class="card-title">{{$entry->UserId}}&nbsp</h4>
              <span style="display:inline;">
              <button class="btn btn-primary cardbutton" style="float:left; margin-right:2px;" onclick="showModalKehadiran({{$entry->Id_Photos}})">T</button>
              <button class="btn btn-primary cardbutton" style="float:left;" onclick="deleteKehadiran( {{$entry->Id }} );" >U</button>
              </span>
             <a href="#" class="btn btn-primary cardbutton" onclick="deleteKehadiran( {{$entry->Id }} );" style="display:none;">Un-Tag</a>
          </div>
        </div>
        <div class="button-x" onclick="deleteFoto({{$entry->Id_Photos}});" >
            <i class="glyphicon glyphicon-remove-circle"></i>
        </div>
      </div>
      @endforeach
    </div>
    @if ($photolist != null)
      {{$photolist->appends($_GET)->links()}}
    @endif

   @elseif ($view_id == 2)

    <form class='form-inline' method="get"  action="2" align="right">
      <select class= 'form-control'  id="filter_class_schedule" name="filter_class_schedule" >
        @foreach ($classes as $entry)
         @if ($classid == $entry->Id)
           <option value='{{ $entry->Id }}' selected >{{  $entry->SubjectName ."-". $entry->ClassNumber }} </option>
         @else
           <option value='{{ $entry->Id }}'>{{  $entry->SubjectName ."-". $entry->ClassNumber }} </option>
         @endif
        @endforeach
      </select>
      <input type=hidden id='scheduleid' value='{{$scheduleid}}' />
      <input type=hidden id='classid' value='{{$classid}}' />
      <input type=submit value='Filter' class='form-control form-control-sm'>
    </form>


  <div  class=flex-container>
    <table class="tableData">
      <tr> <th>Id</th><th>Kelas </th> <th> Tanggal </th> <th></th></tr>
      @foreach($schedulelist as $entry)
        <tr>
          <td>  {{$entry->Id }}</td>
          <td> {{$entry->SubjectName . "-" . $entry->ClassNumber }}</td>
          <td > {{$entry->ScheduleDate}}</td>
          <td>
            <button type=button class="btn  btnKelasku" onclick="deleteJadwal({{$entry->Id}})">
              <i class="glyphicon glyphicon-ban-circle" style="color:white;" ></i> Delete
            </button>
          </td>
        </tr>
      @endforeach
    </table>

      @if ($schedulelist != null)
        {{$schedulelist->appends($_GET)->links()}}
      @endif

  </div>
   @endif

  </div>
  </div>

 <!--modal-->
 <div class="modal fade" id="modalAttendance">
   <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Masukkan NRP atau Nama mahasiswa</h4>
      </div>
      <div class="modal-body">
        <input type=hidden id=id_photo_hidden />
        <select class="form-control selectpicker" style="width:100%;" name="select_nrp_kehadiran" id="select_nrp_kehadiran" data-live-search="true">
          @foreach ($students as $entry)
            <option value="{{$entry->Id}}" > {{ $entry->UserId . " " . $entry->Name }} </option>
          @endforeach
        </select>
      </div>
      <script>
			  $(".selectpicker").select2();
		  </script>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="tambahKehadiran();">Tambah Kehadiran</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
 </div>

 <div class="modal fade" id="modalScheduleDate"  role="dialog">
 <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" >

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Masukkan Jadwal Baru </h4>
      </div>
      <div class="modal-body">
        <p></p>
        <label>Pilih Tanggal untuk ditambahkan</label>

        <div class="container">
          <div class="row">
              <div class='col-sm-6'>
              	  <div class="form-group">
                      <div class='input-group date' id='datetimepicker1'>
                      	 <input id='datepickertext' type='text' class="form-control" style="display: inline-block; float:left;" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                  </div>
              </div>
              <script type="text/javascript">
                  $(function () {
                      $('#datetimepicker1').datetimepicker({
                		  format : 'YYYY-MM-DD HH:mm'
                	});
                  });
              </script>
          </div>
      </div>




      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="tambahJadwal();">Tambah Jadwal</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>

    </div>

  </div>
 </div>

@endsection


@section('sidebar')
  <div class="container">
  <div class="well upload_menu">
    <h4 style="text-align: center;width: 100%;">Upload Kehadiran</h4>

     <form action="../../SchedulePhoto/InsertService" method="post" enctype="multipart/form-data" id="js-upload-form">
        <label style="padding:4px;">Kelas : </label>
        <select name="class_kehadiran" class="form-control" id="class_kehadiran">
             @foreach ($classes as $entry)
             <option value={{$entry->Id}}>{{  $entry->SubjectName ."-". $entry->ClassNumber }} </option>
            @endforeach
        </select>
        <label style="padding: 4px;">Jadwal : </label>
        <div class="row" >
          <div class="col-md-8"  style="padding:5px;">
            <select name="select_schedule_name" class="form-control" id="select_schedule_name" >
                <option>Pick a Class </option>
            </select>
          </div>
          <div class="col-md-4" style="padding:5px;">
            <button type=button  class="btn btn-default"  data-toggle="modal" data-target="#modalScheduleDate">
              Jadwal baru
            </button>
          </div>
        </div>
        <div>&nbsp</div>
        <div class="form-inline">
          <div class="custom-file">
            <input type="file"  name="input_schedulePhotos_photos" multiple class="custom-file-input" id="input_schedulePhotos_photos" >
            <label class="custom-file-label cfl"  for="customFile" id=mylabel>Choose file</label>
          </div>
        </div>


      </form>
      <input type="submit" class="btn btn-submit btn-default" style="width: 100%;" value="Tambah Gambar" onclick="InsertPhoto();">

      <!-- Drop Zone -->
      <div style="display:none;">
      <h4>Or drag and drop files below</h4>
      <div class="upload-drop-zone" id="drop-zone">
        Just drag and drop files here
      </div>
    </div>

  </div>
	</div>
@endsection



@section('extra_script')
  <link rel="stylesheet" href="{{URL::to('css/common.css')}}" />
  <script src="{{URL::to('js/upload_multiple.js')}}" > </script>
  <script>
    var user_id = 33;
  </script>
  <script src="{{URL::to('js/lecturer_kehadiran.js')}}" > </script>

  <link rel="stylesheet" href="{{URL::to('css/bootstrap-datetimepicker-standalone.css')}}" />
  <link rel="stylesheet" href="{{URL::to('css/bootstrap-datetimepicker.css')}}" />
  <link rel="stylesheet" href="{{URL::to('css/bootstrap-datetimep‌​icker.min.css')}}" />
  <script src="{{URL::to('js/bootstrap-datetimepicker.min.js')}}" > </script>



@endsection

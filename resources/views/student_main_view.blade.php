@extends('main')

@section('content')

  <h1> Kelas-Ku   </h1>  
  
  <div>
    KelasKu is an integrated software intended to digitalize and improve classroom
	experience for students. it is a product of Maranatha Christian University.

  </div>
@endsection

@section('sidebar')
  <div class="well">
   <h4> please enter your login credential : </h4>
     <form action="/Users/Login", method='POST'>
       <label>User Id : </label>
       <input type=text name='input_user_id' class="form-control" >
       <label>User Passwords : </label>
       <input type=password name='input_user_password' class="form-control" >
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <br>
       <input type=submit value="Login" class="button form-control">  
     </form>
     <form action="Register", method='POST'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
        <input type=submit value="Register" class="button form-control">
     </form>

  </div>

@endsection

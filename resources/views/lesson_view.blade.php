@extends('main')

@section('content')
  <form action='Lessons' method="get" class="form-inline" align="right">
    <label>Filter : </label>
    <select name="select_lesson_filter" class="form-control form-control-sm">
      <option value=0> All </option>
      @foreach ($Semesters as $names)
        <option value={{$names->Id}}> {{$names->semestername->Name ."-" . $names->Year}}  </option>
      @endforeach
    </select>
    <input type=submit value=Filter class='form-control form-control-sm'>
  </form>
  
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Semester</th>
      <th>Subject</th>

      <th></th>
    </tr>

  @foreach($Lessons as $entry)
    <tr>

       <td>{{$entry->Id}}</td>
       <td>{{$entry->semester->semestername->Name . "-" . $entry->semester->Year . "/" .
            ($entry->semester->Year +1 ) }}</td>
       <td>{{$entry->subject->SubjectName}}</td>

     <td>
         <form action="Lessons/Delete/{{$entry->Id}}" method='POST'>
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  @endforeach
 </table>
 {{$Lessons->appends($_GET)->links()}}
@endsection

@section('sidebar')
   <div class="well">
	  <h4> Lesson Information : </h4>
      <form action="Lessons\Insert", method='POST'>
        <label>Semester : </label>
        <select name='select_lesson_semester' class='form-control' >
            @foreach ($Semesters as $names)
            <option value={{$names->Id}}> {{$names->semestername->Name ."-" . $names->Year}}  </option>
          @endforeach
        </select>
        <label>Subject : </label>
        <select name='select_lesson_subject' class='form-control' >
            @foreach ($Subjects as $entry)
            <option value={{$entry->Id}}> {{$entry->SubjectName}}   </option>
          @endforeach
        </select>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <br>
        <input type=submit value="Insert New Lesson" class="button form-control">
    </form>
   </div>
@endsection

@extends('main')

@section('content')
  <form action='Semesters' method="get" class="form-inline" align="right">
    <label>Filter : </label>
    <select name="select_year_filter" class="form-control form-control-sm">
        <option value=0> All </option>
      @foreach ($semesterall as $entry)
        <option value={{$entry->Year}}> {{$entry->Year}}  </option>
      @endforeach
    </select>
    <input type=submit value=Filter class='form-control form-control-sm'>
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Semester Name</th>
	  <th>Semester Year</th>
	  <th>Start Date</th>
	  <th>End Date</th>

      <th></th>
    </tr>

  @foreach($semesters as $entry)
    <tr>

       <td>{{$entry->Id}}</td>
       <td>{{$entry->semestername->Name}}</td>
	     <td>{{$entry->Year}}</td>
	     <td>{{date_format(date_create($entry->StartDate),'d-m-Y H:i:s') }}</td>
	     <td>{{date_format(date_create($entry->EndDate),'d-m-Y H:i:s') }}</td>


       <td>
         <form action="Semesters/Delete/{{$entry->Id}}" method='POST'>
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  @endforeach
 </table>
 {{$semesters->appends($_GET)->links()}}
@endsection

@section('sidebar')
   <div class="well">
	  <h4> Semester Information : </h4>
      <form action="Semesters\Insert", method='POST'>
        <label>Semester Name : </label>
		<select name='select_semestername' class='form-control' >
		    @foreach ($semesternames as $names)
			  <option value={{$names->Id}}> {{$names->Name}}  </option>
			@endforeach
		</select>
        <label>Semester Year : </label>
        <input type=text name='input_semester_year' class='form-control'>
		<div class="row">
			<div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
				<label>Start Date : </label>
				<input type='date' name='input_semester_startDate' class='form-control' />
			</div>
			<div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
				<label>End Date : </label>
				<input type='date' name='input_semester_endDate' class='form-control' />
			</div>
		</div>
		<br>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type=submit value="Insert New Semester" class="button form-control">
    </form>
   </div>
@endsection

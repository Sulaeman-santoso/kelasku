<b>KELASKU APPLICATION PRIVACY POLICY</b>



<div style="text-decoration: underline;">
INDONESIA:
</div>

<div>
Kebijakan Privasi ini mencakup penggunaan Anda atas aplikasi ini. Aplikasi ini tidak mengumpulkan, menyimpan, atau membagikan informasi pribadi, atau apapun yang berhubungan dengan perangkat Anda. Kami tidak mengumpulkan statistik apapun, tren, ataupun melacak pergerakan/lokasi Anda.
</div>
--------------------------------------------------------------------------------------------------------------------

<div style="text-decoration: underline;">
ENGLISH:
</div>

<div>
This Privacy Policy covers your use of this application. This app does not collect, store, or share any personal information, or anything related to your device. We do not collect any statistics, trends, neither do we track user movements.
</div>
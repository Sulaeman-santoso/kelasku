@extends('main')

@section('content')
<form action="Subjects" method="get" class="form-inline" align="right">

    <input type=text name="input_subject_search" class='form=control' placeholder="subject id or name">
    <input type=submit value='Search' class='form-control'>
	
    
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Subject Id</th>
      <th>Subject Name</th>

      <th></th>
    </tr>

  @foreach($Subjects as $entry)
    <tr>

       <td>{{$entry->Id}}</td>
       <td>{{$entry->SubjectId}}</td>
       <td>{{$entry->SubjectName}}</td>

       <td>
         <form action="Subjects/Delete/{{$entry->Id}}" method='POST'>
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  @endforeach
 </table>
 {{  $Subjects->appends($_GET)->links()}}
@endsection

@section('sidebar')
   <div class="well">
	  <h4> Subjects Information : </h4>
      <form action="Subjects\Insert", method='POST'>
        <label>Subject Id : </label>
        <input type=text name='input_subject_id' class='form-control'>
        <br>
        <label>Subject Name : </label>
        <input type=text name='input_subject_name' class='form-control'>
        <br>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <br>
        <input type=submit value="Insert New Subject" class="button form-control">
    </form>
   </div>
@endsection

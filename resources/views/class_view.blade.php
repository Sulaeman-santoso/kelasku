@extends('main')

@section('content')
  <form action="Classes" method="get" class='form-inline' align="right">
    <!--<input type=checkbox class="form-check-input" id='check_active' >
    <label class='form-check-label' for="check_active" >active only </label>-->
    Filter :
    <select name='select_classes_semester' class='form-control'>
      <option value=0>All </option>
      @foreach ($semesters as $names)
        <option value={{$names->Id}}  {{($old->input('select_classes_semester') == $names->Id)?'Selected':'' }} > {{$names->semestername->Name ."-" . $names->Year."/". ($names->Year+1) }}  </option>
      @endforeach
    </select>
	<select name='select_classes_class' class='form-control'>
      <option value=0>All </option>
      @foreach ($lecturers as $dosen)
        <option value={{$dosen->Id}}  {{($old->input('select_classes_class') == $dosen->Id)?'Selected':'' }}   > {{$dosen->Name }}  </option>
      @endforeach
    </select>
    <input type=submit value=Filter class='form-control form-control-sm'>
  </form>
  <table class="table table-responsive">
    <tr>
      <th>Id</th>
      <th>Lesson</th>
      <th>Lecturer</th>
      <th>Class Number</th>

      <th></th>
    </tr>

  @foreach($classes as $entry)
    <tr>

       <td>{{$entry->Id}}</td>
       <td>{{$entry->lesson->Subject->SubjectName ."-". $entry->lesson->semester->semestername->Name ." ". $entry->lesson->semester->Year . "/". ($entry->lesson->semester->Year + 1) }}</td>
       <td>{{$entry->lecturer->Name}}</td>
       <td> {{$entry->ClassNumber}} </td>

       <td>
         <form action="Classes/Delete/{{$entry->Id}}" method='POST'>
           <input type="hidden" name="_token" value="{{ csrf_token() }}">
           <input type=submit value=delete >
         </form>
       </td>
   </tr>
  @endforeach
 </table>
 {{$classes->appends($_GET)->links()}}
@endsection

@section('sidebar')
   <div class="well">
	  <h4> Class Information : </h4>
      <form action="Classes\Insert", method='POST'>
        <label>Semester : </label>
        <select name='select_classes_lesson' class='form-control' >
            @foreach ($lessons as $entry)
            <option value={{$entry->Id}}> {{$entry->semester->semestername->Name ."-" .$entry->semester->Year . "/" . ($entry->semester->Year+1) . " ". $entry->subject->SubjectName}}  </option>
          @endforeach
        </select>
        <label>Subject : </label>
        <select name='select_classes_lecturer' class='form-control' >
            @foreach ($lecturers as $entry)
            <option value={{$entry->Id}}> {{$entry->Name}}   </option>
          @endforeach
        </select>
        <label>Class Number</label>
        <input type=text class='form-control' name='input_class_classNumber'>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <br>
        <input type=submit value="Insert New Lesson" class="button form-control">
    </form>
   </div>
@endsection

import axios from 'axios';
import { Datetime } from 'vue-datetime';
import FileUpload from 'v-file-upload';
import 'vue-datetime/dist/vue-datetime.css'
import VModal from 'vue-js-modal';
import tag_grid from './components/TagsTable.vue';


$(document).ready(function() {
  Vue.use(VModal);
  var ex = require('./components/ExampleComponent.vue');

//  const VueUploadComponent = require('vue-upload-component')
//  Vue.use(FileUpload);
  const Ann_View = new Vue({
    el: '#card_vue_announcement',
    components : {
      dtp : Datetime,
      tag_view : tag_grid,
    },
    props : {
      data : {
        default() {
            return "default data";
        }
      }
    },
    data : {
      id : '33',
      img_src : '',
    },
    mounted : function() {
      this.data = this.$el.attributes.data.value;
      console.log('isi data : ' + this.data);
    },
    methods : {
      seeDetail : function(id,src) {
        this.img_src = src;
        this.$modal.show('hello-world');
      },
      deleteService : function(id) {
        //alert('delete service with id :  ' + id  + " "  + this.data);
        let _data = new FormData();
        _data.append('id_ann',id);

        axios.post('../../Announcements/Delete',_data)
        .then((response)=>{
          var obj = JSON.parse(JSON.stringify(response.data));
          alert(obj.message);
          console.log(response);
          location.reload();
        })
        .catch((response)=>{
          alert(JSON.stringify(error));
          console.log(error.message);
        });
      }
    }
  });



  const Ann_Module = new Vue( {
  	el : '#announcement_add_module',
      components : {
        ex,
        dtp: Datetime,
      },
      mounted : function() {
        this.id = this.$el.attributes.chosen_user.value;
        console.log('chosen user :  ' + this.id);
      },
      data  :  {
        id : '33',
        message: 'Aww Hell Yeah',
        title : 'Judul',
        content : 'Isi',
        tags : '',
        input_filephoto : null,
        isSaving : false,
        isInitial : true,
        uploadFieldName : 'uploaded_files',
        publish_date : new Date().toJSON().slice(0,10).replace(/-/g,'-') ,
        expire_date : new Date().toJSON().slice(0,10).replace(/-/g,'-') ,
      },
      methods : {

      	AddAnnouncement: function() {
          //using axios w/e that means :D

          let toSend = new FormData();
          toSend.append('id_user', this.id);
          toSend.append('title', this.title);
          toSend.append('content', this.content);
          toSend.append('input_filephoto', this.input_filephoto);

          let revised_date = this.publish_date.replace('T',' ');
          revised_date = revised_date.substring(0, 19);
          toSend.append('publish_date', revised_date);

          revised_date = this.expire_date.replace('T',' ');
          revised_date = revised_date.substring(0, 19);
          toSend.append('expire_date', revised_date);
          toSend.append('tags',this.tags);

          const config ={
            header : {
              'X-CSRF-TOKEN': document.querySelector('#token').getAttribute('value'),
              'content-type': 'multipart/form-data'
             }
          }

          axios.post('../../Announcements/Insert',toSend,config)
          .then(function(response) {
            //alert(JSON.stringify(response.data));
            var obj = JSON.parse(JSON.stringify(response.data));
            alert(obj.message);
            console.log(response);
            location.reload();
          })
          .catch(function (error) {
            alert(JSON.stringify(error));
            console.log(error.message);
          });

          console.log('function ends');
      	 },
        fileUpload : function(event){
          console.log(event);
          this.input_filephoto = event.target.files[0];
          console.log(this.input_filephoto);
        },
        OnSubmit : function(event) {

        }
      }
  });
  const Tag_Module  = new Vue({
  	 el :'#tag_add_module',
  	data : {message : 'Aww Hell No ' }
  });


});

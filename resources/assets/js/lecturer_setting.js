import axios from 'axios';
import { Datetime } from 'vue-datetime';
import FileUpload from 'v-file-upload';
import 'vue-datetime/dist/vue-datetime.css'
import VModal from 'vue-js-modal';

$(document).ready(function() {

  Vue.component('import', require('./components/ImportClass.vue'));
  Vue.component('lreport', require('./components/LecturerClassReport.vue'));

  const VueApp = new Vue({
    el : "#vue_content",
  });

  const VueSideApp = new Vue({
    el: '#vue_sidebar',
  });

});

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
    //
	protected $table = 'enrollments';
	protected $primaryKey = 'Id';
	
	function classes() {
		return $this->belongsTo('App\Classes','Id_Class','Id');
	}
	
	function student() {
		return $this->belongsTo('App\Users','Id_User_Student','Id');
	}
	
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    //
    protected $table='schedules';
    protected $primaryKey = 'Id';
    protected $dates = ['scheduleDate'];
    
    public function formattedDate(){
      return $this->scheduleDate ? $this->scheduleDate->format('d.m.Y G:i:s') : null;
	}


    public function classes() {
      return $this->belongsTo('App\Classes','Id_Class','Id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Announcement_Tag extends Model
{
    //
    protected $table='announcement_tag';
    protected $primaryKey = ['Id_Tag','Id_Announcement'];
    public $incrementing = false;

    protected function setKeysForSaveQuery(Builder $query)
    {
        foreach($this->primaryKey as $pk) {
            $query = $query->where($pk, $this->attributes[$pk]);
        }
        return $query;
    }

    function tag() {
      return $this->belongsTo('App\Tags','Id_Tag','Id');
    }

    function announcement() {
      return $this->belongsTo('App\Announcements','Id_Announcement','Id');
    }
}

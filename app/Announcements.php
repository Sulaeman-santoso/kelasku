<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcements extends Model
{
    //
    protected $table='announcements';
    protected $primaryKey = 'Id';

    function photos() {
      return $this->hasOne('App\Photo','Id','Id_Photo');
    }

    function tags() {
      return $this->hasMany('App\Announcement_Tag');
    }
}

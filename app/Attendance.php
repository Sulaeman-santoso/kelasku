<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //
    protected $table='attendances';
    protected $primaryKey = 'Id';

	function photos() {
		return $this->belongsTo('App\Photo','Id_Photo','Id');
	}
	
	function student() {
		return $this->belongsTo('App\Users','Id_Student','Id');
	}
	
	function schedule() {
		return $this->belongsTo('App\Schedule','Id_Schedule','Id');
	}
	

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class User_Tag extends Model
{
    //
    protected $table='user_tag';
    protected $primaryKey = ['Id_User','Id_Tag'];
    public $incrementing = false;

    protected function setKeysForSaveQuery(Builder $query)
    {
        foreach($this->primaryKey as $pk) {
            $query = $query->where($pk, $this->attributes[$pk]);
        }
        return $query;
    }

    function tag() {
      return $this->belongsTo('App\Tags','Id','Id_Tag');
    }

    function user() {
      return $this->belongsTo('App\User','Id','Id_User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchedulePhoto extends Model
{
    //
    protected $table='schedule_photos';
    protected $primaryKey = 'Id';

    function schedule() {
      return $this->belongsTo('App\Schedule','Id_Schedule','Id');
    }

    function photos() {
      return $this->belongsTo('App\Photo','Id_Photo','Id');
    }

}

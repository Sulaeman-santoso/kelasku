<?php

namespace App\Http\Controllers;

use App\SemesterName;
use Illuminate\Http\Request;

class SemesterNameController extends Controller
{
	
	public function Get() {
		$temp = SemesterName::all();
		return View('semester_name_view')->with('semesterNames',$temp);
	}
	
	public function Insert(Request $request) {
		$temp = new SemesterName;
		
		$temp->Name= $request->input('input_semestername_name');
		$temp->save();
		
		$messages = 'Insert Semester Name Done';
		return Redirect('SemesterNames')->with('messages',$messages);
	}
	
	public function Delete($Id) {
		$temp = SemesterName::find($Id);
		$result = $temp->delete();
		$messages = 'semester name entry deleted';
		return Redirect('SemesterNames')->with('messages', $messages);
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SemesterName  $semesterName
     * @return \Illuminate\Http\Response
     */
    public function show(SemesterName $semesterName)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SemesterName  $semesterName
     * @return \Illuminate\Http\Response
     */
    public function edit(SemesterName $semesterName)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SemesterName  $semesterName
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SemesterName $semesterName)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SemesterName  $semesterName
     * @return \Illuminate\Http\Response
     */
    public function destroy(SemesterName $semesterName)
    {
        //
    }
}

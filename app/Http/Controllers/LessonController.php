<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Semester;
use App\Subject;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Get(Request $request)
    {
       $semesterid = 0;
       if($request->input('select_lesson_filter')!=null){
         $semesterid = $request->input('select_lesson_filter');
       }
       if($semesterid != 0) {
          $lesson = Lesson::where('Id_Semester',$semesterid)->paginate(5);
       }
       else{
           $lesson = Lesson::paginate(5);
       }
        //

        $subjects = Subject::all();
        $semesters = Semester::where('EndDate','>',NOW())->get();
        //print date('Y-m-d');
	    Return View('lesson_view')->with('Lessons', $lesson)->with('Subjects',$subjects)->with('Semesters',$semesters);
    }

	public function Insert(Request $request) {
		$temp = new Lesson;
    $temp->Id_Subject = $request->input('select_lesson_subject');
    $temp->Id_Semester = $request->input('select_lesson_semester');
		$temp->save();
		$messages = 'lesson entry succeeded';
		return Redirect('Lessons')->with('messages',$messages);
	}

	public function Delete($Id) {
		$temp = Lesson::find($Id);
		$temp->delete();
		$messages = 'Lesson entry deleted';
	  return 	Redirect('Lessons')->with('messages',$messages);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function show(Lesson $lesson)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesson $lesson)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lesson $lesson)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lesson  $lesson
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lesson $lesson)
    {
        //
    }
}

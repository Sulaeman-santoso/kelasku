<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Enrollment;
use App\Classes;
use App\Semester;
use App\Users;
use App\Engagement;
use App\Schedule;


class PageController extends Controller
{
    //

   function GetPrivacyPolicies() {

    return View('privacy_policies_view');
   }

   function GetMain() {
     //echo "worked";

     session()->flush();
     return View('entry_main');
	 //echo "Something something";
	 // return View('lecturer_entry');
   }

   function GetStudentWeb() {
	  return View('student_main');
   }

   function GetClientWeb() {
     return View('client_main');
   }

   function GetLecturerSettings(Request $request) {
     $Id = $request->session()->get('lecturer_id');
     //$Id = Session::get('lecturer_id');
     //if ($Id == "") {$Id = 33;}
//     $Id = 33;
     $user = Users::where('Id','=',$Id)->first();
     $chosen_option = $request->input('chosen_option');
     if ($chosen_option == null )
     {
       $chosen_option = 1;
     }

     if($chosen_option == 1){
     $class_teached = DB::table('classes')
                    ->join('lessons','classes.Id_Lesson','lessons.Id')
                    ->join('semesters','lessons.Id_Semester','semesters.Id')
                    ->join('subjects','lessons.Id_Subject','subjects.Id')
                    ->select('classes.Id','subjects.SubjectName','classes.ClassNumber')
                    ->where('Id_User_Lecturer','=',$Id)
                    ->where('semesters.EndDate','>',NOW())
                    ->get();
    }
    else {
      $class_teached = DB::table('classes')
                     ->join('lessons','classes.Id_Lesson','lessons.Id')
                     ->join('semesters','lessons.Id_Semester','semesters.Id')
                     ->join('subjects','lessons.Id_Subject','subjects.Id')
                     ->select('classes.Id','subjects.SubjectName','classes.ClassNumber')
                     ->where('Id_User_Lecturer','=',$Id)
                     ->get();
    }

  //  echo "chosen lecturer " . $Id . "<hr>";
  //  print_r($class_teached);
     return View('lecturer_setting_view')->with('chosen_option',$chosen_option)->with('classes',$class_teached)->with('users',$user);
   }


   function GetClientLecturer(Request $request,$Id) {
     session(['lecturer'=>'Yes']);

     if(!$request->session()->has('lecturer_id')){
        $request->session()->put('lecturer_id', $Id);
     }
     $Id = $request->session()->get('lecturer_id');
     //$Id = Session::get('lecturer_id');
     if ($Id == "") {$Id = 33;}

     $user = Users::where('Id','=',$Id)->first();
  //   echo "id " . $Id;

     $class_teached = DB::table('classes')
                    ->join('lessons','classes.Id_Lesson','lessons.Id')
                    ->join('semesters','lessons.Id_Semester','semesters.Id')
                    ->join('subjects','lessons.Id_Subject','subjects.Id')
                    ->select('classes.Id','subjects.SubjectName','classes.ClassNumber')
                    ->where('Id_User_Lecturer','=',$Id)
                    ->where('semesters.EndDate','>',NOW())
                    ->get();

/*
     foreach ($class_teached as $entry) {
     	echo $entry->Id . " " . $entry->SubjectName;
     }
*/
    //echo "what is this --";
     return View('lecturer_entry')->with('classes',$class_teached)->with('users',$user);
   }

   function GetInstruction() {
     return "instruction";
   }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Enrollment;
use App\Classes;
use App\Semester;
use App\Users;
use App\Engagement;
use App\Schedule;


class EngagementController extends Controller
{


	public function GetLecturerKeaktifan(Request $request,$viewId) {

		
		if ($request->session()->has('lecturer_id')) {
		    	//
		    $Id = $request->session()->get('lecturer_id');
		}
		else {
		    $Id = 33;
		}

		 $class_teached = DB::table('classes')
	                ->join('lessons','classes.Id_Lesson','lessons.Id')
	                ->join('semesters','lessons.Id_Semester','semesters.Id')
	                ->join('subjects','lessons.Id_Subject','subjects.Id')
	                ->join('users','classes.Id_User_Lecturer','users.Id')
	                ->select('classes.Id','subjects.SubjectName','classes.ClassNumber','subjects.SubjectName as Subjects','users.Name as Lecturer')
	                ->where('Id_User_Lecturer','=',$Id)
	                ->where('semesters.EndDate','>',NOW())
	                ->get();

	    $chosen_class = $request->input('select_kehadiran_class');
			
		$enrollment = DB::table('enrollments')
								->join('users','enrollments.Id_User_Student','users.Id')
								->join('classes','enrollments.Id_Class','classes.Id')
								->select('users.Id','users.UserId','users.Name')
								->where('classes.Id','=',$chosen_class)
								->get();


	    $schedules = DB::table('schedules')->get();

	    $engagementsReport = DB::select('select c.Id,count(distinct u.Id) as JmlUser,c.ClassNumber,sb.SubjectName ,coalesce(sum(egg.Value),0) as engagementValue ,count(distinct sc.Id) as jmlSchedule, 
			( coalesce(sum(egg.Value),0)/count(distinct sc.Id)) as avgEngagement   from enrollments err 
			join users u on (err.Id_User_Student = u.Id)
			join classes c on (err.Id_Class =c.Id)
		    join schedules sc on (sc.Id_Class = c.Id)
		    join lessons l on (c.Id_Lesson = l.Id)
		    join subjects sb on (l.Id_Subject = sb.Id)
		    join semesters s  on (l.Id_Semester = s.Id)
		    left join engagements egg on ((egg.Id_Enrollment = err.Id) and (egg.Id_Schedule = sc.Id))
		    where ((s.EndDate > NOW()) and (c.Id_User_Lecturer = '. $Id . '))
		    group by c.Id,sb.SubjectName,c.ClassNumber');

	     $engagementsDetailReport = [];
	     $pageLimit = 7;
	     $engagementSumReport = [];

	     if ($viewId == 1) {
				$chosen_class = $request->input('select_kehadiran_class_2');
				//echo ("chosen class : " . $chosen_class);

	     	if ($chosen_class != ""){
	     		$engagementSumReport  = DB::table('enrollments')
	     				->join('engagements','engagements.Id_Enrollment','enrollments.Id')
	     				->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')
	     				->join('users','enrollments.Id_User_Student','users.Id')
	     				->join('classes','enrollments.Id_Class','classes.Id')
	     				->join('lessons','classes.Id_Lesson','lessons.Id')
							->join('subjects','lessons.Id_Subject','subjects.Id')
							->groupby('enrollments.Id','users.UserId','users.Name','subjects.SubjectName','classes.ClassNumber')
							->select('enrollments.Id','users.UserId','users.Name','subjects.SubjectName','classes.ClassNumber',DB::raw('sum(engagements.Value) as total_engagement'))
							->where('classes.Id','=',$chosen_class)
	     				->paginate($pageLimit);
	     	}
				 else { //null class
					
	     		$engagementSumReport  = DB::table('enrollments')
							 ->join('engagements','engagements.Id_Enrollment','enrollments.Id')
							 ->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')
							 ->join('users','enrollments.Id_User_Student','users.Id')
							 ->join('classes','enrollments.Id_Class','classes.Id')
							 ->join('lessons','classes.Id_Lesson','lessons.Id')
							 ->join('subjects','lessons.Id_Subject','subjects.Id')
							 ->groupby('enrollments.Id','users.UserId','users.Name','subjects.SubjectName','classes.ClassNumber')
							 ->select('enrollments.Id','users.UserId','users.Name','subjects.SubjectName','classes.ClassNumber',DB::raw('sum(engagements.Value) as total_engagement'))
							 ->orderBy('total_engagement','desc')
	     				->paginate($pageLimit);
	     	}
				//print_r($engagementSumReport);
	     
	     }
	     else if ($viewId == 2){
	     	if ($chosen_class != ""){
					$chosen_class = $request->input('select_kehadiran_class');

				//		echo ("chosen class = " . $chosen_class);
	     		$engagementsDetailReport  = DB::table('engagements')
	     				->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')
	     				->join('enrollments','engagements.Id_Enrollment','enrollments.Id')
	     				->join('users','enrollments.Id_User_Student','users.Id')
	     				->join('classes','enrollments.Id_Class','classes.Id')
	     				->join('lessons','classes.Id_Lesson','lessons.Id')
	     				->join('subjects','lessons.Id_Subject','subjects.Id')
	     				->select('engagements.Id as IdEng','subjects.SubjectName','classes.Id','classes.ClassNumber','users.UserId','users.Name','engagement_types.engagement_name','engagements.Value')
	     				->where('classes.Id','=',$chosen_class)
	     				->paginate($pageLimit);
	     	}
	     	else { //null class
	     		$engagementsDetailReport  = DB::table('engagements')
	     				->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')
	     				->join('enrollments','engagements.Id_Enrollment','enrollments.Id')
	     				->join('users','enrollments.Id_User_Student','users.Id')
	     				->join('classes','enrollments.Id_Class','classes.Id')
	     				->join('lessons','classes.Id_Lesson','lessons.Id')
	     				->join('subjects','lessons.Id_Subject','subjects.Id')
	     				->select('engagements.Id as IdEng','subjects.SubjectName','classes.Id','classes.ClassNumber','users.UserId','users.Name','engagement_types.engagement_name','engagements.Value')
	     				->paginate($pageLimit);
	     	}
			//	print_r($engagementsDetailReport);
	     }

	    $chosen_schedule = $request->input('');
	//print_r($engagementsDetailReport);
	return View('lecturer_keaktifan_view')->with('report_sum',$engagementSumReport)->with('chosen_class',$chosen_class)->with('report_detail',$engagementsDetailReport)->with('report',$engagementsReport)->with('view_id',$viewId)->with('classes',$class_teached);
	}


	public function getSemesterReportService(Request $request) {

	
	}


public function getSemesterLecturerReportWeb(Request $request) {
  		$chosen_one = $request->input('input_chosen_lecturer');

  		$meetingReport = DB::table('classes')
  						->join('schedules','schedules.Id_Class','classes.Id')
  						->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
	  					->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
  						->join('lessons','classes.Id_Lesson','lessons.Id')
						->join('subjects','lessons.Id_Subject','subjects.Id')
						->join('semesters','lessons.Id_Semester','semesters.Id')
	  					->leftjoin('attendances','attendances.Id_Photo','schedule_photos_detail.Id_Photo')
	  					->leftjoin('users','attendances.Id_Student','users.Id')
	  					->where('classes.Id_User_Lecturer','=',$chosen_one)
	  					->where('semesters.EndDate', '>', NOW())
	  					->groupBy('classes.Id','classes.Id_User_Lecturer','subjects.SubjectName','classes.ClassNumber')
	  					->select('classes.Id as Id_Class','classes.Id_User_Lecturer as Lecturer',
	  							'subjects.SubjectName as MK','classes.ClassNumber',
	  					DB::raw('sum( !isnull(attendances.Id)) as TaggedFace,
								 sum( isnull(attendances.Id)) as unTaggedFace,
		    					 (sum( !isnull(attendances.Id)) + sum( isnull(attendances.Id))) as faceNum,
								 count(distinct schedules.Id) as jmlPertemuan'))
	  					->get()->toArray();

	  	//echo json_encode($meetingReport);
	  	$resultArray = array();

	  	foreach($meetingReport as $entry) {
			$temp["Id_Class"] = $entry->Id_Class;
			$temp["Lecturer"] = $entry->Lecturer;
			$temp["SubjectName"] = $entry->MK;
			$temp["ClassNumber"] = $entry->ClassNumber;
			$temp["NoOfUntaggedPhoto"] = $entry->unTaggedFace;
			$temp["TotalAverageEngagement"] = 0;
			$temp["TotalMeeting"] = $entry->jmlPertemuan;
			$temp["jmlMahasiswa"] = 0;
			$resultArray[$entry->Id_Class] = $temp;
		}

		//perlu satu query lagi buat cari jml orang yang perlu
		//echo json_encode($resultArray);

		$engagementsReport = DB::select('select c.Id,u.Name ,coalesce(sum(egg.Value),0) as engagementValue ,count(distinct sc.Id) as jmlSchedule, 
			( coalesce(sum(egg.Value),0)/count(distinct sc.Id)) as avgEngagement   from enrollments err 
			join users u on (err.Id_User_Student = u.Id)
			join classes c on (err.Id_Class =c.Id)
		    join schedules sc on (sc.Id_Class = c.Id)
		    join lessons l on (c.Id_Lesson = l.Id)
		    join subjects sb on (l.Id_Subject = sb.Id)
		    join semesters s  on (l.Id_Semester = s.Id)
		    left join engagements egg on ((egg.Id_Enrollment = err.Id) and (egg.Id_Schedule = sc.Id))
		    where ((s.EndDate > NOW()) and (c.Id_User_Lecturer = '. $chosen_one . '))
		    group by c.Id,u.Name,sb.SubjectName');

		foreach ($engagementsReport as $entry) {
			$temp = $resultArray[$entry->Id];
			$temp["TotalAverageEngagement"] += $entry->avgEngagement;
			$temp["jmlMahasiswa"] += 1;
			$resultArray[$entry->Id] = $temp;
		}
  	
		foreach ($resultArray as $entry) {
			$temp = $resultArray[$entry["Id_Class"]];
			if ($entry["jmlMahasiswa"] != 0){
				$temp["TotalAverageEngagement"] = 	$entry["TotalAverageEngagement"]/ $entry["jmlMahasiswa"];	
			}
			else {
				$temp["TotalAverageEngagement"] =0;
			}
			$resultArray[$entry["Id_Class"]] = $temp;
		}
		return json_encode($resultArray);  
  	}


  	public function getSemesterLecturerReport(Request $request) {
  		$chosen_one = $request->input('input_chosen_lecturer');

  		/*
		select c.Id, sb.SubjectName, sc.Id as Id_Schedule, sc.ScheduleDate, spd.Id_Photo,  
			sum( !isnull(att.Id)) as TaggedFace,
			sum( isnull(att.Id)) as noOfunTagged,
		    (sum( !isnull(att.Id)) + sum( isnull(att.Id))) as jmlFace
			from classes c 
			join schedules sc on (sc.Id_Class = c.Id)
		    join schedule_photos sp on (sp.Id_Schedule = sc.Id)
		    join schedule_photos_detail spd on (spd.Id_SchedulePhoto = sp.Id)
		    join lessons l on (c.Id_Lesson = l.Id)
		    join subjects sb on (l.Id_Subject = sb.Id)
		    join semesters s  on (l.Id_Semester = s.Id)
		    left join attendances att on (att.Id_Photo = spd.Id_Photo)
		    left join users u on (att.Id_Student = u.Id)
		    where ((c.Id_User_Lecturer = 33) AND (s.EndDate > NOW()) )
		    group by c.Id, sc.Id
  		*/

  		$meetingReport = DB::table('classes')
  						->join('schedules','schedules.Id_Class','classes.Id')
  						->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
	  					->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
  						->join('lessons','classes.Id_Lesson','lessons.Id')
						->join('subjects','lessons.Id_Subject','subjects.Id')
						->join('semesters','lessons.Id_Semester','semesters.Id')
	  					->leftjoin('attendances','attendances.Id_Photo','schedule_photos_detail.Id_Photo')
	  					->leftjoin('users','attendances.Id_Student','users.Id')
	  					->where('classes.Id_User_Lecturer','=',$chosen_one)
	  					->where('semesters.EndDate', '>', NOW())
	  					->groupBy('classes.Id','classes.Id_User_Lecturer','subjects.SubjectName','classes.ClassNumber')
	  					->select('classes.Id as Id_Class','classes.Id_User_Lecturer as Lecturer',
	  							'subjects.SubjectName as MK','classes.ClassNumber',
	  					DB::raw('sum( !isnull(attendances.Id)) as TaggedFace,
								 sum( isnull(attendances.Id)) as unTaggedFace,
		    					 (sum( !isnull(attendances.Id)) + sum( isnull(attendances.Id))) as faceNum,
								 count(distinct schedules.Id) as jmlPertemuan'))
	  					->get()->toArray();

	  	//echo json_encode($meetingReport);
	  	$resultArray = array();

	  	foreach($meetingReport as $entry) {
			$temp["Id_Class"] = $entry->Id_Class;
			$temp["Lecturer"] = $entry->Lecturer;
			$temp["SubjectName"] = $entry->MK;
			$temp["ClassNumber"] = $entry->ClassNumber;
			$temp["NoOfUntaggedPhoto"] = $entry->unTaggedFace;
			$temp["TotalAverageEngagement"] = 0;
			$temp["TotalMeeting"] = $entry->jmlPertemuan;
			$temp["jmlMahasiswa"] = 0;
			$resultArray[$entry->Id_Class] = $temp;
		}

		//perlu satu query lagi buat cari jml orang yang perlu
		//echo json_encode($resultArray);

		$engagementsReport = DB::select('select c.Id,u.Name ,coalesce(sum(egg.Value),0) as engagementValue ,count(distinct sc.Id) as jmlSchedule, 
			( coalesce(sum(egg.Value),0)/count(distinct sc.Id)) as avgEngagement   from enrollments err 
			join users u on (err.Id_User_Student = u.Id)
			join classes c on (err.Id_Class =c.Id)
		    join schedules sc on (sc.Id_Class = c.Id)
		    join lessons l on (c.Id_Lesson = l.Id)
		    join subjects sb on (l.Id_Subject = sb.Id)
		    join semesters s  on (l.Id_Semester = s.Id)
		    left join engagements egg on ((egg.Id_Enrollment = err.Id) and (egg.Id_Schedule = sc.Id))
		    where ((s.EndDate > NOW()) and (c.Id_User_Lecturer = '. $chosen_one . '))
		    group by c.Id,u.Name,sb.SubjectName');

		foreach ($engagementsReport as $entry) {
			$temp = $resultArray[$entry->Id];
			$temp["TotalAverageEngagement"] += $entry->avgEngagement;
			$temp["jmlMahasiswa"] += 1;
			$resultArray[$entry->Id] = $temp;
		}
  	
		foreach ($resultArray as $entry) {
			$temp = $resultArray[$entry["Id_Class"]];
			if ($entry["jmlMahasiswa"] != 0){
				$temp["TotalAverageEngagement"] = 	$entry["TotalAverageEngagement"]/ $entry["jmlMahasiswa"];	
			}
			else {
				$temp["TotalAverageEngagement"] =0;
			}
			$resultArray[$entry["Id_Class"]] = $temp;
		}

		$returnArray = array();
		foreach ($resultArray as $entry) {
			array_push($returnArray, $entry);
		}

		echo json_encode($returnArray);  
  	}


	public function getSemesterLecturerReportOld(Request $request) {
		$chosen_one = $request->input('input_chosen_lecturer');

  		//kalo gak usah by schedule
  		 $photoList = DB::table('schedules')
    				->join('classes','schedules.Id_Class','classes.Id')
    				->join('lessons','classes.Id_Lesson','lessons.Id')
					->join('subjects','lessons.Id_Subject','subjects.Id')
					->join('semesters','lessons.Id_Semester','semesters.Id')
  					->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
  					->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
  					->join('photos','schedule_photos_detail.Id_Photo','photos.Id')
  					->leftjoin('attendances','attendances.Id_Photo','photos.Id')
  					->leftjoin('users','attendances.Id_Student','users.Id')
  					->where('semesters.EndDate','>',NOW())
  					->groupBy('classes.id','subjects.SubjectName','classes.ClassNumber','attendances.isValid')
  					->select('classes.id as Class','subjects.SubjectName','classes.ClassNumber','attendances.isValid',DB::raw('Count(*) as jml'))
  					->where('classes.Id_User_Lecturer','=', $chosen_one)
  					->get()->toArray();

  		//echo json_encode($photoList);


		$jmlPertemuan = DB::table('schedules')
			 ->join('classes','schedules.Id_Class','classes.Id')
			 ->join('lessons','classes.Id_Lesson','lessons.Id')
			 ->join('subjects','lessons.Id_Subject','subjects.Id')
			 ->join('semesters','lessons.Id_Semester','semesters.Id')
			 ->join('users','classes.Id_User_Lecturer','users.Id')
			 ->where('semesters.EndDate','>',NOW())
			 ->groupBy('classes.Id','classes.ClassNumber','users.Name','subjects.SubjectName')
			 ->select('classes.Id','classes.ClassNumber',
			 	'users.Name as Lecturer','subjects.SubjectName as MK',
			 	DB::raw('count(*) as jmlPertemuan'))
			 ->where('classes.Id_User_Lecturer','=',$chosen_one)->get()->toArray();

		//echo json_encode($jmlPertemuan);
	 	//ini attendance by class.. we dont need cuman perlu 
		$attendancesReport = DB::table('attendances')
							 ->leftjoin('schedules','attendances.Id_Schedule','schedules.Id')
							 ->join('classes','schedules.Id_Class','classes.Id')
							 ->join('lessons','classes.Id_Lesson','lessons.Id')
							 ->join('subjects','lessons.Id_Subject','subjects.Id')
							 ->join('semesters','lessons.Id_Semester','semesters.Id')
							 ->join('users','attendances.Id_Student','users.Id')
							 ->where('semesters.EndDate','>',NOW())
							 ->groupBy('users.Id','classes.Id','classes.ClassNumber','subjects.SubjectName')
							 ->select('users.Id as User','classes.Id as Class', 'classes.ClassNumber' ,'subjects.SubjectName as Name',
									  DB::raw('count(*) as jml'))
							 ->where('classes.Id_User_Lecturer','=',$chosen_one)			
							 ->get()->toArray();

	//	echo json_encode($attendancesReport);

		$class_report = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','lessons.Id_Subject','subjects.Id')
				  ->join('semesters','lessons.Id_Semester','semesters.Id')
				  ->where('semesters.EndDate','>',NOW())
				  ->groupBy('users.Id','users.name','engagements.Value','classes.Id' ,'subjects.SubjectName')
				  ->select('users.Id as User','classes.Id as Class', 'subjects.SubjectName as Name',DB::raw('coalesce(sum(Value),0) as jml'))
				  ->where('classes.Id_User_Lecturer','=',$chosen_one)	
				  ->get()->toArray();

		//echo json_encode($class_report);

		$resultArray = array();

		foreach($jmlPertemuan as $entry) {
			$temp["Id_Class"] = $entry->Id;
			$temp["Lecturer"] = $entry->Lecturer;
			$temp["SubjectName"] = $entry->MK;
			$temp["ClassNumber"] = $entry->ClassNumber;
			$temp["NoOfUntaggedPhoto"] = 0;
			$temp["TotalAverageEngagement"] = 0;
			$temp["TotalMeeting"] = $entry->jmlPertemuan;
			$temp["jmlMahasiswa"] = 0;
			$resultArray[$entry->Id] = $temp;
		}

		foreach($photoList as $entry) {
			$temp = $resultArray[$entry->Class];
			if ($entry->isValid != 1){
				$temp["NoOfUntaggedPhoto"] += $entry->jml;
			}
			else { //dikurangin hasil valid
				$temp["NoOfUntaggedPhoto"] -= $entry->jml;		
			}
			$resultArray[$entry->Class] = $temp;
		}
		
		
		//duh ini query super lama

		//[{"User":34,"Class":32,"ClassNumber":"A","Name":"Grafika Komputer","jml":3}
		$arrayMahasiswa = array();

		foreach($attendancesReport as $entry) {
			$temp2["User"] = $entry->User;
			$temp2["Class"] = $entry->Class;
			$temp2["jmlHadir"] =  $entry->jml;
			$temp2["TotalAverageEngagement"] = 0;
			
			$arrayMahasiswa[$entry->User . "-".$entry->Class] = $temp2;		
		}


		foreach($class_report as $entry) {
			if(!array_key_exists($entry->User . "-".$entry->Class,$arrayMahasiswa)){
				//belum pernah hadir
				$temp2["User"] = $entry->User;
				$temp2["Class"] = $entry->Class;
				$temp2["jmlHadir"] =  0;
				$temp2["AverageEngagementPoint"] = 0;
			}
			else {
				$temp2 = $arrayMahasiswa[$entry->User . "-".$entry->Class];
				$temp2["TotalAverageEngagement"] += $entry->jml / $temp2["jmlHadir"];
			}
			$arrayMahasiswa[$entry->User . "-".$entry->Class] = $temp2;
		}

		foreach ($arrayMahasiswa as $entry) {

			$temp = $resultArray[$entry["Class"]];
			$temp["jmlMahasiswa"] +=1;
			$temp["TotalAverageEngagement"] += $entry["TotalAverageEngagement"];
			$resultArray[$entry["Class"]] = $temp;
		}

		//kalo perlu array biasa diconvert 
		$normalArray = array();
		foreach ($resultArray as $entry) {
			//echo("-". $entry["TotalAverageEngagement"] ." dan " . $entry["jmlMahasiswa"] . " hasil : ");
			
			if ($entry["jmlMahasiswa"] != 0){
				$entry["TotalAverageEngagement"] = ( $entry["TotalAverageEngagement"] /  $entry["jmlMahasiswa"]);
			}
			else {
				$entry["TotalAverageEngagement"] = 0;
			}

			//echo($entry["TotalAverageEngagement"] . "\n");
			array_push($normalArray, $entry);
		}

		echo json_encode($normalArray);
		
		
	}


	public function getSemesterEngagementReportWeb(Request $request) {

	}

	public function getSemesterEngagementReport(Request $request) {
		$chosen_one = $request->input('input_chosen_student');
		
/*
		$ActiveClasses = DB::table('clasess')
						->join('lessons','classes.Id_Lesson','lessons.Id')
					    ->join('semesters','lessons.Id_Semester','semesters.Id')
						->where('semesters.EndDate','>',NOW())
						->get();
*/


		//gimana kalo kita search hiji hiji heula aakkakaka

		$jmlPertemuan = DB::table('schedules')
			 ->join('classes','schedules.Id_Class','classes.Id')
			 ->join('lessons','classes.Id_Lesson','lessons.Id')
			 ->join('subjects','lessons.Id_Subject','subjects.Id')
			 ->join('semesters','lessons.Id_Semester','semesters.Id')
			 ->join('enrollments','enrollments.Id_Class','classes.Id')
			 ->join('users','classes.Id_User_Lecturer','users.Id')
			 ->where('semesters.EndDate','>',NOW())
			 ->groupBy('classes.Id','classes.ClassNumber','users.Name','subjects.SubjectName')
			 ->select('classes.Id','classes.ClassNumber',
			 	'users.Name as Lecturer','subjects.SubjectName as MK',
			 	DB::raw('count(*) as jmlPertemuan'))
			 ->where('enrollments.Id_User_Student','=',$chosen_one)->get()->toArray();

		//echo json_encode($jmlPertemuan);

		$attendancesReport = DB::table('attendances')
							 ->join('schedules','attendances.Id_Schedule','schedules.Id')
							 ->join('classes','schedules.Id_Class','classes.Id')
							 ->join('lessons','classes.Id_Lesson','lessons.Id')
							 ->join('subjects','lessons.Id_Subject','subjects.Id')
							 ->join('semesters','lessons.Id_Semester','semesters.Id')
							 ->join('users','attendances.Id_Student','users.Id')
							 ->where('semesters.EndDate','>',NOW())
							 ->groupBy('users.Id','classes.Id','semesters.EndDate','subjects.SubjectName')
							 ->select('users.Id as User','classes.Id as Class', 'subjects.SubjectName as Name',
									  DB::raw('count(*) as jml'))
							 ->where('users.Id','=',$chosen_one)			
							 ->get()->toArray();

		//echo json_encode($attendancesReport);


		$class_report = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','lessons.Id_Subject','subjects.Id')
				  ->join('semesters','lessons.Id_Semester','semesters.Id')
				  ->where('semesters.EndDate','>',NOW())
				  ->groupBy('users.Id','users.name','engagements.Value','classes.Id' ,'subjects.SubjectName')
				  ->select('users.Id as User','classes.Id as Class', 'subjects.SubjectName as Name',DB::raw('coalesce(sum(Value),0) as jml'))
				  ->where('users.Id','=',$chosen_one)	
				  ->get()->toArray();

		//echo json_encode($class_report);

		$resultArray = array();

		foreach($jmlPertemuan as $entry) {
			$temp["Id_Class"] = $entry->Id;
			$temp["Lecturer"] = $entry->Lecturer;
			$temp["SubjectName"] = $entry->MK;
			$temp["ClassNumber"] = $entry->ClassNumber;
			$temp["NoOfAbsence"] = $entry->jmlPertemuan;
			$temp["TotalMeeting"] = $entry->jmlPertemuan;
			$temp["TotalAttendance"] = 0;
			$temp["TotalEngagementPoint"] = 0;
			$resultArray[$entry->Id] = $temp;
		}
		
		foreach($attendancesReport as $entry) {
			$temp = $resultArray[$entry->Class];
			$temp["TotalAttendance"] = $entry->jml;
			$temp["NoOfAbsence"] = $temp["NoOfAbsence"] -  $entry->jml;
			$resultArray[$entry->Class] = $temp;		
		}
		foreach($class_report as $entry) {
			$temp = $resultArray[$entry->Class];
			$temp["TotalEngagementPoint"] = $entry->jml;	
			$resultArray[$entry->Class] = $temp;
		}

		//kalo perlu array biasa diconvert 
		$normalArray = array();
		foreach ($resultArray as $entry) {
			array_push($normalArray, $entry);
		}

		echo json_encode($normalArray);
		


	}

	public function getEngagementByEnrollmentServiceSum(Request $request){
		$chosen_class = $request->input("input_chosen_class");
		$chosen_schedule = $request->input("input_chosen_schedule");


		/*
select c.Id, u.UserId, u.Name, sc.scheduleDate, p.Path, coalesce(sum(egg.value),0) as point from enrollments err 
	join users u on (err.Id_User_Student = u.Id)
    left join photos p on (u.profile_picture = p.Id)
    join classes c on (err.Id_Class = c.Id)
    join schedules sc on (sc.Id_Class = c.Id)
    join lessons l on (c.Id_Lesson = l.Id)
    join subjects sb on (l.Id_Subject = sb.Id)
    join semesters s  on (l.Id_Semester = s.Id)
    left join engagements egg on ( (egg.Id_Schedule = sc.Id) and ( egg.Id_Enrollment = err.Id) )
    where ((c.Id = 33) and (sc.Id = 363))
    group by c.Id, u.Id, sc.Id;
		*/
/*
    	$query = 'select c.Id, u.UserId, u.Name, sc.scheduleDate, p.Path, coalesce(sum(egg.value),0) as point from enrollments err 
			join users u on (err.Id_User_Student = u.Id)
		    left join photos p on (u.profile_picture = p.Id)
		    join classes c on (err.Id_Class = c.Id)
		    join schedules sc on (sc.Id_Class = c.Id)
		    join lessons l on (c.Id_Lesson = l.Id)
		    join subjects sb on (l.Id_Subject = sb.Id)
		    join semesters s  on (l.Id_Semester = s.Id)
		    left join engagements egg on ( (egg.Id_Schedule = sc.Id) and ( egg.Id_Enrollment = err.Id) )
		    where ((c.Id = ' . $chosen_class. ') and (sc.Id = '. $chosen_schedule.'))
		    group by c.Id, u.UserId,u.Name,sc.scheduleDate,p.Path;';

		echo $query;

    	//$class_engagement = DB::select($query);

    	//echo (json_encode($class_engagement));
*/

		$class_engagement = DB::table('enrollments')	
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->join('classes','classes.Id','enrollments.Id_Class')
				  ->join('schedules','schedules.Id_Class','classes.Id')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','subjects.Id','lessons.Id_Subject')
				  ->join('semesters','lessons.Id_Semester','semesters.Id')
				  ->leftjoin('photos','users.profile_picture','photos.Id')
				  ->leftjoin('engagements',function($join) {
				  		$join->on('engagements.Id_enrollment','enrollments.Id');
				  		$join->on('engagements.Id_Schedule','schedules.Id');	
				  })
				  ->groupBy('users.Id','users.UserId','users.name','classes.Id','photos.Path')
				  ->select('users.Id','users.UserId','users.name',
				  	DB::raw('coalesce(photos.Path,"") as Path'),
				  	DB::raw('coalesce(sum(Value),0) as jml'))	
				  ->where('classes.Id','=',$chosen_class)
				  ->where('schedules.Id','=',$chosen_schedule)
				  ->get()->toArray();

		//print_r($class_engagement);

		$tempArray = $class_engagement;
		$resultArray = array();

		foreach ($tempArray as $entry) {
			$temp["Id"] = $entry->Id;
			$temp["UserId"] = $entry->UserId;
			$temp["Name"] = $entry->name;
			$temp["jml"] = $entry->jml;
			if ($entry->Path == null) {
				$temp["path"] = "";
			}
			else {
				$temp["path"] = $entry->Path;
			}
			array_push($resultArray,$temp);
		}
		echo json_encode($resultArray);
	}


	public function getEngagementServiceClassSum(Request $request) {
		$chosen_class = $request->input("input_chosen_class");
		$chosen_schedule = $request->input("input_chosen_schedule");
		$chosen_one = $request->input("input_chosen_user");
		$sum_kah = $request->input("input_chosen_sum");

		
		if ($chosen_schedule != null) {//per class per person
			
			$class_engagement = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->leftjoin('photos','users.profile_picture','photos.Id')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','subjects.Id','lessons.Id_Subject')
				  ->groupBy('users.Id','users.UserId','users.name','engagements.Value','classes.Id','photos.Path' )
				  ->select('users.Id','users.UserId','users.name', 'photos.Path',
				  	DB::raw('coalesce(sum(Value),0) as jml'))	
				  ->where('classes.Id','=',$chosen_class)
				  ->where('schedules.Id','=',$chosen_schedule)
				  ->get()->toArray();
		}
		else if ($chosen_one != null) { //asumsi kalo gak ada schedule berarti per person

			$class_engagement = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				   ->leftjoin('photos','users.profile_picture','photos.Id')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','subjects.Id','lessons.Id_Subject')
				  ->groupBy('users.Id','users.UserId','users.name','engagements.Value','classes.Id','photos.Path')
				  ->select('users.Id','users.UserId','users.name','photos.Path',
				  	DB::raw('coalesce(sum(Value),0) as jml'))	
				  ->where('classes.Id','=',$chosen_class)
				  ->where('users.Id','=',$chosen_one)
				  ->get()->toArray();
		}
		else { //sum untuk kelas itu tanpa schedule tanpa per person
			;
			$class_engagement = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->leftjoin('photos','users.profile_picture','photos.Id')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','subjects.Id','lessons.Id_Subject')
				  ->groupBy('users.Id','users.UserId','users.name','engagements.Value','classes.Id','photos.Path')
				  ->select('users.Id','users.UserId','users.name','photos.Path',
				  	DB::raw('coalesce(sum(Value),0) as jml '))
				  ->where('classes.Id','=',$chosen_class)
				  ->get()->toArray();
		}


			  //->where('users.Id','=',$chosen_one)
		$tempArray = $class_engagement;
		$resultArray = array();

		foreach ($tempArray as $entry) {
			
			$temp["Id"] = $entry->Id;
			$temp["UserId"] = $entry->UserId;
			$temp["Name"] = $entry->name;
			$temp["jml"] = $entry->jml;
			if ($entry->Path == null) {
				$temp["path"] = "";
			}
			else {
				$temp["path"] = $entry->Path;
			}
			array_push($resultArray,$temp);
		}
		echo json_encode($resultArray);
	}


	public function getEngagementServiceClass(Request $request) {
		$chosen_class = $request->input("input_chosen_class");
		$chosen_schedule = $request->input("input_chosen_schedule");
		$chosen_one = $request->input("input_chosen_user");
		$sum_kah = $request->input("input_chosen_sum");


		if ($chosen_schedule != null) {//per class per person
			$class_engagement = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','subjects.Id','lessons.Id_Subject')
				  ->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')  
				  ->groupBy('users.Id','users.name','engagement_types.engagement_name', 'engagement_types.Id','engagements.Value','classes.Id')
				  ->select('users.Id','users.name',DB::raw('engagement_types.engagement_name as tipe'),
				  	DB::raw('coalesce(sum(Value),0) as jml'))	
				  ->where('classes.Id','=',$chosen_class)
				  ->where('schedules.Id','=',$chosen_schedule)
				  ->get()->toArray();
		}
		else if ($chosen_one != null) { //asumsi kalo gak ada schedule berarti per person
			$class_engagement = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','subjects.Id','lessons.Id_Subject')
				  ->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')  
				  ->groupBy('users.Id','users.name','engagement_types.engagement_name', 'engagement_types.Id','engagements.Value','classes.Id')
				  ->select('users.Id','users.name',DB::raw('engagement_types.engagement_name as tipe'),
				  	DB::raw('coalesce(sum(Value),0) as jml'))	
				  ->where('classes.Id','=',$chosen_class)
				  ->where('users.Id','=',$chosen_one)
				  ->get()->toArray();
		}
		else { //sum untuk kelas itu tanpa schedule tanpa per person
			$class_engagement = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','subjects.Id','lessons.Id_Subject')
				  ->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')  
				  ->groupBy('users.Id','users.name','engagement_types.engagement_name', 'engagement_types.Id','engagements.Value','classes.Id')
				  ->select('users.Id','users.name',DB::raw('engagement_types.engagement_name as tipe'),
				  	DB::raw('coalesce(sum(Value),0) as jml'))	
				  ->where('classes.Id','=',$chosen_class)
				  ->get()->toArray();
		}


			  //->where('users.Id','=',$chosen_one)
		$tempArray = $this->ConvertToJsonEngagement($class_engagement);
		echo json_encode($tempArray);
	}

	public function GetEngagementSchedule() {

		$chosen_class = $request->input("input_chosen_class");
		$chosen_schedule = $request->input("input_chosen_schedule");

		$class_engagement = DB::table('engagements')
			  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
			  ->join('users','users.Id','enrollments.Id_User_Student')
			  ->join('schedules','schedules.Id','engagements.Id_Schedule')
			  ->join('classes','classes.Id','schedules.Id_Class')
			  ->join('lessons','lessons.Id','classes.Id_Lesson')
			  ->join('subjects','subjects.Id','lessons.Id_Subject')
			  ->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')  
			  ->groupBy('users.Id','users.name','engagement_types.engagement_name', 'engagement_types.Id','engagements.Value','classes.Id')
			  ->select('users.Id','users.name',DB::raw('engagement_types.engagement_name as tipe'),
			  	DB::raw('coalesce(sum(Value),0) as jml'))	
			  ->where('classes.Id','=',$chosen_class)
			  ->where('schedules.Id','=',$chosen_schedule)
			  ->get()->toArray();

		$tempArray = $this->ConvertToJsonEngagement($global_engagement);
		echo json_encode($tempArray);
	}

	public function GetEngagementUser() {

	}

	public function GetEngagementClass(Request $request) {

		$chosen_class = $request->input("input_chosen_class");

		$class_engagement = DB::table('engagements')
			  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
			  ->join('users','users.Id','enrollments.Id_User_Student')
			  ->join('schedules','schedules.Id','engagements.Id_Schedule')
			  ->join('classes','classes.Id','schedules.Id_Class')
			  ->join('lessons','lessons.Id','classes.Id_Lesson')
			  ->join('subjects','subjects.Id','lessons.Id_Subject')
			  ->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')  
			  ->groupBy('users.Id','users.name','engagement_types.engagement_name', 'engagement_types.Id','engagements.Value','classes.Id')
			  ->select('users.Id','users.name',DB::raw('engagement_types.engagement_name as tipe'),
			  	DB::raw('coalesce(sum(Value),0) as jml'))	
			  ->where('classes.Id','=',$chosen_class)
			  ->get()->toArray();

		$tempArray = $this->ConvertToJsonEngagement($global_engagement);
		echo json_encode($tempArray);

	}

	public function GetEngagementGlobal() {
		//get data engagement total 
		//$engagement_types = EngagementType::All();

		$global_engagement = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','subjects.Id','lessons.Id_Subject')
				  ->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')  
				  ->groupBy('users.Id','users.name','engagement_types.engagement_name', 'engagement_types.Id','engagements.Value')
				  ->select('users.Id','users.name',DB::raw('engagement_types.engagement_name as tipe'),
				  	DB::raw('coalesce(sum(Value),0) as jml'))	
				  ->get()->toArray();

		$tempArray = $this->ConvertToJsonEngagement($global_engagement);
		
		echo json_encode($tempArray);
	}
	
	public function ConvertToJsonEngagement($engagement) {
		$tempArray = [];
		$lastId = $engagement[0]->Id;
		$temp = null;
		$temp["Id"] = "0";
		$temp["Nama"] = "no data found";
		$temp["Engagement"]=[];
		
		foreach ($engagement as $entry) {
			if ($entry->Id == $lastId) {
				//timpa data gapapa
				$temp["Id"] = $entry->Id;
				$temp["Nama"] = $entry->name;
				$tempChild = null;
				$tempChild[$entry->tipe] = $entry->jml;
				array_push( $temp["Engagement"], $tempChild);
			}	
			else {
				array_push($tempArray, $temp);
				$temp = null;
				//data baru
				$lastId = $entry->Id;
				$temp["Id"] = $entry->Id;
				$temp["Nama"] = $entry->name;
				$temp["Engagement"] = [];

				$tempChild = null;
				$tempChild[$entry->tipe] = $entry->jml;	
				array_push($temp["Engagement"], $tempChild);
			}
		}
		array_push($tempArray, $temp);
		return $tempArray;
	}



	public function FilterScheduleByClass(Request $request) {
		$id_class = $request->input('select_engagement_class');
		$schedules = Schedule::where('Id_Class','=',$id_class)->get();
		return json_encode($schedules);
	}

	public function FilterUserByClass(Request $request) {
		
		$id_class = $request->input('select_engagement_class');
		//echo "id class : $id_class\n<br>";
		
		$enrollment = Enrollment::where('Id_Class','=',$id_class)->select('Id_User_Student')->get();
		//filter user by enrollment return them by json
		//print_r($enrollment->toArray());
		$temp[] = $enrollment->toArray();
		$user_array = array();
		for($i=0;$i<count($temp[0]);$i++){
			//echo "whaaaaaat";
			//echo ($temp[0][$i]["Id_User_Student"] . "\n");
			array_push($user_array, $temp[0][$i]["Id_User_Student"]);
		} 
		//echo("\n<br>");
		//print_r($user_array);
		$result = Users::whereIn('Id',$user_array)->get();
		
		$return_object = json_encode($result);
		return $return_object;

	}

    //
	public function Get(Request $request) {
		//$engagement = Engagement::paginate(10);
		$filterClassId = $request->input('filter_engagement_class');

		if(($filterClassId != null) && ($filterClassId != 0)){
				$engagement = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','subjects.Id','lessons.Id_Subject')
				  ->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')
				  ->select('engagements.Id','classes.ClassNumber as Class','schedules.ScheduleDate','subjects.SubjectName','users.UserId as StudentId','users.Name as StudentName','engagement_types.engagement_name as Interaksi','engagements.Value')
				  ->orderBy('engagements.Id','desc')
				  ->where('classes.Id','=',$filterClassId)
				  ->paginate(10);
			}
			else {
				$engagement = DB::table('engagements')
				  ->join('enrollments','enrollments.Id','engagements.Id_Enrollment')
				  ->join('users','users.Id','enrollments.Id_User_Student')
				  ->join('schedules','schedules.Id','engagements.Id_Schedule')
				  ->join('classes','classes.Id','schedules.Id_Class')
				  ->join('lessons','lessons.Id','classes.Id_Lesson')
				  ->join('subjects','subjects.Id','lessons.Id_Subject')
				  ->join('engagement_types','engagements.Id_Eng_Type','engagement_types.Id')
				  ->select('engagements.Id','classes.ClassNumber as Class','schedules.ScheduleDate','subjects.SubjectName','users.UserId as StudentId','users.Name as StudentName','engagement_types.engagement_name as Interaksi','engagements.Value')
				  ->orderBy('engagements.Id','desc')
				  ->paginate(10);	
			}

		$class_id = $request->input('select_engagement_class');
		
		//$enrollment;
		//$schedules;
		
		if ($class_id != null) {
			$schedules = Schedule::Where('Id_Class','=',$class_id);
			$users = DB::table('enrollments')
				 ->join('users','users.Id','enrollments.Id_User_Student')
				 ->select('users.Id','users.UserId','users.Name')
				 ->Where('enrollments.Id_Class','=',$chosen_class)
				 ->get();
		}
		else {
			$schedules =  Schedule::All(); 
			$users = DB::table('enrollments')
				 ->join('users','users.Id','enrollments.Id_User_Student')
				 ->select('users.Id','users.UserId','users.Name')
				 ->get();
		}
		
		$ActiveClasses = DB::table('classes')
			   ->join('lessons','classes.Id_Lesson','lessons.Id')
			   ->join('semesters','semesters.Id','lessons.Id_Semester')
			   ->join('subjects','subjects.Id','lessons.Id_Subject')
			   ->join('users','classes.Id_User_Lecturer','users.Id')
               ->select('classes.Id','users.Name AS Lecturer','subjects.SubjectName As Subjects','classes.ClassNumber')
			   ->where('semesters.EndDate','>',NOW())
               ->get();
		
		return view('engagement_view')->with('old',$request)->with('users',$users)->with('chosen_class',$class_id)->with('engagements',$engagement)->with('classes',$ActiveClasses)->with('schedules',$schedules);
	}
	
	public function GetStudent(Request $request) {
		
	}
	
	public function GetLecturer(Request $request) {
		
	}
	
	
	public function GetJson(Request $request) {
		$idClass = $request->input('IdClass');
		$engagement = DB::table('engagements')
               ->join('schedules','engagements.Id_Schedule','schedules.Id')
			   ->join('enrollments','engagements.Id_enrollment','enrollments.Id')
			   ->join('users','enrollments.Id_User_Student','users.Id')
			   ->join('classes','schedules.Id_Class','classes.Id')
               ->select('engagement.Id','users.Name','schedules.scheduleDate','engagements.Ans_Right','engagements.Ans_Wrong','engagements.Asking','engagements.Interacting')
               ->where('enrollments.Id_Class','=',$idClass)
               ->orderBy('engagement.Id','desc')
               ->get();
    return json_encode($engagement);
	}


	public function InsertService(Request $request) {
	  $temp = new Engagement;
	  //kayanya dapetnya id student deh --" ama id_class

	  $IdStudent = $request->input('input_engagement_user');
	  $IdClass = $request->input('input_engagement_classes');
	  $eng_id = $request->input('input_id_eng');
	  $temp->Id_Schedule = $request->input('input_engagement_schedule');
	 // echo "id student : $IdStudent  -  id class : $IdClass";	
	  //echo "eng_id = $eng_id";

	  $enrollment = Enrollment::Where('Id_User_Student','=',$IdStudent)
					->Where('Id_Class','=',$IdClass)
					->first();
	  
	  /*
	  echo "enrollementnya dapat gak ya  ?";
	  print_r($enrollment);
	  //schedule nya bs dipilih atau bs dibuat dulu atau gmana ya --"a... omg
	  */

	  $temp->Id_Enrollment = $enrollment["Id"];
	  

	  $temp->Value = 1;

	   switch ($eng_id) {
	  	case 0 : $temp->Id_Eng_Type = 1;break;
	  	case 1 : $temp->Id_Eng_Type = 2;break;
	  	case 2 : $temp->Id_Eng_Type = 3;break;
	  	case 3 : $temp->Id_Eng_Type = 4;break;
	  	default : $temp->Id_Eng_Type = 1; break;
	  }
	  
	  
	  $result = $temp->save();
	 // $result =0;
	if($result) {
	    $returnObject = array(
	      "Id" => "1",
	      "Message" => "Engagement Recorded"
	    );
	    return json_encode($returnObject);
	 }
	 else {
	    $returnObject = array(
	      "Id" => "2",
	      "Message" => "Engagement Recording  Failed"
	    );
	    return json_encode($returnObject);
	 }
	  
	}


	
	public function Insert(Request $request) {
	  $temp = new Engagement;
	  //kayanya dapetnya id student deh --" ama id_class

	  $IdStudent = $request->input('select_engagement_user');
	  $IdClass = $request->input('select_engagement_classes');
	  $eng_id = $request->input('_id_eng');

	 // echo "id student : $IdStudent  -  id class : $IdClass";	
	  //echo "eng_id = $eng_id";

	  $enrollment = Enrollment::Where('Id_User_Student','=',$IdStudent)
					->Where('Id_Class','=',$IdClass)
					->first();
	  
	  /*
	  echo "enrollementnya dapat gak ya  ?";
	  print_r($enrollment);
	  //schedule nya bs dipilih atau bs dibuat dulu atau gmana ya --"a... omg
	  */

	  $temp->Id_Enrollment = $enrollment["Id"];
	  $temp->Id_Schedule = $request->input('select_engagement_schedule');

	  $temp->Value = 1;

	   switch ($eng_id) {
	  	case 0 : $temp->Id_Eng_Type = 1;break;
	  	case 1 : $temp->Id_Eng_Type = 2;break;
	  	case 2 : $temp->Id_Eng_Type = 3;break;
	  	case 3 : $temp->Id_Eng_Type = 4;break;
	  	default : $temp->Id_Eng_Type = 1; break;
	  }
	  
	  
	  $result = $temp->save();
	 // $result =0;

	  if($result) {
		$messages = 'Engagament recorded';
		return Redirect('Engagements')->with('messages',$messages);
	  }
	  else {
		$messages = ['Engagement recording failed'];
		return Redirect('Engagements')->with('messages',$messages);
	  }
	  
	}
	
	public function Update (Request $request) {
		
		
	}
	
	
	public function Delete(Request $request,$Id) {
		$temp = Engagement::find($Id);
      	$temp->delete();
     	$messages = 'engagement entry deleted';
      	return Redirect('Engagements')->with('messages',$messages);
	}
	
	
}

<?php

namespace App\Http\Controllers;

use App\Schedule;
use App\SchedulePhoto;
use App\Photo;
use App\SchedulePhotoDetail;
use App\Attendance;

use Illuminate\Support\Facades\DB;
use App\Classes;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{

  public function WipeSchedule(Request $request) {

    //ngapus sebuah jadwal beserta semua yang terkait dengan nyah
    $id= $request->input('id_schedule');
    $This->Delete($id);

  }

  public function GetScheduleByClass(Request $request) {
    $id_class = $request->input('input_class');
    $schedules = DB::table('schedules')->where('Id_Class','=',$id_class)
                ->orderBy('ScheduleDate','desc')->get();
    
    return json_encode($schedules);
  }

 public function GetDetailPhotoServiceWeb(Request $request) {
    $Id_Schedule = $request->input('input_schedule_id');
    $photoList = DB::table('schedules')
            ->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
            ->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
            ->join('photos','schedule_photos_detail.Id_Photo','photos.Id')
            ->leftjoin('attendances','attendances.Id_Photo','photos.Id')
            ->leftjoin('users','attendances.Id_Student','users.Id')
            ->select('schedules.Id as Id_Schedules','schedule_photos.Id as Id_SchedulePhotos','photos.Path', 'attendances.Id_Student','users.UserId','users.Name','attendances.isValid','photos.Id as Id_Photos')
            ->where('schedules.Id','=', $Id_Schedule)
            ->get();

    $result = array();
    foreach($photoList as $photo) {
      if (($photo->isValid == null) || ($photo->isValid == 1)) {

        $result[] = $photo;
      }
    }
    return Redirect('Attendance/')->with('photos',$photos);
 }

 public function GetDetailPhotoService(Request $request) {
  	$Id_Schedule = $request->input('input_schedule_id');
  	
    
    $photoList = DB::table('schedules')
  					->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
  					->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
  					->join('photos','schedule_photos_detail.Id_Photo','photos.Id')
  					->leftjoin('attendances','attendances.Id_Photo','photos.Id')
  					->leftjoin('users','attendances.Id_Student','users.Id')
  					->select('schedules.Id as Id_Schedules','schedule_photos.Id as Id_SchedulePhotos','photos.Path', 'attendances.Id_Student','users.UserId','users.Name','attendances.isValid','photos.Id as Id_Photos')
  					->where('schedules.Id','=', $Id_Schedule)
  					->get();
  
  
  	$result = array();
  	foreach($photoList as $photo) {
  		if (($photo->isValid == null) || ($photo->isValid == 1)) {
  			$result[] = $photo;
  		}
  	}
  	return json_encode($result);
  }

  public function GetDetailPhotoServiceOld(Request $request) {
  	$Id_Schedule = $request->input('input_schedule_id');
  	$photoList = DB::table('schedules')
  					->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
  					->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
  					->select('schedules.Id as Id_Schedules','schedule_photos.Id as Id_SchedulePhotos','photos.Path', 'attendances.Id_Student','users.UserId','users.Name','attendances.isValid','photos.Id as Id_Photos')
  					->where('schedules.Id','=', $Id_Schedule)
  					->get();
  	return json_encode($photoList);
  }

  public function GetService(Request $request) {
  	$Id_Class = $request->input('input_class_id');
  	$scheduleList = Schedule::where('Id_Class','=',$Id_Class)
            ->orderBy('scheduleDate','desc')
  					->selectRaw("Id,Id_Class as Id_Classes,DATE_FORMAT(scheduleDate,'%d-%m-%Y %H:%i') as Date,created_at,updated_at")
  					->get();
  	return json_encode($scheduleList);
  }

  public function Get(Request $request) {
    $id_class = 0;
    if ($request->input('input_schedules_filter') != null){
      $id_class=$request->input('input_schedules_filter');
    }
    if($id_class ==0){
      $temp = Schedule::paginate(5);
      $classes = Classes::whereHas('Lesson',function($query) {
		  $query->whereHas('Semester',function($query2) {
			  $query2->where('EndDate','>',NOW());
		  });
	  })->get();
    }
    else {
      $temp = Schedule::where('Id_Class','=',$id_class)->paginate(20);
	  $classes = Classes::whereHas('Lesson',function($query) {
		  $query->whereHas('Semester',function($query2) {
			  $query2->where('EndDate','>',NOW());
		  });
	  })->get();
	}
    $messages = 'filterd';
    return View('schedule_view')->with('schedules',$temp)->with('classes',$classes)->with('messages',$messages);

  }

  public function Insert(Request $request) {
    $temp = new Schedule;
    $temp->Id_Class = $request->input('select_schedule_classes');
    $temp->ScheduleDate = $request->input('input_schedule_date');
    $result = $temp->save();

    $messages = 'schedule insert succeded';
    return Redirect('Schedules')->with('messages',$messages);
  }

  public function InsertService(Request $request) {
     $idClass = $request->input('input_schedule_IdClass');
     $dateTime = $request->input('input_schedule_Date');
     $temp = new Schedule;
     $temp->Id_Class = $idClass;
     $temp->ScheduleDate = $dateTime;
     $result = $temp->save();

     if($result) {
        $returnObject = array(
          "Id" => "1",
          "Message" => "Insert Successfull"
        );
        return json_encode($returnObject);
     }
     else {
        $returnObject = array(
          "Id" => "2",
          "Message" => "Insert Failed"
        );
        return json_encode($returnObject);
     }
  }


  public function Filter($Id) {
    $temp = Schedule::Where('Id_Class','=',$Id)->get();
    //$temp = Schedule::all();
    foreach ($temp as $entry) {
      echo "<option value=". $entry->Id.">". date_format(date_create($entry->scheduleDate),'d-m-Y H:i:s')."</option>";
    }
  }

  public function DeleteService(Request $request) {
   $Id = $request->input('input_id');

   $temp = Schedule::find($Id);

   //enak aje :D .. aps dulu semua schedule photo terkait dan detail2nya
   $sp = SchedulePhoto::where('Id_Schedule','=',$temp->Id)->get();
   $spId = array();
  
    foreach ($sp as $entry) {
    //ntuk semua foto gedhe
    $spId[] = $entry->Id;
    //get foto kecil
    $child = SchedulePhotoDetail::where('Id_SchedulePhoto','=',$entry->Id)->get();
    $childId = array();
    foreach($child as $entryChild) {
      //untuk semua foto kecil
      $childId[] = $entryChild->Id;
      $childPhoto = Photo::where('Id','=',$entryChild->Id_Photo)->first();
      if ($childPhoto){
    
         $childPath = substr($childPhoto->Path,21,strlen($childPhoto->Path));
         $childPath = '/var/www/html/'. $childPath;
        
          if(file_exists($childPath)){
                unlink($childPath);
              }
          Photo::find($entryChild->Id_Photo)->delete(); //apus entry photonya
      }
    }
    
    SchedulePhotoDetail::whereIn('Id_SchedulePhoto',$childId)->delete();
    $p = Photo::find($entry->Id_Photo);
    if ($p) {
      $spPath = substr($p->Path,21,strlen($p->Path));
      $spPath = "/var/www/html/" . $spPath;
      

      if (file_exists($spPath)){
        unlink ($spPath);
        //hapus file .json yang dihasilin
        $exp_path = explode(".",$spPath); //harusnya /var/www/html/blablabla/1.jpg 
        $spPath = $exp_path[0] . ".json";
        if (file_exists($spPath)) {
          unlink($spPath);
        }
      }
      Photo::find($entry->Id_Photo)->delete(); //apus entry photonya
    }

    SchedulePhoto::whereIn('Id',$spId)->delete();
     //err kudu apus semua attendance yang ada 
     Attendance::where('Id_Schedule','=',$temp->Id)->delete();
    }

   //delete de schedule
    $temp->delete();
    return "Schedule Deleted";
  }

  public function Delete($Id) {
    $temp = Schedule::find($Id);

	//enak aje :D .. aps dulu semua schedule photo terkait dan detail2nya
	 $sp = SchedulePhoto::where('Id_Schedule','=',$temp->Id)->get();
	 $spId = array();
	
    foreach ($sp as $entry) {
    //ntuk semua foto gedhe
		$spId[] = $entry->Id;
		//get foto kecil
    $child = SchedulePhotoDetail::where('Id_SchedulePhoto','=',$entry->Id)->get();
    $childId = array();
		foreach($child as $entryChild) {
      //untuk semua foto kecil
			$childId[] = $entryChild->Id;
			$childPhoto = Photo::where('Id','=',$entryChild->Id_Photo)->first();
      if ($childPhoto){
		
         $childPath = substr($childPhoto->Path,21,strlen($childPhoto->Path));
			   $childPath = '/var/www/html/'. $childPath;
        
    			if(file_exists($childPath)){
            		unlink($childPath);
           		}
          Photo::find($entryChild->Id_Photo)->delete(); //apus entry photonya
      }
    }
		
		SchedulePhotoDetail::whereIn('Id_SchedulePhoto',$childId)->delete();
		$p = Photo::find($entry->Id_Photo);
    if ($p) {
  		$spPath = substr($p->Path,21,strlen($p->Path));
  		$spPath = "/var/www/html/" . $spPath;
      

      if (file_exists($spPath)){
        unlink ($spPath);
        //hapus file .json yang dihasilin
        $exp_path = explode(".",$spPath); //harusnya /var/www/html/blablabla/1.jpg 
        $spPath = $exp_path[0] . ".json";
        if (file_exists($spPath)) {
          unlink($spPath);
        }
      }
      Photo::find($entry->Id_Photo)->delete(); //apus entry photonya
    }

    SchedulePhoto::whereIn('Id',$spId)->delete();
     //err kudu apus semua attendance yang ada 
     Attendance::where('Id_Schedule','=',$temp->Id)->delete();
    }

   //delete de schedule
    $temp->delete();

    $messages = 'schedule delete succeded';
    return Redirect('Schedules')->with('messages',$messages);
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        //
    }
}

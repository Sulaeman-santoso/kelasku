<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Enrollment;
use App\Classes;
use App\Semester;
use App\Users;
use App\Subject;
use App\Lesson;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class EnrollmentController extends Controller
{

	public function BulkImport(Request $request) {
		$file = $request->input_bulk_import;
	

	$destinationPath = 'public_images/';
		if ($file != null) {
			$targetFile = "bulkImport.". $file->getClientOriginalExtension();
			$file->move($destinationPath, $targetFile);
			$filePath = $destinationPath . "/". $targetFile;

			//dah di copy

			Excel::load($filePath, function($reader) {
				//read excel;
				$results = $reader->get();
				$data = $results->toArray();
				//bisa xlsx atau xls bisa bentuk yang ok bisa bentuk gak ok
				print_r($data);
				
				foreach ($data as $posEntry) {
					if (array_key_exists('nrp', $posEntry)) {
						$this->ImportEntry($posEntry);
					}
					else {
						//go deeper
						foreach ($posEntry as $entry) {
							if (array_key_exists('nrp', $entry)) {
								$this->ImportEntry($entry);
							}		
						}
					}
				}
				
			});//end excel read
		}//end if
		echo "enter here";
	}//end function


	public function ImportEntry($entry) {
		/*
		 [kodejurusan] => 72
                    [nip] => 720140
                    [nama] => Dr. Ir. Mewati Ayub, MT.  
                    [kode] => IN010
                    [namai] => DASAR PEMROGRAMAN
                    [kelas] => A
                    [nrp] => 1372024
                    [namamhs] => VINZA IPSANDY
                    [0] => 
		*/
		
		$kode_mk = $entry["kode"];
		//test dulu apakah kode mk ini tersedia di subject
		$mk_available = Subject::Where('SubjectId','=','$kode_mk')->first();
		$id_mk = 0;
		$works = true;
		if ($mk_available == null) {
			//insert dulu mknya bang
			$temp = new Subject;
			$temp->SubjectId = $kode_mk;
			$temp->SubjectName = $entry["namai"];
			$works  = $temp->save();
			$id_mk = $temp->Id;
		}	
		else {
			$id_mk = $mk_available->Id;
		}
		if ($works == true) { // mk sudah ada..
			//make sure lessonsnya ada..
			$lessons_available = Lesson::WhereHas('IdSubject','=','$id_mk')->first();
			$id_lessons = 0;
			if ($lessons_available == null) {
				//insert lessons if not available
				$temp = new Lesson;
				$temp->SubjectId = $id_mk;
				$temp->SemesterId = $request->input_semester_id;
				$works = $temp->save();
				$id_lessons = $temp->Id;
			}
			else{
				$id_lessons = $lessons_available->Id;
			}
		}
		if ($works == true) { // lessons sudah ada 
			//cari classnya 
			$class_available = Classes::WhereHas('IdLesson','=','$id_lessons')->where('ClassNumber','=','$entry["kelas"]')->first();
			$id_class =0;
			if ($class_available == null) {
				//create classnya
				$temp = new Classes;
				$temp->ClassNumber = $entry["kelas"]; 
				$temp->Id_User_Lecturer = $entry["nip"]; //asumsi semua lecturer udah ditambahin..
				$works = $temp->save();
				
				$id_class = $temp->Id;
			}
			else {
				$id_class = $class_available->Id;
			}
		}
		//works so far..
		if ($works == true) {
			//class udah ada tinggal di enroll
			$temp= new Enrollment;
			$temp->Id_Class = $id_Class;
			$temp->Id_User_Student = $entry["nrp"];
			$works = $temp->save();

			if ($works ==  true) {
				echo ("\n<br>import ". $entry['nrp'] ." berhasil");
			}
			else {
				echo ("\n<br>import" . $entry['nrp'] . " gagal");
			}
			
		}
		//tinggal dicoba
	
	}



  public function importWithoutClassId(Request $request) {
	  $file = $request->input_file_excel;

    $destinationPath = 'public_images/';
    $targetFile = "enrollImport." . $file->getClientOriginalExtension();
    $file->move($destinationPath, $targetFile);
    $filePath = $destinationPath . "/". $targetFile;
	$class_id = $request->input("select_enrollment_class_import");
	
    Excel::load($filePath, function($reader) {
       $results = $reader->get();
	   $enrollArray = $results->toArray();
	   //print_r($enrollArray);
			
	   foreach ($enrollArray as $enroll_data) {
		  DB:transaction(function() use ($enroll_data) {
			  $nrp = $enroll_data["nrp"];
			  $id_class = $class_id;
			  $user = Users::where('UserId','=',$nrp)->first();
			  //if user null
			  if($user != null) {
				  $id_student = $user->Id;
				  $temp = new Enrollment;
				  $temp->Id_Class = $id_class;
				  $temp->Id_User_Student = $id_student;
				  $result = $temp->save();
			  }
			  else {
				  //kudu diubah datanya jadi satu file aja
			  }
		  });
	   }
    });
    $messages = " entry added";
    return Redirect('Enrollments')->with('messages',$messages);
  }

  public function Import(Request $request) {
    $file = $request->input_file_excel;

    $destinationPath = 'public_images/';
    $targetFile = "enrollImport." . $file->getClientOriginalExtension();
    $file->move($destinationPath, $targetFile);
    $filePath = $destinationPath . "/". $targetFile;

    Excel::load($filePath, function($reader) {
       $results = $reader->get();
	   $enrollArray = $results->toArray();
	   //print_r($enrollArray);
			
       DB::transaction(function() use ($enrollArray){
         foreach ($enrollArray as $enroll) {
		   //print_r($enroll);
		   
		   
		   $nrp = $enroll['nrp']; //nrp
           $id_class = $enroll['id_class']; //id_class
           $user = Users::where('UserId','=',$nrp)->first();
           //echo $nrp . " " . $id_class . "<br>";
		   if ($user !=null) {
		   $id_student = $user->Id;
           //echo $id_student . " " . $id_class."<br>";
           
			$temp = new Enrollment;
			$temp->Id_Class = $id_class;
			$temp->Id_User_Student =  $id_student;
			$result = $temp->save();
		   }
          }

       });
    });
    $messages = " entry added";
    return Redirect('Enrollments')->with('messages',$messages);
  }


  public function GetEnrolledStudent(Request $request) {
    $idClass = $request->input('IdClass');
    $student = DB::table('enrollments')
               ->join('users','enrollments.Id_User_Student','users.Id')
               ->select('users.Id','users.UserId','users.Name')
               ->where('enrollments.Id_Class','=',$idClass)
               ->get();
    return json_encode($student);
  }


  public function Get(Request $request) {
	$dt = Carbon::now()->toDateString();
	$id_Class=0;
	
	$class=Classes::whereHas('Lesson',function($query) {
		$query->whereHas('Semester',function($query2) {
			$query2->where('EndDate','>',NOW());
	});})->get();
			
    if ($request->input('input_enrollment_filter')!= null)  {
		
      $id_Class = $request->input('input_enrollment_filter');
	  echo $id_Class;
      if ($id_Class != 0){
      
	  $temp = Enrollment::where('Id_Class','=',$id_Class)->paginate(14);
		$class = Classes::whereHas('Lesson',function($query) {
		$query->whereHas('Semester',function($query2) {
			$query2->where('EndDate','>',NOW());
		});
		})->get();
      }
      else {
		  
        $temp = Enrollment::paginate(14);
      }
    }
    else {
  	  $temp = Enrollment::paginate(14);
  	 
    }
	
	if ($request->input('input_semester_filter')!= null)  {
      $id_Class = $request->input('input_semester_filter');
	
		if ($id_Class != 0){
			$temp = Semester::where('Year','=',$id_Class)->paginate(14);
			$class = Classes::whereHas('Lesson',function($query) use($id_Class){
			$query->where('Id_Semester',$id_Class) ;
			})->get();
			
	  
		}
	}
	$semester= Semester::all();
  	$users = Users::where('Id-Roles','=','3')->get();
	$lecturers = Users::where('Id-Roles','=','2')->get();
	
  	return View('enrollment_view')->with('lecturers',$lecturers)->with('enrollments',$temp)->with('classes',$class)->with('students',$users)->with('Semester',$semester);
  }

  public function Insert( Request $request) {
	  $temp = new Enrollment;

	  $temp->Id_Class = $request->input('select_enrollment_class');
	  $temp->Id_User_Student = $request->input('select_enrollment_student');
	  $result = $temp->save();

	  if($result) {
		$messages = 'Enrollment succeded';
		return Redirect('Enrollments')->with('messages',$messages);
	  }
	  else {
		$messages = 'Enrollment failed';
		return Redirect('Enrollments')->with('errors',$messages);
	  }

  }

  public function Delete($Id) {
	  $temp = Enrollment::find($Id);
	  $result =  $temp->delete();

	  if($result) {
		$messages = 'Enrollment delete success';
		return Redirect('Enrollments')->with('messages',$messages);
	  }
	  else {
		$messages = 'Enrollment  delete failed';
		return Redirect('Enrollments')->with('errors',$messages);
	  }
  }


}

<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Classes;
use App\Enrollment;
use App\Semester;
use App\Subjects;

use App\Lesson;
use App\Users;

class ClassController extends Controller
{
    //
    public function GetClassServiceDummy(Request $request) {
		//print_r($request);
		$lecturerId = $request->input('input_lecturer_id');
		
		 $lecturer = Users::where('UserId','=',$lecturerId)->first();
		 $lecturerRealId = $lecturer->Id;
		 echo 'lecturer id :' . $lecturerRealId;
		
	     $today = now();
         $activeSemester = Semester::whereDate('StartDate','<',$today->format('Y-m-d'))
                                ->whereDate('EndDate','>',$today->format('Y-m-d'))->get();
	    
		 foreach ($activeSemester as $result) {
			  echo "\nactive : " . $result->Id . " " . $result->SemesterName . " " . $result->StartDate . " " . $result->EndDate;
		 }
		
		  echo '---------------------------------------------------------------------------------------------';
		  
		  $allSemester = Semester::All();
		  foreach ($allSemester as $result) {
			  	  echo "\nactive : " . $result->Id . " " . $result->SemesterName . " " . $result->StartDate . " " . $result->EndDate;
		
		  }
         
	
							
	}
	
	
    public function GetClassServiceLecturer(Request $request) {
      $lecturerId = $request->input('input_lecturer_id');
      $lecturer = Users::where('UserId','=',$lecturerId)->first();
      $lecturerRealId = $lecturer->Id;

      $today = now();
      $activeSemester = Semester::whereDate('StartDate','<',$today->format('Y-m-d'))
                                ->whereDate('EndDate','>',$today->format('Y-m-d'))
                                ->first();
      $activeSemesterId = $activeSemester->Id;

      $detail = DB::table('classes')
                ->join('lessons','classes.Id_Lesson','lessons.Id')
                ->join('subjects','lessons.Id_Subject','subjects.Id')
                ->select('classes.Id','subjects.SubjectName','classes.ClassNumber')
                ->where('classes.Id_User_Lecturer','=', $lecturerRealId)
                ->where('lessons.Id_Semester','=', $activeSemesterId)
                ->get();

      return json_encode($detail);

    }


    public function GetClassService(Request $request) {

      $studentId = $request->input('input_student_id');
      $student = Users::where('UserId','=',$studentId)->first();
      $studentRealId = $student->Id;

     // echo "this is the real id : " . $studentRealId;

      //get active semester
      $today = now();
      $activeSemester = Semester::whereDate('StartDate','<',$today->format('Y-m-d'))
                                ->whereDate('EndDate','>',$today->format('Y-m-d'))
                                ->first();
      $activeSemesterId = $activeSemester->Id;

      /*
      //get lesson
      $activeLesson = Lesson::where('Id_Semester','=',$activeSemesterId)->get();
      $activeLessonId = $activeLesson->pluck('Id');

      //get class
      $activeClass = Classes::whereIn('Id_Lesson', $activeLessonId)->get();
      $activeClassId = $activeClass->pluck('Id');

      //get Enrollment
      $activeRoll = Enrollment::whereIn('Id_Class',$activeClassId)
                              ->where('Id_User_Student','=', $studentId )->get();

      */

      $detail = DB::table('enrollments')
                    ->join('classes','enrollments.Id_Class','classes.Id')
                    ->join('lessons','classes.Id_Lesson','lessons.Id')
                    ->join('subjects','lessons.Id_Subject','subjects.Id')
                    ->select('classes.Id','subjects.SubjectName','classes.ClassNumber')
                    ->where('enrollments.Id_User_Student','=', $studentRealId)
                    ->where('lessons.Id_Semester','=', $activeSemesterId)
                    ->get();

      return json_encode($detail);
    }

    public function GetService() {
        $temp = Classes::all();
        $result = array();
        foreach($temp as $entry) {
          $result[] = $entry;
        }
        return json_encode($result);
    }


    public function Get(Request $request) {
      $semesterid = 0;
	  $dosen_id = 0;
	  
      if ($request->input('select_classes_semester') != null){
        $semesterid = $request->input('select_classes_semester');
      }
	  if ($request->input('select_classes_class') != null){
        $dosen_id = $request->input('select_classes_class');
      }

      if ($semesterid != 0){
  
		if ($dosen_id != 0){
			$temp = Classes::whereHas('lesson', function($q) use ($semesterid) {
				$q->where('Id_Semester','=',$semesterid);
			})->where('Id_User_Lecturer','=', $dosen_id)->paginate(5);
		}
	   else {
  
			$temp = Classes::whereHas('lesson', function($q) use ($semesterid) {
				$q->where('Id_Semester','=',$semesterid);
			})->paginate(5);	
	   }
        $lesson = Lesson::where('Id_Semester','=',$semesterid)->get();
        $semester = Semester::all();
        $lecturer = Users::where('Id-Roles','=',2)->get();
      }
      else{
		 
		if ($dosen_id != 0){
			$temp = Classes::where('Id_User_Lecturer','=', $dosen_id)->paginate(5);
		}
		else {
			$temp = Classes::paginate(5);
		}
        $lesson = Lesson::whereHas('Semester',function($query) {
			$query->where('EndDate','>',NOW());
		})->get();
        $semester = Semester::all();
        $lecturer = Users::where('Id-Roles','=',2)->get();
      }
	  
	
	 
      return View('class_view')->with('old',$request)->with('classes',$temp)->with('semesters',$semester)->with('lecturers',$lecturer)->with('lessons',$lesson);
    }

    public function Insert(Request $request) {
      $temp = new Classes;
      $temp->Id_Lesson = $request->input('select_classes_lesson');
      $temp->Id_User_Lecturer = $request->input('select_classes_lecturer');
      $temp->ClassNumber = $request->input('input_class_classNumber');
      $temp->save();
      $messages = 'class entry inserted';
      return Redirect('Classes')->with('messages',$messages);
    }

    public function Delete($Id) {
      $temp = Classes::find($Id);
      $temp->delete();
      $messages = 'class entry deleted';
      return Redirect('Classes')->with('messages',$messages);
    }

}

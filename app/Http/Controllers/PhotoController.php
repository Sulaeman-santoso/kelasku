<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PhotoController extends Controller
{

    public function Delete (Request $request,$id) {
        $temp = Photo::find($id);
        $temp->delete();
    }

    public function DeleteWeb (Request $request) {
        $id = $request->input('input_id');

        $sc_p_detail = DB::table('schedule_photos_detail')->where('Id_Photo','=',$id)->first();
        $sc_photo = DB::table('schedule_photos')->where('Id','=',$sc_p_detail->Id_SchedulePhoto)->delete();
        
        $sc_p_detail =  DB::table('schedule_photos_detail')->where('Id_Photo','=',$id)->delete();
        DB::table('attendances')->where('Id_Photo','=',$id)->delete();
        $result = Photo::find($id)->delete();

        $messages  = "delete unsuccessfull";
        if ($result)
            $messages = "delete successfull";

        return json_encode($result);
        //find attendance or find schedule_photo_detail yang berhubungan dengan photo ini 

    }


    public function Update(Request $request) {
        $id = $request->input('input_id');

        $temp = Photo::find($id);
        $temp->Path = $new_path;
        $temp->save();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        //
    }
}

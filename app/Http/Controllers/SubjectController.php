<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Subject;


class SubjectController extends Controller
{
    public function Get(Request $request) {
	   //$temp = Subject::paginate(5);
	   //return View('subjects_view')->with('Subjects',$temp);
	   
	    $q = $request->input('input_subject_search');
		$subject = Subject::where('SubjectId','=',$q)
               ->orWhere('SubjectName','LIKE','%'.$q."%")->paginate(5);
		return View('subjects_view')->with('Subjects',$subject);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function Insert(Request $request)
    {
        //
        $temp = new Subject;
        $temp->SubjectId=$request->input('input_subject_id');
        $temp->SubjectName=$request->input('input_subject_name');
        $temp->save();
        $message = 'Subject Entry added';
        return  Redirect('Subjects')->with('messages',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function Delete($Id)
    {
      $temp= Subject::find($Id);
      $temp->delete();
      $message = 'Subject Entry Deleted';
      return  Redirect('Subjects')->with('messages',$message);


        //
    }
}

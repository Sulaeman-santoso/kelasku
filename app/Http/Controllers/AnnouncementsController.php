<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Enrollment;
use App\Classes;
use App\Semester;
use App\Users;
use App\Schedule;
use App\Announcements;
use App\Photo;
use App\Tags;
use App\Announcement_Tag;


class AnnouncementsController extends Controller
{
    //Route::get('Announcements','AnnouncementController@Get');
    public function Get(Request $request,$viewId){
      return ('entry_main');
    }



    public function InsertService(Request $request) {

      //getting information
      $id_user = $request->input('id_user');
      $title = $request->input('title');
      $content = $request->input('content');
      $photo = $request->input('photo');
      $filePhoto = $request->input_filephoto;
      $publish_date = $request->input('publish_date');
      $expire_date = $request->input('expire_date');
      $tags = $request->input('tags');



      if ($filePhoto === null){
            $result = array();
            $result["id"]= 0;
            $result["message"] = 'no photo found';
            return json_encode($result);
      }
      else {
        if (is_array($filePhoto)){
          print_r($filePhoto);
          $filePhoto = $filePhoto;
        }

        $public_root_dir = "http://124.81.122.93/laravel/public/public_ann/";
        $local_root_dir = "public_ann/";

        $destinationPath = $local_root_dir . "/" . $id_user  ."/";
        if (!file_exists($destinationPath)) { //cek if directory exist
           mkdir($destinationPath, 0755);
         }
        //disini kudu masukin file gambarnya
        $jpgCount = count(glob($destinationPath . "*.{jpg,png,gif,jpeg}",GLOB_BRACE));
        $intEntry = $jpgCount+1;

        /*try {
          $targetFile = $intEntry. ".". $filePhoto->getClientOriginalExtension();
        }
        catch(Exception $e) {
          $targetFile = $intEntry. ".". 'jpg';
        }*/
        $targetFile = $intEntry. ".". 'jpg';
        //echo "<hr> targetFile : " .$targetFile;

        while (file_exists($destinationPath . $targetFile)){
        //  echo "<hr>file exists ".  $targetFile;
          $intEntry = $intEntry +1;
          //$targetFile = $intEntry. ".". $filePhoto->getClientOriginalExtension();
          $targetFile = $intEntry. ".". 'jpg';
        }

  //      should be clear now
  //      $targetFile = $intEntry. ".". $filePhoto->getClientOriginalExtension();
        $filePhoto->move($destinationPath, $targetFile);
        list($width, $height, $type, $attr) = getimagesize($destinationPath. "/". $targetFile);

        DB::beginTransaction();
        try {

          $poto = new Photo;
          $poto->Path = $public_root_dir . $id_user ."/" . $targetFile;
          $poto->save();

          //insert tag dulu
          $tagSplit = explode(",",$tags);
          $tagIdList = array();


          foreach ($tagSplit as $tag) {
            $tagContent = strtolower(trim($tag));
            $temp = Tags::Where('Content','=',$tagContent)->first();
            if (!$temp) {
              $temp = new Tags;
              $temp->Content = $tagContent;
              $temp->save();
            }

            $tagIdList[] = $temp->Id;
          }


          //tags sudah ditangani tinggal insert announcement
          $ann = new Announcements;
          $ann->Title = $title;
          $ann->Photo_Ratio = $width/$height;
          $ann->Content = $content;
          $ann->Id_Photo = $poto->Id;
          $ann->Id_User_Lecturer = $id_user;
          $ann->Publish_Date =  $publish_date;
          $ann->Expire_Date = $expire_date;
          $ann->save();
          $idAnn = $ann->Id;

          //isi relasinya
          //print_r($tagIdList);

          foreach ($tagIdList as $tagId) {
            $temp = new Announcement_Tag;
            $temp->Id_Announcement = $idAnn;
            $temp->Id_Tag = $tagId;
            $temp->save();
          }

        //kalo mau pake transaction just in case

      //end of try
      } catch (ValidationException $e) {
        DB::rollback();
        $result = array();
        $result["id"]= 0;
        $result["message"] = 'insert failed';
        return json_encode($result);
      }
      DB::commit();

      $result = array();
      $result["id"]= 1;
      $result["message"] = 'insert successfull';
      return json_encode($result);
      }
    //  $result = This.InsertMethod();
    //  return json_encode($result);
    }

    public function Insert(Request $request) {

    }

    public function getLocalPath($public_path) {
      $local_root_dir = "public_ann/";
      $parts = explode('/',$public_path);
      if (count($parts) > 2) {
        $addon = $parts[count($parts)-2] ."/" . $parts[count($parts)-1];
      }
      return $local_root_dir . $addon ;
    }

    public function DeleteService(Request $request) {


      $Id = $request->input('id_ann');

      $result = array();
      $result["id"]= 0;
      $result["message"] = 'delete failed';


      DB::transaction(function() use ($Id,$result){

          try {
              $temp = Announcement_Tag::Where('Id_Announcement','=',$Id)->delete();
              $ann = Announcements::Find($Id);
              if ($ann) {
                $photo = Photo::Find($ann->Id_Photo);

                $path_photo = $photo->Path;
                echo $path_photo;
                $local_path = $this->getLocalPath($path_photo);
                echo "<hr>". $local_path;
                unlink($local_path); //delete the actual file

                $photo->delete();
                $ann->delete();
              }
          }
          catch(Exception $e) {
            echo "an error has occured" .$e;
          }

          $result["id"]= 1;
          $result["message"] = 'delete succesfull ';
          return json_encode($result);
          //  $result = This.InsertMethod();
      });

      return json_encode($result);

    }

    public function GetLecturer(Request $request,$viewId) {
	    $pageNumber = 10;
	    $chosen_user = 33;
      //$chosen_user = $request->session()->get('lecturer_id');
	    //get Announcement Related to specific lecturer

	  	if ($viewId == 0) {
	    	$announcements = Announcements::Where('Id_User_Lecturer', '=', $chosen_user )
	 									->orderBy('Id','desc')->paginate($pageNumber);
			 return View('lecturer_pengumuman_view')->with('chosen_user',$chosen_user)->with('announcements',$announcements)->with("view_id",$viewId);
		  }
		  else if($viewId == 1) {
        return View('lecturer_pengumuman_view')->with('chosen_user', $chosen_user)->with('view_id',$viewId);
		  }
		  else if ($viewId == 2) {
        return View('lecturer_pengumuman_view')->with('chosen_user', $chosen_user)->with('view_id',$viewId);
		  }
	}

}

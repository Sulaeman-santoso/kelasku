<?php

namespace App\Http\Controllers;

use App\Semester;
use App\SemesterName;
use Illuminate\Http\Request;

class SemesterController extends Controller
{
   function Get(Request $request) {
     $yearFilter = 0;
     if($request->input('select_year_filter') != null){
       $yearFilter = $request->input('select_year_filter');
     }
     if($yearFilter != 0) {
          $semester = Semester::where('Year',$yearFilter)->paginate(5);
     }
     else {
          $semester = Semester::paginate(5);
     }

     $semesterall = Semester::all();
	   $semesternames = SemesterName::all();
	   return View('semester_view')->with('semesternames',$semesternames)->with('semesters',$semester)->with('semesterall',$semesterall);
   }

   function Insert(Request $request) {
	   $temp = new Semester;
	   $temp->{'Id_Semester'} = $request->input('select_semestername');
	   $temp->year = $request->input('input_semester_year');
	   $temp->StartDate = $request->input('input_semester_startDate');
	   $temp->Enddate = $request->input('input_semester_endDate');

	   $temp->save();
	   $messages = 'insert semester completed';
	   return Redirect('Semesters')->with('messages',$messages);
   }

   function Delete($Id) {
	   $temp = Semester::find($Id);
	   $temp->delete();
	   $temp->save();
	   $messages = 'delete semester completed';
	   return Redirect('Semesters')->with('messages',$messages);
   }

}

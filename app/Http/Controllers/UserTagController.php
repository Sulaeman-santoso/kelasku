<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User_Tag;
use App\Announcements;
use App\Announcement_Tag;

use App\Tags;

class UserTagController extends Controller
{
    //

    function GetTag(Request $request) {
      $id_user = $request->input('Id_User');

      //$temp = User_Tag::where('Id_User','=',$id_user)->get();//naive ahahahaha
      $temp = DB::table('tags')
        ->join('user_tag','user_tag.Id_Tag','tags.Id')
        ->select('tags.Id','tags.Content')
        ->groupby('tags.Id','tags.Content')
        ->get();

      return json_encode($temp);
    }

    function GetTagPaginate(Request $request) {
      $id_user = $request->input('Id_User');

      $pageLimit = 10;
      $temp = Tags::paginate($pageLimit);
      return json_encode($temp);
    }


    function AssignTag(Request $request) {
      $id_user = $request->input('Id_User');
      $id_tag = $request->input('Id_Tag');

      //gak ngetest dulu
      $temp = new User_Tag;
      $temp->Id_User = $id_user;
      $temp->Id_Tag = $id_tag;

      $finder = User_Tag::where('Id_User','=',$id_user)->where('Id_Tag','=',$id_tag)->first();
      $result=0;
      if ($finder == null) {
        $temp->save();
        $result = 1;
      }

      $returnObject = array(
        'id' => ($result)?0:1,
        'message' => ($result)?"Assign Successful":"Assign Failed (duplicate or faulty)"
      );

      return json_encode($returnObject);
    }

    function UnAssignTag(Request $request) {
      $id_user = $request->input('Id_User');
      $id_tag = $request->input('Id_Tag');

      $finder = User_Tag::where('Id_User','=',$id_user)->where('Id_Tag','=',$id_tag)->first();
      $result = 0;
      if ($finder) {
        $finder->delete();
        $result = 1;
      }

      $returnObject = array(
        'id' => ($result)?0:1,
        'message' => ($result)?"UnAssign Successful":"UnAssign Failed (Wrong or No Data)"
      );
      return json_encode($returnObject);
    }

    function GetAnnByTag(Request $request) {
      $id_user = $request->input('Id_User');


      $announcement_filtered = DB::table('announcements')
              ->join('announcement_tag','announcement_tag.Id_Announcement','announcements.Id')
              ->join('tags','announcement_tag.Id_Tag','tags.Id')
              ->join('user_tag','user_tag.Id_Tag','tags.Id')
              ->where('user_tag.Id_User','=',$id_user)
              ->select('announcements.Id','announcements.Title','announcements.Content')
              ->groupby('announcements.Id','announcements.Title','announcements.Content')
              ->get();

      return json_encode($announcement_filtered);

    }

}

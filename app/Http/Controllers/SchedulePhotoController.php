<?php

namespace App\Http\Controllers;

use App\SchedulePhoto;
use App\SchedulePhotoDetail;
use App\Schedule;
use App\Classes;
use App\Lesson;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SchedulePhotoController extends Controller
{

  public static function storeScheduleImage($idSchedule,$file) {

  $query = DB::table('schedules')
         ->join('classes','schedules.Id_Class','=','classes.Id')
         ->join('lessons','classes.Id_Lesson','=','lessons.Id')
         ->join('subjects','lessons.Id_Subject','=','subjects.Id')
         ->join('semesters','lessons.Id_Semester','=','semesters.Id')
         ->join('semester_names','semesters.Id_Semester','=','semester_names.Id')
         ->where('schedules.Id','=',$idSchedule)
         ->select('semesters.Year as Year','semester_names.Name AS Semester','subjects.SubjectName as Subject',
         'classes.ClassNumber as ClassNum','schedules.ScheduleDate as Dates')
         ->first();
     //->get('semesters.Year as Year','semester_names AS Semester','subjects.SubjectName as Subject','classes.ClassNumber as ClassNum','schedules.ScheduleDate as Dates');

   $semester_year = $query->Year;
   $semester_name = $query->Semester;
   $subject_name = $query->Subject;
   $class_name = $query->ClassNum;
   $sched_date = $query->Dates;

   $public_path = 'http://124.81.122.93/laravel/public/public_images/' . $semester_year;
   $destinationPath = 'public_images/'. $semester_year;
   if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

   $public_path .= '/'. $semester_name ;
   $destinationPath .= '/'. $semester_name ;
   if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

   $public_path .= '/'. $subject_name;
   $destinationPath .= '/'. $subject_name ;
   if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

   $public_path .= '/'. $class_name;
   $destinationPath .= '/'. $class_name;
   if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

   $public_path .= '/'. $sched_date  .'/';
   $destinationPath .= '/'. $sched_date  .'/';
   if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
    }

    $jpgCount = count(glob($destinationPath . "*.{jpg,png,gif,jpeg}",GLOB_BRACE));
    $intEntry = $jpgCount+1;
    $targetFile = $intEntry. ".". $file->getClientOriginalExtension();

    while (file_exists($destinationPath . $targetFile)){
    //  echo "<hr>file exists ".  $targetFile;
      $intEntry = $intEntry +1;
      $targetFile = $intEntry. ".". $filePhoto->getClientOriginalExtension();
    }

    $file->move($destinationPath, $targetFile);
    $fileTotalPath = $destinationPath . $targetFile;
    $filePublicPath = $public_path . $targetFile;

    echo "file total path = $fileTotalPath";
    echo "<hr>";
    echo "file public path = $filePublicPath";
    echo "<hr>";


    $photos = new Photo;
    $photos->Path = $fileTotalPath;
    $photos->Path = $filePublicPath;
    //$photos->save();
    $photoId = $photos->Id;

    $sp = new SchedulePhoto;
    $sp->Id_Photo = $photoId;
    $sp->Id_Schedule = $idSchedule;
    // $sp->save();

    //do recog here
    $pyExecute ="python  /var/www/html/laravel/splitImage.py '". $destinationPath . "' " . $targetFile . " " . $sp->Id;
    $result = exec($pyExecute ,$output,$output_var);
    echo ("result : " . $result);


    return $result;
  }

  public function InsertWeb(Request $request) {
    $photos = new Photo;
    $files = $request->input_schedulePhotos_photos; //get file //can be multiple
    $idSchedule = $request->input('select_schedule_name');

    $success_rate = array();

    foreach ($files as $file) {

       $result = SchedulePhotoController::storeScheduleImage($idSchedule,$file);
       // $file->store('test/test.jpg');
       // echo "multiple files";
    }
    return $success_rate;
  }

  public function Get(Request $request) {
    $classid = 0;
	  $scheduleId = 0;
   	$classes =  DB::table('classes')
                ->join('lessons','classes.Id_Lesson','lessons.Id')
                ->join('subjects','lessons.Id_Subject','subjects.Id')
                ->join('semesters','lessons.Id_Semester','semesters.Id')
                ->select('classes.Id','classes.ClassNumber','subjects.SubjectName')
                ->where('semesters.EndDate','>',NOW())->get();

   if($request->input('select_schedule_filter')!= null){
      $scheduleId = $request->input('select_schedule_filter');
	 }

    if($request->input('select_class_filter')!= null){
      $classid = $request->input('select_class_filter');
    }

    if ($classid != 0) { //filter photo by class

        $temp = SchedulePhoto::whereHas('Schedule',function($q4) use ($classid){
          $q4->whereHas('Classes',function($q) use($classid) {
            $q->where('Id','=',$classid);
            $q->whereHas('Lesson', function($q2) {
              $q2->whereHas('Semester',function($q3) {
                $q3->where('EndDate','>',NOW());
              });
            });
          });
        })
        ->orderBy('Id','desc')
        ->paginate(14);


    }
    else { //filter photo by none;
      $temp = SchedulePhoto::whereHas('Schedule',function($q4) {
		  $q4->whereHas('Classes',function($q) {
			$q->whereHas('Lesson', function($q2) {
				$q2->whereHas('Semester',function($q3) {
					$q3->where('EndDate','>',NOW());
				});
			});
		});
	  })->orderBy('Id','desc')
      ->paginate(14);

    }

    $schedules = Schedule::orderBy('Id','desc');

    return View('schedule_photos_view')->with("chosen_class",$classid)->with('schedulePhotos',$temp)->with('schedules',$schedules)->with('classes',$classes);

  }

  public function ShowPhoto($Id) {
    $temp = Photo::where('Id','=',$Id)->first();

    return View('show_photo')->with('photo',$temp);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\SchedulePhoto  $schedulePhoto
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, SchedulePhoto $schedulePhoto)
  {
      //in short versionnya ini .. kudu apus dulu baru add lagi..bedanya si schedulephoto entrynya sama.


  }

  public function InsertService(Request $request) {
    //
	try {

    $idClass = $request->input('input_schedulePhotos_idClass');
    $date = $request->input('input_schedulePhotos_Date');
	  $add = "";
    $finder = Schedule::Where('ScheduleDate','=',$date)->Where('Id_Class','=',$idClass)->get();

    $scheduleId = 0;
    if (count($finder)){
      // berarti ada schedulenya

    $scheduleId = $finder->first()->Id;//
	  $scheduleName = $finder->first()->classes->lesson->subject->SubjectName;
	  $classNumber = $finder->first()->classes->ClassNumber;
	  $add = " Mata Kuliah : " . $scheduleName . "-" . $classNumber ."-" .$date ;
	// $add .=" Duplicate date: " . $date . " with " .  $finder->first()->scheduleDate;
    }
    else {
      // bikin baru
      $temp = new Schedule;
      $temp->Id_Class = $idClass;
      $temp->ScheduleDate = $date;
      $result = $temp->save();
      $scheduleId = $temp->Id;
    }
    //insert picture...
    $result = $this->InsertSchedulePhoto($request,$date, $scheduleId);
    //output result

     if($result) {
        return "Insert Successfull" . $add . " ";
     }
     else {
        return "Insert Failed";
     }
	}
	catch(Exception $e) {
		return  "Exception " . $e->getMessage();
	}
  }


  public function InsertSchedulePhoto(Request $request, $date, $idSchedule) {
    //get file
	try {
    $file = $request->input_schedulePhotos_photos;
	/*
	OLD PATH
    //harus namain si initial filenya berdasarkan jadwal kelas yang mana
    $destinationPath = 'public_images/'. $idSchedule .'/';
    $publicPath = 'http://124.81.122.90/laravel/public/public_images/'. $idSchedule .'/';
	$publicPath = 'http://124.81.122.93/laravel/public/public_images/'. $idSchedule .'/'; //path terbaru
	*/
	  $query = DB::table('schedules')
			   ->join('classes','schedules.Id_Class','=','classes.Id')
			   ->join('lessons','classes.Id_Lesson','=','lessons.Id')
			   ->join('subjects','lessons.Id_Subject','=','subjects.Id')
			   ->join('semesters','lessons.Id_Semester','=','semesters.Id')
			   ->join('semester_names','semesters.Id_Semester','=','semester_names.Id')
			   ->where('schedules.Id','=',$idSchedule)
			   ->select('semesters.Year as Year','semester_names.Name AS Semester','subjects.SubjectName as Subject',
			   'classes.ClassNumber as ClassNum','schedules.ScheduleDate as Dates')
			   ->first();
     //->get('semesters.Year as Year','semester_names AS Semester','subjects.SubjectName as Subject','classes.ClassNumber as ClassNum','schedules.ScheduleDate as Dates');

	 $semester_year = $query->Year;
	 $semester_name = $query->Semester;
	 $subject_name = $query->Subject;
	 $class_name = $query->ClassNum;
	 $sched_date = $query->Dates;

	 $public_path = 'http://124.81.122.93/laravel/public/public_images/' . $semester_year;
	 $destinationPath = 'public_images/'. $semester_year;
	 if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

	 $public_path .= '/'. $semester_name ;
	 $destinationPath .= '/'. $semester_name ;
	 if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

	 $public_path .= '/'. $subject_name;
	 $destinationPath .= '/'. $subject_name ;
	 if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

	 $public_path .= '/'. $class_name;
	 $destinationPath .= '/'. $class_name;
	 if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

	 $schedDate = substr($sched_date,0,16); //get only date :D // i should lose this
  // $schedDate = $sched_date;
   //$schedDate = $date;
	 $public_path .= '/'. $schedDate .'/';
	 $destinationPath .= '/'. $schedDate  .'/';
	 if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
    }

    /* nah ieu rese... kudu liat jumlah file terakhir baru tahu nomor terakhirnya berapa*/
    $path = $destinationPath;
   // echo "destinationPath : $path  $schedDate";
    $jpgCount = count(glob($destinationPath . "*.{jpg,png,gif,jpeg}",GLOB_BRACE));
	  //print_r(glob($destinationPath . "*.{jpg,png,gif,jpeg}",GLOB_BRACE));
    $intEntry = $jpgCount;
	  //echo "jumlah jpg yang terhitung : $jpgCount";
    $intEntry = $intEntry+1;


	$targetFile = $intEntry. ".". $file->getClientOriginalExtension();

  while (file_exists($destinationPath . $targetFile)){
  //  echo "<hr>file exists ".  $targetFile;
    $intEntry = $intEntry +1;
    $targetFile = $intEntry. ".". $filePhoto->getClientOriginalExtension();
  }


    $file->move($destinationPath, $targetFile);


    $fileTotalPath = $destinationPath . $targetFile;
    $filePublicPath = $public_path . $targetFile;

   // echo $fileTotalPath . " file (original ) ke : " . $intEntry;
    $photos = new Photo;
    $photos->Path = $fileTotalPath;
    $photos->Path = $filePublicPath;

    $photos->save();
    $photoId = $photos->Id;

    $sp = new SchedulePhoto;
    $sp->Id_Photo = $photoId;
    $sp->Id_Schedule = $idSchedule;
    $result = $sp->save();

    //excute python script here
    exec("python  /var/www/html/laravel/splitImage.py '". $destinationPath . "' " . $targetFile . " " . $sp->Id ,$output,$output_var);
    //print_r($output);

    return $result;
	}
	catch(Exception $e) {
		echo "Exception : " . $e->getMessage();
		return null;
	}

  }


  public function InsertRevised(Request $request) {
	  $photos = new Photo;
	  $file = $request->input_schedulePhotos_photos; //get file
	  $idSchedule = $request->input('select_schedule_name');

	 // print $idSchedule . ' ';

	  $query = DB::table('schedules')
			   ->join('classes','schedules.Id_Class','=','classes.Id')
			   ->join('lessons','classes.Id_Lesson','=','lessons.Id')
			   ->join('subjects','lessons.Id_Subject','=','subjects.Id')
			   ->join('semesters','lessons.Id_Semester','=','semesters.Id')
			   ->join('semester_names','semesters.Id_Semester','=','semester_names.Id')
			   ->where('schedules.Id','=',$idSchedule)
			   ->select('semesters.Year as Year','semester_names.Name AS Semester','subjects.SubjectName as Subject',
			   'classes.ClassNumber as ClassNum','schedules.ScheduleDate as Dates')
			   ->first();
     //->get('semesters.Year as Year','semester_names AS Semester','subjects.SubjectName as Subject','classes.ClassNumber as ClassNum','schedules.ScheduleDate as Dates');

	 $semester_year = $query->Year;
	 $semester_name = $query->Semester;
	 $subject_name = $query->Subject;
	 $class_name = $query->ClassNum;
	 $sched_date = $query->Dates;
   $sched_date = substr($sched_date,0,16);

	 $public_path = 'http://124.81.122.93/laravel/public/public_images/' . $semester_year;
	 $destinationPath = 'public_images/'. $semester_year;
	 if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

	 $public_path .= '/'. $semester_name ;
	 $destinationPath .= '/'. $semester_name ;
	 if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

	 $public_path .= '/'. $subject_name;
	 $destinationPath .= '/'. $subject_name ;
	 if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }

	 $public_path .= '/'. $class_name;
	 $destinationPath .= '/'. $class_name;
	 if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
     }


	 $public_path .= '/'. $sched_date  .'/';
	 $destinationPath .= '/'. $sched_date  .'/';
	 if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
    }

	  $jpgCount = count(glob($destinationPath . "*.{jpg,png,gif,jpeg}",GLOB_BRACE));
	  $intEntry = $jpgCount+1;

	  $targetFile = $intEntry. ".". $file->getClientOriginalExtension();

    while (file_exists($destinationPath . $targetFile)){
    //  echo "<hr>file exists ".  $targetFile;
      $intEntry = $intEntry +1;
      $targetFile = $intEntry. ".". $filePhoto->getClientOriginalExtension();
    }


	  $file->move($destinationPath, $targetFile);
    $fileTotalPath = $destinationPath . $targetFile;
    $filePublicPath = $public_path . $targetFile;

    //echo $fileTotalPath . "file ke : " . $intEntry;


    $photos->Path = $fileTotalPath;
    $photos->Path = $filePublicPath;

    $photos->save();
    $photoId = $photos->Id;

    $sp = new SchedulePhoto;
    $sp->Id_Photo = $photoId;
    $sp->Id_Schedule = $request->input('select_schedule_name');
    $sp->save();


    //excute python script here
	  $pyExecute ="python  /var/www/html/laravel/splitImage.py '". $destinationPath . "' " . $targetFile . " " . $sp->Id;
  //$pyExecute ="python  /var/www/html/laravel/splitImage.py '". $destinationPath . "' " . $targetFile ;

    print '<hr>' . $pyExecute . '<hr>';
	  exec($pyExecute , $output, $output_var);

    //print_r($output);
/*
    for ($i=0;$i<count($output);$i++) {
      echo "<hr>" . $output[$i];
    }
*/
    $messages= 'insert schedule photos succeeded';
    return Redirect('SchedulePhotos')->with('messages',$messages);

  }


  public function Insert(Request $request) {
/**
  images should be placed under a directory with the following rules
  1. under id_schdule/scheduledate/uploadNumber.jpg
  2. resulting face then should be placed in id_schedule/scheduledate/output/resultNo.jpg
*/
    $photos = new Photo;
    //move input file
    $file = $request->input_schedulePhotos_photos;
  /**
    harus namain si initial filenya berdasarkan jadwal kelas yang mana
  */
    $destinationPath = 'public_images/'. $request->input('select_schedule_name') .'/';
    $publicPath = 'http://124.81.122.90/laravel/public/public_images/' . $request->input('select_schedule_name') .'/';
	$publicPath = 'http://124.81.122.93/laravel/public/public_images/' . $request->input('select_schedule_name') .'/';


    if (!file_exists($destinationPath)) { //cek if directory exist
      mkdir($destinationPath, 0777);
    }
    $sched = Schedule::find($request->input('select_schedule_name'));
    $schedDate = substr($sched->scheduleDate,0,10);
/**
  ditambah dengan tanggal pertemuan
*/
    $destinationPath = $destinationPath . $schedDate   ."/";
    $publicPath = $publicPath . $schedDate ."/";

    if (!file_exists($destinationPath)) {
      mkdir($destinationPath, 0777);
    }

    /**
      nah ieu rese... kudu liat jumlah file terakhir baru tahu nomor terakhirnya berapa
    */
    $path = $destinationPath;

    $latest_ctime = 0;
    $latest_filename = '';

    $d = dir($path);
    print_r($d);
    while (false !== ($entry = $d->read())) {
      $filepath = "{$path}/{$entry}";
      echo '<br>filepath : ' . $filepath;
      // could do also other checks than just checking whether the entry is a file
      if (is_file($filepath) && filectime($filepath) > $latest_ctime) {
        $latest_ctime = filectime($filepath);
        $latest_filename = $entry;
      }
    }
    $entry = $latest_filename;
    //echo "<br>Entry =" . $latest_filename;
    $subEntry = substr($entry,0,strlen($entry) -4);
    //echo "<br>subentry " . $subEntry;
    $intEntry = $subEntry *1;
    //echo '<br>intentry = ' . $intEntry;
    $intEntry = $intEntry+1;
    $targetFile = $intEntry. ".". $file->getClientOriginalExtension();
    //echo '<br>' . $targetFile ."<br>";
    //$file->move($destinationPath, $schedDate . "." . $file->getClientOriginalExtension());
    $file->move($destinationPath, $targetFile);

    $fileTotalPath = $destinationPath . $targetFile;
    $filePublicPath = $publicPath . $targetFile;

    //echo $fileTotalPath;
    $photos->Path = $fileTotalPath;
    $photos->Path = $filePublicPath;

    $photos->save();
    $photoId = $photos->Id;

    $sp = new SchedulePhoto;
    $sp->Id_Photo = $photoId;
    $sp->Id_Schedule = $request->input('select_schedule_name');
    $sp->save();

    //excute python script here
    exec('python  /var/www/html/laravel/splitImage.py '. $destinationPath . ' ' . $targetFile . " " . $sp->Id ,$output,$output_var);
//    echo "<br>";
    print_r($output);
//    echo '<br>';
  //  print_r($output_var);

    $messages= 'insert schedule photos succeeded';
    return Redirect('SchedulePhotos')->with('messages',$messages);
  }

  public function Delete(Request $request, $Id) {
    $sp = SchedulePhoto::find($Id);

    //apus dulu gambarnya plz
    $fileImagePath = $sp->Path;
    //before deleting primary image should delete image first
    $childImage = SchedulePhotoDetail::where('Id_SchedulePhoto','=', $sp->Id_Photo)->get();

    //apus file file kecilnya dulu
    $childIds = array();
      foreach ($childImage as $entry){
        $childIds[] = $entry->Id;
        //delete the real child file
        $photos = Photo::where('Id','=',$entry->Id_Photo)->first(); //entry photonya
        //find attendance yang id photonya ini .. delete
        $att = Attendance::where('Id_Photo','=',$entry->Id_Photo)->delete(); //apus attendance yang udah keburu terkait ama photo kecilnya

        if ($photos) {
          $path = substr($photos->Path,21,strlen($photos->Path));
          $path = '/var/www/html/'  . $path;
          if (file_exists($path)){
            unlink($path);
          }
        }

        Photo::find($entry->Id_Photo)->delete(); //apus photonya
      }


     SchedulePhotoDetail::whereIn('Id_SchedulePhoto',$childIds)->delete();//children deleted

      $p = Photo::find($sp->Id_Photo);

      if ($p) {
          $path = substr($p->Path,21,strlen($p->Path));
          $path = '/var/www/html/'  . $path;

         $Errormessages = "";
         if (file_exists($path)) {
            unlink($path);
            //hapus jsonnya juga
            $exp_path = explode(".",$path); //harusnya /var/www/html/blablabla/1.jpg
            $path = $exp_path[0] . ".json";
            if (file_exists($path)) {
              unlink($path);
            }
          }
      //unlink($p->Path); //hapus file aslinya
      $p->delete(); //photo deleted;
     }
     else {
       $Errormessages[] = ( "Warning !! schedule photo might not have been deleted" );
     }

     $sp->delete(); //link dengan pertemuan di hapus
     $messages= 'Delete schedule photos succeeded';
     //return Redirect('SchedulePhotos')->with('messages',$messages);
	   return Redirect()->back()->with('messages',$messages)->with('errors',$Errormessages);
  }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SchedulePhoto  $schedulePhoto
     * @return \Illuminate\Http\Response
     */
    public function show(SchedulePhoto $schedulePhoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SchedulePhoto  $schedulePhoto
     * @return \Illuminate\Http\Response
     */
    public function edit(SchedulePhoto $schedulePhoto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SchedulePhoto  $schedulePhoto
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchedulePhoto $schedulePhoto)
    {
        //
    }
}

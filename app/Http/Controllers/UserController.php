<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;
use App\Users;
use App\Attendance;
use App\Classes;
use App\Enrollment;
use App\Photo;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{

  function getPP(Request $request) {


  }

  function UploadPP(Request $request) {
	try {
	
	    $file = $request->input_pp_photo;
	    $chosen_one = $request->input('input_id_user');
	 
		 $public_path = 'http://124.81.122.93/laravel/public/public_images/profile_pictures/' . $chosen_one;
	 	 $destinationPath = 'public_images/profile_pictures/'. $chosen_one;

	 	
		 if (!file_exists($destinationPath)) { //cek if directory exist
	      mkdir($destinationPath, 0777);
	     }
		 
	    $path = $destinationPath;

	 
	    $jpgCount = count(glob($destinationPath . "*.". $file->getClientOriginalExtension()));
	  	$intEntry = $jpgCount;
		$intEntry = $intEntry+1;
	    
		$targetFile = $intEntry. ".". $file->getClientOriginalExtension();
	    $file->move($destinationPath, $targetFile);

	
	    $fileTotalPath = $destinationPath ."/". $targetFile;
	    $filePublicPath = $public_path . "/". $targetFile;


	    
		$photos = new Photo;
	    $photos->Path = $fileTotalPath;
	    $photos->Path = $filePublicPath;

	    $photos->save();
	    $photoId = $photos->Id;

	    $found_user = Users::find($chosen_one);
	    $found_user->profile_picture = $photoId;
	    $found_user->has_pp = 1;
	    $result = $found_user->save();

		if($result) {
		    $returnObject = array(
		      "Id" => "1",
		      "Message" => "UploadPP worked "
		    );
	 	}
		 else {
		    $returnObject = array(
		      "Id" => "2",
		      "Message" => "UploadPP  Failed"
		    );
		 }
 		return json_encode($returnObject);
   	 
	}
	catch(Exception $e) {
		echo "Exception : " . $e->getMessage();
		return null;
	}


  }

  function Import(Request $request) {
	  $file = $request->input_file_excel;

	  $destinationPath = 'public_images/';
	  $targetFile = "userImport." . $file->getClientOriginalExtension();
	  echo $destinationPath  . " " . $targetFile;
	  $file->move($destinationPath, $targetFile);

	  $filePath = $destinationPath . "/". $targetFile;

	  Excel::load($filePath, function($reader) {
		//reader method
		echo "<br>excel is loading<br>";


		$results = $reader->get();
		$userArray = $results->toArray();

		foreach ($userArray as $user) {
			print_r( $user );
			//foreach ($user as $entry) {
			    
				//print_r($entry);
				//echo "<br>";/*
				$temp = new Users;
				$temp->UserId = $user['nrp'];
				//echo $user;
				
				$finder = Users::where('UserId','=',$temp->UserId)->count();
				if ($finder == 0){
					$temp->Name = $user['name'];
					$temp->Password = MD5($user['password']);
					$temp->{'Id-Roles'} = $user['roles'];
					$temp->profile_picture = 0;//default picture
					$temp->save();
				} 
				
			//}
			//echo $user[0]["name"];
			//echo $user["nama"];
			//echo $user['nrp'] . "-" . $user['name'] ."-". $user['password'] ."<br>";
			//insert this to database

		}
	  });
	 $messages = 'Bulk import completed';
	 return Redirect('Users')->with('messages',$messages);
  }

  function Get(Request $request) {
	 $user=Users::all();
    $q = $request->input('input_user_search');
	$role = Roles::all();
	$user = Users::where('UserId','=',$q)
                ->orWhere('Name','LIKE','%'.$q."%")->paginate(5);
			
				
	$roleFilter = 0;
    if($request->input('select_role_filter') != null){
      $roleFilter = $request->input('select_role_filter');
    
		if($roleFilter != 0) {
			$role = Roles::wherehas('User',function ($q) use($roleFilter){
				$q->where('Id-Roles',$roleFilter);
			})
			->paginate(5);
			
		}
		else {
			$role = Roles::paginate(5);
		}
	}
	else {
        $role = Roles::paginate(5);
    }
    
  //  var_dump($user);
  		//$roleList = DB::table('roles')->get();
    return View('users.view')->with('roles',$role)->with('users',$user);
	
  }

  function AuthLogin(Request $request) {
	  $Id = $request->input('input_user_id');
	  $password = $request->input('input_user_password');
	  
	  echo "it doesn't works";
	  $credentials=array(
		'Id' => $Id,
		'password'  => $password
	  );
	  print_r($credentials);
	 
		if (Auth::attempt($credentials)) {
		  echo "it works lol";
		}
		else {
			echo "what's wrong nya ";
		}
	  
  }
  
  
  function LoginService(Request $request){

 	$UserId = $request->input('input_user_id');
  	$Password = $request->input('input_user_password');

  	//echo $UserId ."----" . $Password;
  	//echo MD5($Password);
  	$findUser = Users::where('UserId','=',$UserId)->get();
  	if (count($findUser)== 0){
  		//user not found
		$response = array(
  			'Id' => '0',
  			'UserId' =>'0',
  			'Name' =>'0',
  			'Id_Roles' => '0',
  			'Message' => 'Username not found.'
  		);
  		return json_encode($response);
  	}

  	$User = Users::where('UserId','=',$UserId)->where('Password',MD5($Password))->first();

  	if (count($User)){
  		$response = array(
  			'Id' => $User->Id,
  			'UserId' =>$User->UserId,
  			'Name' =>$User->Name,
  			'Id_Roles' => $User->{'Id-Roles'},
  			'Message' => 'Login Success'
  		);
  		return json_encode($response);
  	}
  	else {
  			$response = array(
  			'Id' => '0',
  			'UserId' =>'0',
  			'Name' =>'0',
  			'Id_Roles' => '0',
  			'Message' => 'Username or Password mismatch.'
  		);
  		return json_encode($response);

  	}

  }

  function Login(Request $request) {

  	$UserId = $request->input('input_user_id');
  	$Password = $request->input('input_user_password');

  	$result = $this->LoginService($request);
  	echo "result of login : " . $result;
  	$messages = "Result : " . $result . " ";
 	return View('entry_main')->with('messages',$messages);
  }

  function UpdatePassword(Request $request) {
  	$UserId = $request->input('input_user_id');
  	$OldPassword =  MD5($request->input('input_user_pass'));
  	$NewPassword =  MD5($request->input('input_new_pass'));
  	
 	$finder = Users::where('UserId','=',$UserId)->where('Password','=',$OldPassword)->first();
  	if (count($finder) != 0) {
  		$tempUser = $finder;
  		$tempUser->Password = $NewPassword;
  		$tempUser->save();
  		$response = array(
  			'Id' => '1',
  			'Message' => 'Update Password Success'
  		);
  		return json_encode($response);
  	}
  	else {
  		$response = array(
  			'Id' => '1',
  			'Message' => 'User no Found'
  		);
  		return json_encode($response);
  	}
  }


  function Insert(Request $request) {
	  $tempUser = new Users;

	  $input = $request->all();
	  $tempUser->Name = 'Test nama';
	  $tempUser->UserId = $request->input('input_user_id');
	  $tempUser->Name = $request->input('input_user_name');
	  $tempUser->Password =  MD5($request->input('input_user_pass'));
	  $tempUser->{'Id-Roles'} = $request->input('select_user_role');
	  $tempUser->profile_picture = 0;

	  $finder = Users::where('UserId','=',$tempUser->UserId)->first();
	  if(count($finder) ==0 ){
		  $tempUser->save();
	 	  $message = "insert done";
	  }
	  else {
	  	  $message = "Record already Existed";
	  }


	  return Redirect("Users")->with('messages',$message);
  }

  function Delete($Id) {
	  $tempUser = Users::find($Id);
	  //echo $tempUser;
	  //when deleting student must before delete all attendance,
	  //delete all enrollments
	  $attendance  = Attendance::where('Id_Student','=',$tempUser->Id)->delete();
	  $enrollments = Enrollment::where('Id_User_Student','=',$tempUser->Id)->delete();
	  //if dosen
	  $classes = Classes::where('Id_User_Lecturer','=',$tempUser->Id)->delete();
	  $result  =  $tempUser->delete();



	  $messages = "User Entry Deleted";
	  echo $result;
	  return Redirect("Users")->with('messages',$messages);
  }



}

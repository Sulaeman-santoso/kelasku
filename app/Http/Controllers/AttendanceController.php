<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Schedule;
use App\Classes;
use App\Users;
use App\SchedulePhoto;
use App\SchedulePhotoDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller{


	public function DeleteAttendancesWeb(Request $request) {
		$id = $request->input('input_id');
		$result = $this->Delete($id);
		if ($result) {
			$message = "delete works";
		}
		else {
			$message = "delete fail";
		}
		return json_encode($message);
	}



	public function GetProfilePicture(Request $request) {
		//get profile picture path
		$user_id = $request->input('user_id');
		$getLatestPhoto = DB::table('photos')
						->join('attendances','attendances.Id_Photo','photos.Id')
						->where('attendances.Id_User_Student','=',$user_id)
						->first();

		if ($getLatestPhoto == null) {
			//kasih data boongan
		}
		return json_encode($getLatestPhoto);
	}


	public function GetLecturerKehadiran(Request $request,$viewId) {
	 //get data
	 //kudu simpen id nya di session bah.
	
	if ($request->session()->has('lecturer_id')) {
    	//
    	 $Id = $request->session()->get('lecturer_id');
	}
	else {
     $Id = 33;
 	}

	 $chosen_one = $Id;
	 
	 

	 $class_teached = DB::table('classes')
                ->join('lessons','classes.Id_Lesson','lessons.Id')
                ->join('semesters','lessons.Id_Semester','semesters.Id')
                ->join('subjects','lessons.Id_Subject','subjects.Id')
                ->select('classes.Id','subjects.SubjectName','classes.ClassNumber')
                ->where('Id_User_Lecturer','=',$Id)
                ->where('semesters.EndDate','>',NOW())
                ->get();

     $chosen_class = $request->input('select_kehadiran_class');


     if ($chosen_class != "") {
     	$schedules = DB::table('schedules')->
		where('Id_Class','=',$chosen_class)->get();
					  
		$student_enrolled = DB::table('enrollments')
		->join('users','enrollments.Id_User_Student','users.Id')
		->select('users.Id','users.UserId','users.Name')
		->where('enrollments.Id_Class','=',$chosen_class)
		->get();
     }
     else {
	 $schedules = DB::table('schedules')->get();
	 $student_enrolled = DB::table('enrollments')
		->join('users','enrollments.Id_User_Student','users.Id')
		->select('users.Id','users.UserId','users.Name')
		->get();
	}
	

	$meetingReport = DB::table('classes')
				->join('schedules','schedules.Id_Class','classes.Id')
				->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
				->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
				->join('lessons','classes.Id_Lesson','lessons.Id')
				->join('subjects','lessons.Id_Subject','subjects.Id')
				->join('semesters','lessons.Id_Semester','semesters.Id')
				->leftjoin('attendances','attendances.Id_Photo','schedule_photos_detail.Id_Photo')
				->leftjoin('users','attendances.Id_Student','users.Id')
				->where('classes.Id_User_Lecturer','=',$chosen_one)
				->where('semesters.EndDate', '>', NOW())
				->groupBy('classes.Id','classes.Id_User_Lecturer','subjects.SubjectName','classes.ClassNumber')
				->select('classes.Id as Id_Class','classes.Id_User_Lecturer as Lecturer',
						'subjects.SubjectName as MK','classes.ClassNumber',
				DB::raw('sum( !isnull(attendances.Id)) as TaggedFace,
					 sum( isnull(attendances.Id)) as unTaggedFace,
					 (sum( !isnull(attendances.Id)) + sum( isnull(attendances.Id))) as faceNum,
					 count(distinct schedules.Id) as jmlPertemuan'))
				->get()->toArray();

	  	//echo json_encode($meetingReport);
	 $resultArray = array();

  	foreach($meetingReport as $entry) {
		$temp["Id_Class"] = $entry->Id_Class;
		$temp["Lecturer"] = $entry->Lecturer;
		$temp["SubjectName"] = $entry->MK;
		$temp["ClassNumber"] = $entry->ClassNumber;
		$temp["NoOfUntaggedPhoto"] = $entry->unTaggedFace;
		$temp["NoOfTaggedPhoto"] = $entry->TaggedFace;
		$temp["TotalAverageEngagement"] = 0;
		$temp["TotalMeeting"] = $entry->jmlPertemuan;
		$temp["jmlMahasiswa"] = 0;
		$resultArray[$entry->Id_Class] = $temp;
	}

		//perlu satu query lagi buat cari jml orang yang perlu
		//echo json_encode($resultArray);

		$engagementsReport = DB::select('select c.Id,u.Name ,coalesce(sum(egg.Value),0) as engagementValue ,count(distinct sc.Id) as jmlSchedule, 
			( coalesce(sum(egg.Value),0)/count(distinct sc.Id)) as avgEngagement   from enrollments err 
			join users u on (err.Id_User_Student = u.Id)
			join classes c on (err.Id_Class =c.Id)
		    join schedules sc on (sc.Id_Class = c.Id)
		    join lessons l on (c.Id_Lesson = l.Id)
		    join subjects sb on (l.Id_Subject = sb.Id)
		    join semesters s  on (l.Id_Semester = s.Id)
		    left join engagements egg on ((egg.Id_Enrollment = err.Id) and (egg.Id_Schedule = sc.Id))
		    where ((s.EndDate > NOW()) and (c.Id_User_Lecturer = '. $chosen_one . '))
		    group by c.Id,u.Name,sb.SubjectName');

		foreach ($engagementsReport as $entry) {
			$temp = $resultArray[$entry->Id];
			$temp["TotalAverageEngagement"] += $entry->avgEngagement;
			$temp["jmlMahasiswa"] += 1;
			$resultArray[$entry->Id] = $temp;
		}
  	
		foreach ($resultArray as $entry) {
			$temp = $resultArray[$entry["Id_Class"]];
			if ($entry["jmlMahasiswa"] != 0){
				$temp["TotalAverageEngagement"] = 	$entry["TotalAverageEngagement"]/ $entry["jmlMahasiswa"];	
			}
			else {
				$temp["TotalAverageEngagement"] =0;
			}
			$resultArray[$entry["Id_Class"]] = $temp;
		}

		$returnArray = array();
		foreach ($resultArray as $entry){
			array_push($returnArray, $entry);
		}
		

		//schedulephooooootoooo should also be taken

		//cek if schedule and class exist then do things 
		$photoList = array();
		$scheduleList = array();
		$id_class = "";
		$id_schedule = "";

		if ($viewId == 1) {	
			
			$id_schedule = $request->input('filter_schedule');
			$id_class = $request->input('filter_class');

			//echo ("searching for class $id_class and schedule : $id_schedule");
			if (($id_class != "") && ($id_schedule!= "")) {
			 
			$student_enrolled = DB::table('enrollments')
			->join('users','enrollments.Id_User_Student','users.Id')
			->select('users.Id','users.UserId','users.Name')
			->where('enrollments.Id_Class','=',$id_class)
			->get();

			 $photoList = DB::table('schedules')
			->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
			->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
			->join('photos','schedule_photos_detail.Id_Photo','photos.Id')
			->leftjoin('attendances','attendances.Id_Photo','=','photos.Id')
			->leftjoin('users','attendances.Id_Student','=','users.Id')
			->select('attendances.Id','schedules.Id as Id_Schedules','schedule_photos.Id as Id_SchedulePhotos','photos.Path', 'attendances.Id_Student','users.UserId','users.Name','attendances.isValid','photos.Id as Id_Photos')
			->where (function($query) use ($id_schedule){
				$query->where('attendances.Id_Schedule','=',$id_schedule)
					->orWherenull('attendances.Id_Student');
			})
			->where('schedules.Id','=', $id_schedule)
			->where('schedules.Id_Class','=',$id_class)
			->paginate(12);
			
			}
			
		}
		else if ($viewId == 2) {
		
		$id_class = $request->input('filter_class_schedule');
		//print_r($request->post());
		$pagenumber = 5;
		 if ($id_class != "") {
	     	$scheduleList = DB::table('schedules')->
	     					join('classes','schedules.Id_Class','classes.Id')
	     					->join('lessons','classes.Id_lesson','lessons.Id')
	     					->join('subjects','lessons.Id_Subject','subjects.Id')
	     					->select('classes.Id as Id_Class','schedules.Id','subjects.SubjectName','classes.ClassNumber','schedules.ScheduleDate')
	     					->where('Id_Class','=',$id_class)
	     					->orderBy('Id','desc')
	     					->paginate($pagenumber);
	 	}
		 else {
		     $scheduleList = DB::table('schedules')->
	     					join('classes','schedules.Id_Class','classes.Id')
	     					->join('lessons','classes.Id_lesson','lessons.Id')
	     					->join('subjects','lessons.Id_Subject','subjects.Id')
	     					->select('classes.Id as Id_Class','schedules.Id','subjects.SubjectName','classes.ClassNumber','schedules.ScheduleDate')
	     					->orderBy('Id','desc')
	     					->paginate($pagenumber);
		 }
		}
	
	  return View('lecturer_kehadiran_view')->with('students',$student_enrolled)->with('schedulelist',$scheduleList)->with('scheduleid',$id_schedule)->with('classid',$id_class)->with('photolist',$photoList)->with('report',$returnArray)->with('classes',$class_teached)->with('view_id',$viewId)->with('schedules',$schedules);
	}


	public function GetAdv(Request $request) {
	  $id_class = $request->input('select_attendance_class');
	  $id_schedule = $request->input('input_attendance_IdSchedule');
			
	
      // cek if id_schedule is valid with the class
      $find = Schedule::Where('Id_Class','=',$id_class)->Where('Id','=',$id_schedule)->get();

      if (count($find) ==0 ) 
      {
      	 $id_schedule = '';
      }


	  if (($id_class!='') && ($id_schedule != '')) {
		  $temp = Attendance::where('Id_Schedule','=',$id_schedule)->paginate(12);
		  $photoList = DB::table('schedules')
  					->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
  					->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
  					->join('photos','schedule_photos_detail.Id_Photo','photos.Id')
  					->leftjoin('attendances','attendances.Id_Photo','=','photos.Id')
  					->leftjoin('users','attendances.Id_Student','=','users.Id')
  					->select('schedules.Id as Id_Schedules','schedule_photos.Id as Id_SchedulePhotos','photos.Path', 'attendances.Id_Student','users.UserId','users.Name','attendances.isValid','photos.Id as Id_Photos')
  					->where (function($query) use ($id_schedule){
  						$query->where('attendances.Id_Schedule','=',$id_schedule)
  							->orWherenull('attendances.Id_Student');
  					})
  					->where('schedules.Id','=', $id_schedule)
  					->where('schedules.Id_Class','=',$id_class)
  					->paginate(12);

	  	
		  $schedules = Schedule::Where('Id_Class','=',$id_class)->get();
		  //$classes = Classes::all()->sortBy('ClassNumber')->sortBy('Id_Lesson');
	 }	
	 else if ( $id_class != ''){
	 	$schedules = Schedule::Where('Id_Class','=',$id_class)->get();

	 	if (count($schedules) > 0){

	 	  $id_schedule =  $schedules->first()->Id;
 		  $temp = Attendance::where('Id_Schedule','=',$id_schedule)->paginate(12);
		  $photoList = DB::table('schedules')
  					->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
  					->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
  					->join('photos','schedule_photos_detail.Id_Photo','photos.Id')
  					->leftjoin('attendances','attendances.Id_Photo','=','photos.Id')
  					->leftjoin('users','attendances.Id_Student','=','users.Id')
  					->select('schedules.Id as Id_Schedules','schedule_photos.Id as Id_SchedulePhotos','photos.Path', 'attendances.Id_Student','users.UserId','users.Name','attendances.isValid','photos.Id as Id_Photos')
  					->where (function($query) use ($id_schedule){
  						$query->where('attendances.Id_Schedule','=',$id_schedule)
  							->orWherenull('attendances.Id_Student');
  					})
  					->where('schedules.Id','=', $id_schedule)
  					->where('schedules.Id_Class','=',$id_class)
  					->paginate(12);
		  }
		  else {
		  	$photoList = [];
		 	$schedules = Schedule::all();
			$temp =[];
		  }
	 }
	 else {		
		// $classes =  Classes::all()->sortBy('ClassNumber')->sortBy('Id_Lesson');
		 $photoList =DB::table('schedules')
					->join('classes','classes.Id','schedules.Id_Class')
					->join('lessons','lessons.Id','classes.Id_Lesson')
					->join('semesters','lessons.Id_Semester','semesters.Id')
  					->join('schedule_photos','schedule_photos.Id_Schedule', 'schedules.Id')
  					->join('schedule_photos_detail','schedule_photos_detail.id_schedulePhoto','schedule_photos.Id')
  					->join('photos','schedule_photos_detail.Id_Photo','photos.Id')
  					->leftjoin('attendances','attendances.Id_Photo','=','photos.Id')
  					->leftjoin('users','attendances.Id_Student','=','users.Id')
  					->select('schedules.Id as Id_Schedules','schedule_photos.Id as Id_SchedulePhotos','photos.Path', 'attendances.Id_Student','users.UserId','users.Name','attendances.isValid','photos.Id as Id_Photos')
  					->where('semesters.EndDate','>',NOW())
					->paginate(12); ;
		 $schedules = Schedule::all();
		 $temp = Attendance::paginate(12); 
	 }
	 //get non valid entry ...
	 $classes = Classes::whereHas('Lesson',function($query) {
		  $query->whereHas('Semester',function($query2) {
			  $query2->where('EndDate','>',NOW());
		  });
	  })->get()->sortBy('ClassNumber')->sortBy('Id_Lesson');
	 
	$nonvalidAttendance = Attendance::where('Id_Schedule','=',$id_schedule)->get();
	
	//echo "id schedule yang dicari : " . $id_schedule;
     return View('attendance_view_adv')->
		 with('nonvalidattendance',$nonvalidAttendance)->
		 with('photolist',$photoList)->
		 with('attendances',$temp)->
		 with('classes',$classes)->
		 with('schedules',$schedules)->
		 with('id_class',$id_class)->
		 with('id_schedule',$id_schedule);


  }


   public function Get(Request $request) {

    $id_class = $request->input('select_attendance_class');
    $id_schedule = $request->input('select_attendance_schedule');

    if (($id_class!='') && ($id_schedule!='')){
      //echo $id_class ."-". $id_schedule;
       $classes = Classes::whereHas('Lesson',function($query) {
		  $query->whereHas('Semester',function($query2) {
			  $query2->where('EndDate','>',NOW());
		  });
	  })->get();
      $schedules = Schedule::where('Id_Class','=',$id_class)->Get();
      $jumlah = Schedule::where('Id_Class','=',$id_class)->count();
      //  echo 'jumlah data schedule : ' . $jumlah;
      //$temp = Attendance::all();
	  //only active attendances
	  $temp = Attendance::whereHas('Schedule',function($q4) {
		  $q4->whereHas('Classes',function($q) {
			$q->whereHas('Lesson', function($q2) {
				$q2->whereHas('Semester',function($q3) {
					$q3->where('EndDate','>',NOW());
				});			
			});
		});
	  })->get();
      return View('attendance_view')->with('attendances',$temp)->with('classes',$classes)->with('schedules',$schedules)->with('chosen_class',$id_class)->with('chosen_schedules',$id_schedule);
    }

    //$classes = Classes::all();
     $classes = Classes::whereHas('Lesson',function($query) {
		  $query->whereHas('Semester',function($query2) {
			  $query2->where('EndDate','>',NOW());
		  });
	  })->get();
	$schedules = Schedule::all();
    // $temp = Attendance::all();
	//only active attendances
    $temp = Attendance::whereHas('Schedule',function($q4) {
		  $q4->whereHas('Classes',function($q) {
			$q->whereHas('Lesson', function($q2) {
				$q2->whereHas('Semester',function($q3) {
					$q3->where('EndDate','>',NOW());
				});			
			});
		});
	  })->get();
	//  echo "yang iniloggh mas bro";
	return View('attendance_view')->with('attendances',$temp)->with('classes',$classes)->with('schedules',$schedules);
  }


public function unTagService(Request $request) {
	$IdStudent = $request->input('input_attendance_IdStudent');
    $IdSchedule = $request->input('input_attendance_IdSchedules');
    $IdPhoto = $request->input('input_attendance_IdPhotos');

    $finder = Attendance::where('Id_Schedule','=',$IdSchedule)
                    ->where('Id_Student','=',$IdStudent)->first(); //find the attendance

    if (count($finder)) {
    	//exist
    	$Id = $finder->Id;
	  	$temp = Attendance::find($Id);
	    $temp->delete();

	    $returnObject = array(
                    "Id" => "1",
                    "Message" => "Attendance Cleared"
                );
        return json_encode($returnObject);
    }
    else {
  		$returnObject = array(
                    "Id" => "2",
                    "Message" => "Attendance Not Found"
                );
        return json_encode($returnObject);
    }
}



public function InsertBasicService(Request $request) {
    //cek dulu apakah udah ada entry di schedule yang sama dengan photo yang sama
   
    $IdStudent = $request->input('input_attendance_IdStudent');
    $IdSchedule = $request->input('input_attendance_IdSchedules');
    $IdPhoto = $request->input('input_attendance_IdPhotos');

   // print_r ($request->post()); //buat cek post parameter udah ok
   //cek apakah ada attendance dengan id schedule dan student yang bersangkutan   
    $finder = Attendance::where('Id_Schedule','=',$IdSchedule)
                        ->where('Id_Student','=',$IdStudent)->first();
    
    if (count($finder)){
        //sudah pernah absen
  	 	$returnObject = array(
                    "Id" => "3",
                    "Message" => "Attendance already registered"
                );
        return json_encode($returnObject);
    }

    $finder = Attendance::where('Id_Schedule','=',$IdSchedule)
                        ->where('Id_Photo','=',$IdPhoto)->first();
    $IsValid = 1;
    if (count($finder)){
        //berarti ada udah ada yang absenin
        if ($finder->Id_Student == $IdStudent){
             $returnObject = array(
                    "Id" => "2",
                    "Message" => "Duplicate Entry"
                );
            return json_encode($returnObject);
        }
        $IsValid = 0;
    }

    $temp = new Attendance;
    $temp->Id_Schedule = $IdSchedule;
    $temp->Id_Student = $IdStudent;
    $temp->Id_Photo = $IdPhoto;
    $temp->IsValid = $IsValid;
   	//$result = 1;
    $result = $temp->save();

    //uplod pp if has no pp
    $chosen = Users::find($IdStudent);
    if ($chosen->has_pp == 0) {
    	$chosen->profile_picture = $IdPhoto;
    }

    if ($result) {
    	$returnObject = array(
            "Id" => "1",
                 "Message" => "Attendance Recorded"
           	);
    }
    else {
    	$returnObject = array(
           "Id" => "1",
            "Message" => "Attendance Failed"
        );
    }

	AttendanceController::MarkedAttendanceInJSON($request,$IdSchedule,$IdPhoto,$IdStudent);
	return json_encode($returnObject);
  }

public static function MarkedAttendanceInJSON(Request $request, $idSchedule,$IdPhoto,$IdStudent) {
	  //find the json file
	  //is marked on i gotta do the same thing again
	  //$Name itu teh idstudent mas bro jadi cari dulu nrp dan namanya
	  $student =  Users::find($IdStudent);
	  
	  $destination_path = AttendanceController::getFileDestinationPath($idSchedule);
	  
	  /*
	  echo "destination_path" . $destination_path;
	  echo "<hr>";
	  echo "find id Photo :" . $IdPhoto;
      echo "<hr>";
	  */
	  $files = array_diff(scandir($destination_path), array('.','..'));
	  foreach ($files as $file) {
		  $ext = substr($file,-4);
		  
		  if (($ext == 'json') && ($file != 'meta.json')) {
			//if json
			$file_path = $destination_path . $file;  
			$file_string = file_get_contents($file_path);
			$json_result = json_decode($file_string);
		
			//echo ("$file_string");
			//print_r($json_result);
			//get array if array turns out to be tru ..then timpa.. else do nothing .. done
			$change = 0;
			
			foreach ($json_result as $entry) {
				if ($entry->Id_Photo == $IdPhoto) {
					$entry->NRP = $student->UserId;
					$entry->Name = "'". $student->Name . "'";
					$change = 1;
				}
			}
			if ($change == 1) {
			//	print_r($json_result);
				$fp = fopen($file_path, 'w');
				fwrite($fp, json_encode($json_result));
				fclose($fp);
			}
			
		  }
	 }
	  
	  //load it to array (remind me to add filename to json file )
	  //fill it with new data.
  }
  
  public static function loadAllJsonFileToArray($destination_path) {
	  $path = $destination_path;
	  
	  
  }
  
  public static function getFileDestinationPath($idSchedule) {
	  $query = DB::table('schedules')
			   ->join('classes','schedules.Id_Class','=','classes.Id')
			   ->join('lessons','classes.Id_Lesson','=','lessons.Id')
			   ->join('subjects','lessons.Id_Subject','=','subjects.Id')
			   ->join('semesters','lessons.Id_Semester','=','semesters.Id')
			   ->join('semester_names','semesters.Id_Semester','=','semester_names.Id')
			   ->where('schedules.Id','=',$idSchedule)
			   ->select('semesters.Year as Year','semester_names.Name AS Semester','subjects.SubjectName as Subject',
			   'classes.ClassNumber as ClassNum','schedules.ScheduleDate as Dates')
			   ->first();
     //->get('semesters.Year as Year','semester_names AS Semester','subjects.SubjectName as Subject','classes.ClassNumber as ClassNum','schedules.ScheduleDate as Dates');
			   
	 $semester_year = $query->Year;
	 $semester_name = $query->Semester;
	 $subject_name = $query->Subject;
	 $class_name = $query->ClassNum;
	 $sched_date = $query->Dates;
	 
     $prune_date = substr($sched_date,0,16);// should be deleted because a change of format
     //$prune_date =$sched_date;
    
	 $destination_path =  'public_images/'. $semester_year . '/'. $semester_name . 
	 '/'. $subject_name .'/'. $class_name .  '/'. $prune_date  .'/'; 
	 
	 return $destination_path;
  }
  

  public function Insert(Request $request) {
    $NRP = $request->input('input_attendance_IdStudent');
    echo "mencari user dengan NRP " . $NRP;
	echo "<hr>";
	try {
		
	$userData = Users::where('UserId','=',$NRP)->first();
   
	$IdStudent = 0;
	if (count($userData) > 0) {
		 echo " ditemukan user dengan nama : " . $userData->Name . "<hr>";
		$IdStudent = $userData->Id;
	}
	
	$IdSchedule = $request->input('input_hidden_idSchedule');
    $IdPhoto = $request->input('input_hidden_idPhotos');
	$checkbox = $request->input('input_attendance_without_picture');
	
	//echo $request;
	//print_r($request->input);
    echo "insert " . $IdStudent . " to attendance " . $IdSchedule . " for photos " . $IdPhoto;
	
	
    $finder = Attendance::where('Id_Schedule','=',$IdSchedule)
                        ->where('Id_Student','=',$IdStudent)->first();
    
    if (count($finder)){
        //sudah pernah absen
         $returnObject = array(
                    "Id" => "3",
                    "Message" => "Attendance already registered"
                );
		$messages = "Attendance has already registered";
		//return Redirect('Attendances')->with('messages',$messages);	
        //return json_encode($returnObject);
    }

	$IsValid = 1;
	if ($checkbox) {
		$temp = new Attendance;
		$temp->Id_Schedule = $IdSchedule;
		$temp->Id_Student = $IdStudent;
		$temp->Id_Photo = null;//photo special buat yang gak punya muka
		$temp->IsValid = $IsValid;
		
		$temp->save();
		$messages = 'attendance entry no picture ' . $checkbox;
		//return Redirect('Attendances')->with('messages',$messages);
	}

    $finder = Attendance::where('Id_Schedule','=',$IdSchedule)
                        ->where('Id_Photo','=',$IdPhoto)->first();
   
    if (count($finder)){
        //berarti ada udah ada 
        if ($finder->Id_Student == $IdStudent){
             $returnObject = array(
                    "Id" => "2",
                    "Message" => "Duplicate Entry"
                );
		  return Redirect('Attendances')->with('messages',$messages);
          //  return json_encode($returnObject);
        }
        $IsValid = 0;
    }

    $temp = new Attendance;
    $temp->Id_Schedule = $IdSchedule;
    $temp->Id_Student = $IdStudent;
    $temp->Id_Photo = $IdPhoto;
    $temp->IsValid = $IsValid;
    
    echo "<hr>";
    echo "Id_Schedule :" . $temp->Id_Schedule . " ";
    echo "Id_Student :" . $temp->Id_Student . " ";
    echo "Id_Photo :" . $temp->Id_Photo . " ";
    
    echo "<hr>";
    //$temp->save();
 

    $messages = 'attendance entry updated ' . $checkbox;
	  
	AttendanceController::MarkedAttendanceInJSON($request,$IdSchedule,$IdPhoto,$NRP);
	}
	catch (Exception $e){
		
		$messages = "Error" .  $e->getMessage();
	}
	
   // return Redirect('Attendances')->with('messages',$messages);

  }

  public function Delete($Id) {
    $temp = Attendance::find($Id);
    $temp->delete();

    $messages = 'attendance entry deleted';
    return Redirect('Attendances')->with('messages',$messages);
  }

  public function DeleteService($Id) {
    $temp = Attendance::find($Id);
    $temp->delete();

    $messages = 'attendance entry deleted';
    return json_encode($messages);
  }


}


?>
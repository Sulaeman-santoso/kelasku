<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'Users/Login',
        'Users/UploadPP',
        'Users/UpdatePass',
        'Classes/Filter',
        'Schedules/Filter',
        'Schedules/GetScheduleByClass',
        'Schedules/GetDetailPhoto',
        'Attendances/Insert',
        'Attendances/Delete/{Id}',
        'Attendances/DeleteAttendances',
        'Attendances/UntagAttendances',
        'Classes/FilterLecturer',
		'Classes/Dummy',
        'Schedules/InsertService',
        'SchedulePhotos/InsertService',
        'Enrollments/BulkImport',
        'Engagements/InsertService',
        'Engagements/FilterUserByClass',
        'Engagements/FilterScheduleByClass',
        'Engagements/GetEngagementService',
        'Engagements/GetSemesterReport',
        'Engagements/GetSemesterLecturerReport',
        'Engagements/GetEngagementByEnrollment',
        'Enrollments/GetEnrolledStudent',
        'Photos/DeleteWeb',
        'SchedulePhotos/InsertWeb',
        'Schedules/DeleteService',
        'Announcements/Insert',
        'Announcements/Delete',
        'UserTag/UnAssignTag',
        'UserTag/AssignTag',
        'UserTag/GetTag',
        'UserTag/GetTagPaginate',
        'Announcements/Filtered',


    ];
}

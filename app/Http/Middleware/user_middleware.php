<?php

namespace App\Http\Middleware;

use Closure;

class user_middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $Id = $request->session()->get('lecturer_id');
       if ($Id === null) {
         return redirect('/');
       }
       else {
        return $next($request);
      }
    }
}

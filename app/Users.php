<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticated;

class Users extends Authenticated
{
	
   protected $table= 'users';
   protected $primaryKey= 'Id';
   
    //
    function roles() {
      return  $this->belongsTo('App\Roles','Id-Roles','Id');
    }

    function profile_picture() {
    	return $this->belongsTo('App\Photos','profile_picture','Id');
    }

}

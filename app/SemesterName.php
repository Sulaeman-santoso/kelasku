<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SemesterName extends Model
{
	protected $tables = 'semester_names';
	protected $primaryKey = 'Id';
    //

	function semesters() {
	  return $this->hasManny('App\Semesters','Id_Semester','Id');
	}
}

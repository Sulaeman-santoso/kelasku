<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
	protected $table = 'semesters';
	protected $primaryKey = 'Id';
    //
	
	function semestername() {
		return $this->belongsTo('App\SemesterName','Id_Semester', 'Id');
	}
}

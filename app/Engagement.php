<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engagement extends Model
{
    //
	protected $table='engagements';
    protected $primaryKey = 'Id';
    
    public function engagement_type() {
	return $this->belongsTo('App\Engagement_Types','Id_Engagement_Type','Id');
    }

    public function enrollments() {
      return $this->belongsTo('App\Enrollments','Id_Enrollment','Id');
    } 
	
	public function schedules() {
      return $this->belongsTo('App\Schedules','Id_Schedule','Id');
    }
}

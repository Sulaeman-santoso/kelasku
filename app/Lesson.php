<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $table='lessons';
    protected $primaryKey = 'Id';
    //
    function semester() {
      return $this->belongsTo('App\Semester','Id_Semester','Id');
    }

    function subject() {
      return $this->belongsTo('App\Subject','Id_Subject','Id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    //
    protected $table='classes';
    protected $primaryKey = 'Id';

    function lesson() {
      return $this->belongsTo('App\Lesson','Id_Lesson','Id');
    }

    function lecturer() {
      return $this->belongsTo('App\Users','Id_User_Lecturer','Id');
    }
}

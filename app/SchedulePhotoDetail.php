<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchedulePhotoDetail extends Model
{
    //
    protected $table='schedule_photos_detail';
    protected $primaryKey = 'Id';

    function parentPhoto() {
      return $this->belongsTo('App\Photo','Id_SchedulePhoto','Id');
    }

    function photo() {
      return $this->belongsTo('App\Photo','Id_Photo','Id');
    }
}

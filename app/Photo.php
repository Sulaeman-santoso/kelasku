<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
  protected $table='photos';
  protected $primaryKey='Id';

  function photoDetail(){
    return $this->hasMany('App\SchedulePhotoDetail','Id_SchedulePhoto','Id');
  }

  function announcements() {
    return $this->belongsTo('App\Announcement','Id_Photo','Id');
  }
    //
}

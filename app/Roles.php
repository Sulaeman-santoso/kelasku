<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    //
   protected $table= 'roles';
   protected $primaryKey= 'Id';

   function user() {
     return $this->hasMany('App\Users','Id-Roles','Id');
   }
}

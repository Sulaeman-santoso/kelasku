<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
           Schema::create('user_tag', function (Blueprint $table) {
            $table->integer('Id_User')->reference('Id')->on('users');
            $table->integer('Id_Tag')->reference('Id')->on('tags');
            $table->timestamps();
            $table->primary(array('Id_User','Id_Tag'));
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('user_tag');
    }
}

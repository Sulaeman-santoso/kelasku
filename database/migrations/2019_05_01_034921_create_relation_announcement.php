<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationAnnouncement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('announcement_tag', function (Blueprint $table) {
            $table->integer('Id_Announcement')->reference('Id')->on('announcements');
            $table->integer('Id_Tag')->reference('Id')->on('tags');
            $table->timestamps();
            $table->primary(array('Id_Announcement','Id_Tag'));
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
          Schema::dropIfExists('announcement_tag');
    }
}

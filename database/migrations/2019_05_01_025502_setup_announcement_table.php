<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetupAnnouncementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    

        Schema::create('announcements', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('Id_User_Lecturer')->reference('Id')->on('users');
            $table->integer('Id_Photo')->reference('Id')->on('photos');
            $table->float('Photo_Ratio');//just in case dibutuhin
            $table->string('Title',250);
            $table->longText('Content');
            $table->dateTime('Publish_Date');
            $table->dateTime('Expire_Date');
            $table->integer('Id_Class')->reference('Id')->on('classes');
            $table->timestamps();
       
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        /*
        Schema::dropIfExists('announcements');/*
        Schema::dropIfExists('tags');
      
        */
    }
}

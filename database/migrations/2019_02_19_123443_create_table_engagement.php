<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEngagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('engagements',function(Blueprint $table) {
			$table->increments('Id');
			$table->integer('Id_Enrollment')->references('Id')->on('enrollments');
			$table->integer('Id_Schedule')->references('Id')->on('schedules');
			$table->integer('Ans_Right');
			$table->integer('Ans_Wrong');
			$table->integer('Asking');
			$table->integer('Interacting');
		});
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('engagements');
    }
}

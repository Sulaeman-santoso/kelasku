<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EngagementTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('engagement_types',function(Blueprint $table) {
			$table->increments('Id');
			$table->integer('engagement_name');
			$table->timestamps();
		});
		
		Schema::table('engagements', function($table) {
		  $table->integer('Id_Eng_Type')->references('Id')->on('engagement_types');
          $table->dropColumn('Ans_Right');
          $table->dropColumn('Ans_Wrong');
          $table->dropColumn('Asking');
          $table->dropColumn('Interacting');
		 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::dropIfExists('engagement_types');
    }
}

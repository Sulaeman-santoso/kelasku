//alert('hello  i\'m engaging you');

$(document).ready(function() {

	//do at start of page.. which is nothing 
	FilterSelect();

	$("#btnRight").click(function() {
		SubmitFormEngagement(0);
	});
	$("#btnWrong").click(function() {
		SubmitFormEngagement(1);
	});
	$("#btnAsk").click(function() {
		SubmitFormEngagement(2);
	});
	$("#btnAct").click(function() {
		SubmitFormEngagement(3);
	});


});


function SubmitFormEngagement(id_eng) {
	//isi id engagement
	$("#_id_eng").val(id_eng);
	var temp = $("#_id_eng").val();
	
}

function FilterSelect() {

	$('#select_engagement_classes').change(function() {
		//ajax for getting user filtered by class
		var result = $('#select_engagement_classes').val();
		
		//filter mahasiswa
		$.post("Engagements/FilterUserByClass",{
			"select_engagement_class": result
		},function(data,status) {
			
			var returnObject = "";
			var obj = JSON.parse(data);
		 	for(i=0;i<obj.length;i++) {
			 returnObject +=  "<option value=" + obj[i].Id +  " >"  + obj[i].UserId + "-" + obj[i].Name + " </option>";
		 	}
		 	$("#select_engagement_user").html(returnObject);
		});

		//filter schedule
		$.post("Engagements/FilterScheduleByClass",{
			"select_engagement_class": result
		},function(data,status) {
			
			var returnObject = "";
			var obj = JSON.parse(data);
		 	for(i=0;i<obj.length;i++) {
		 		
			 returnObject +=  "<option value=" + obj[i].Id +  " >"  + obj[i].scheduleDate + " </option>";
		 	}
		 	$("#select_engagement_schedule").html(returnObject);
		});

	});

}
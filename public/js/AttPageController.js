$(document).ready(function() {

	//initialize image picker 
	$("#picker_image2").imagepicker(

	);
	
	$("#picker_nonValid").imagepicker( {
	  hide_select : true,
	  show_label : true
	});

  $("#picker_image").imagepicker({
	  hide_select : true,
	  show_label : true
	});

 
   $("#btnAddAttendance").click(function() {
	 
	  //schedule dengan assumption bahwa uda bener.. ini get id_schedule
	  var idSchedule = $("#select_attendance_schedule").find(":selected").val();
	  $('#input_hidden_idSchedule').val(idSchedule);
	  
	  //photo .... ini get _id_photo
	  var idPhoto = $("#picker_image").find(":selected").val();
	  $("#input_hidden_idPhotos").val(idPhoto);

	  //submit to Add attendance
	  $('#AttendanceForm').submit();
   });
   
  //populate schedule
 // InitSchedule();
  AddScheduleChangeHandler();

  //populate userId 
  addAutoCompleteIdUser(token);

  //pick one either way
  photoPickerChange(0,token);
  
   $("#picker_image").imagepicker({
	  hide_select : true,
	  show_label : true
	});
	
//	$("#select_attendance_schedule").trigger("chosen:updated");
//	$("#select_attendance_schedule").trigger("change");
	
});


function InitSchedule() {
   
  var id = $("#select_attendance_class").find(":selected").val();  // Get Class ID
  var schedHandle = $("#select_attendance_schedule").find(":selected"); // Get  Schedule handler
  var id_sebelum = 0; // default nilai untuk schedule id 
  
  
  //alert("testing");
  if (schedHandle != undefined) {
	  id_sebelum = schedHandle.val(); // if not null then id_sebelum diisi  id schedule yang sebelumnya 
	  //alert(id_sebelum); //ini untuk tes nilai nya bener gak keambil ama nilai awal if any
  }
  
  var tokens = token; //token for laravel
  
  // Filter Schedule berdasarkan ID Class 
  $.post('Schedules/Filter/'+id,{
	Id : id,
	_token : tokens
  },function(data,status){
	  $('#select_attendance_schedule').html(data); // populate combo box schedule 
	  $("$select_attendance_schedule").val(id_sebelum); // isi index 
	  
	  var idSchedule = $("#select_attendance_schedule").find(":selected").val(); // ini binding hidden dengan idschedule
	  $('#input_hidden_idSchedule').val(idSchedule);
  });
}

function AddScheduleChangeHandler() {
  $("#select_attendance_schedule").change(function() {
	var idSchedule = $("#select_attendance_schedule").find(":selected").val();
	$("#input_hidden_idSchedule").val(idSchedule);
	//alert(idSchedule);
  });
}


//when picker change..
function photoPickerChange(value, token) {
	var idPhoto = $("#picker_image").find(":selected").val();
	$("#input_hidden_idPhotos").val(idPhoto);
	//alert('changed ' + idPhoto); 
}

function addAutoCompleteIdUser(token) {
	var idClass = $("#select_attendance_schedule").find(":selected").val(); 
	var tokens = token;
	$.post('Enrollments/GetEnrolledStudent',{
		 IdClass : idClass,
		_token : tokens
	},function(data,status) {
		 //alert(data);  
		 var obj = JSON.parse(data);
		 var list = [];
		 for(i=0;i<obj.length;i++) {
			 list.push(obj[i].UserId);
			// alert(i);
		 }
		 
		 $("#input_id_autocomplete").autocomplete({
          source : list
        });
	});
}



function filter(value,token) {
 	//$('#AttendanceForm').submit();
 // $("#btnAddAttendance").click();
 InitSchedule();

	/*
  var id = $("#select_attendance_class").find(":selected").val();
 var id_sebelum =$("#select_attendance_schedule").find(":selected").val();
 
  var tokens = token;
 
  $.post('Schedules/Filter/'+id,{
	Id : id,
	_token : tokens
  },function(data,status){
	  $('#select_attendance_schedule').html(data);
	$("#select_attendance_schedule").val(id_sebelum);
	  var idSchedule = $("#select_attendance_schedule").find(":selected").val();
	  $("#input_hidden_idSchedule").val(idSchedule);
	  //alert(idSchedule);
	//  getAttendancePhoto(0,tokens);
	  addAutoCompleteIdUser(tokens);  
  });*/
}
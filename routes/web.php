<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!|
*/

Route::get('/', 'PageController@GetMain');
Route::get('Student','PageController@GetStudentWeb');
Route::get('PrivacyPolicies','PageController@GetPrivacyPolicies');


Route::get('Client','PageController@GetClientWeb');
Route::post('Client','PageController@GetClientWeb');
//Route::get('Lecturer/{Id}','PageController@GetClientLecturer');


//Lecturer Web related routes
Route::get('Lecturer/{Id}','PageController@GetClientLecturer');
Route::get('Lecturer/Kehadiran','AttendanceController@GetLecturerKehadiran');
Route::get('Lecturer/Kehadiran/{viewId}','AttendanceController@GetLecturerKehadiran');
Route::get('Lecturer/Keaktifan/{viewId}','EngagementController@GetLecturerKeaktifan');
Route::get('Lecturer/Aktif/{viewId}','AttendanceController@GetLecturerKehadiran');
Route::get('Lecturer/Pengumuman/{viewId}','AnnouncementsController@GetLecturer')->middleware('user_mid');
Route::get('Lecturer/Pengaturan/{viewId}','PageController@GetLecturerSettings')->middleware('user_mid');

//Route::get('Lecturer/','AttendanceController@GetLecturerKehadiran');
Route::post('Announcements/Insert','AnnouncementsController@InsertService');
Route::post('Announcements/Delete','AnnouncementsController@DeleteService');
Route::get('Announcements','AnnouncementsController@Get');
Route::get('Announcements/Filtered','UserTagController@GetAnnByTag');

Route::post('UserTag/AssignTag','UserTagController@AssignTag');
Route::post('UserTag/UnAssignTag','UserTagController@UnAssignTag');
Route::get('UserTag/GetTag','UserTagController@GetTag');
Route::get('UserTag/GetTagPaginate','UserTagController@GetTagPaginate');

Route::get('Roles','RoleController@GetRoles');
Route::post('Roles/Insert','RoleController@Insert');
Route::post('Roles/Delete/{Id}','RoleController@Delete');
Route::post('Roles/Update/{Id}','RoleController@Update');

Route::get('Users','UserController@Get');
Route::post('Users/Import', 'UserController@Import');
Route::post('Users/Insert','UserController@Insert');
Route::post('Users/Delete/{Id}','UserController@Delete');
Route::post('Users/Update/{Id}','UserController@Update');
Route::post('Users/Login','UserController@LoginService');
Route::post('Users/AuthLogin','UserController@AuthLogin');
Route::post('Users/LoginWeb','UserController@Login');
Route::post('Users/UploadPP','UserController@UploadPP');
Route::post('Users/UpdatePass','UserController@UpdatePassword');


//we're here
Route::get('Subjects','SubjectController@Get');
Route::post('Subjects/Insert/','SubjectController@Insert');
Route::post('Subjects/Delete/{Id}','SubjectController@Delete');
Route::post('Subjects/Update/{Id}','SubjectController@Update');

Route::get('SemesterNames','SemesterNameController@Get');
Route::post('SemesterNames/Insert/','SemesterNameController@Insert');
Route::post('SemesterNames/Delete/{Id}','SemesterNameController@Delete');
Route::post('SemesterNames/Update/{Id}','SemesterNameController@Update');

Route::get('Semesters','SemesterController@Get');
Route::post('Semesters/Insert/','SemesterController@Insert');
Route::post('Semesters/Delete/{Id}','SemesterController@Delete');
Route::post('Semesters/Update/{Id}','SemesterController@Update');


Route::get('Lessons','LessonController@Get');
Route::post('Lessons/Insert/','LessonController@Insert');
Route::post('Lessons/Delete/{Id}','LessonController@Delete');
Route::post('Lessons/Update/{Id}','LessonController@Update');

Route::get('Classes','ClassController@Get');
Route::post('Classes/FilterLecturer','ClassController@GetClassServiceLecturer');
Route::post('Classes/Filter','ClassController@GetClassService');
Route::post('Classes/Insert/','ClassController@Insert');
Route::post('Classes/Delete/{Id}','ClassController@Delete');
Route::post('Classes/Update/{Id}','ClassController@Update');
Route::post('Classes/Dummy/','ClassController@GetClassServiceDummy');



Route::post('Schedules/GetDetailPhoto','ScheduleController@GetDetailPhotoService');
Route::post('Schedules/GetScheduleByClass','ScheduleController@GetScheduleByClass');
Route::post('Schedules/Filter','ScheduleController@GetService');
Route::get('Schedules','ScheduleController@Get');
Route::post('Schedules/Insert/','ScheduleController@Insert');
Route::post('Schedules/InsertService','ScheduleController@InsertService');
Route::post('Schedules/DeleteService','ScheduleController@DeleteService');
Route::post('Schedules/Delete/{Id}','ScheduleController@Delete');
Route::post('Schedules/Wipe','ScheduleController@WipeSchedule');
Route::post('Schedules/Update/{Id}','ScheduleController@Update');
Route::post('Schedules/Filter/{Id_Class}','ScheduleController@Filter');
Route::get('Schedules/Filter/{Id_Class}','ScheduleController@Filter');


Route::get('Photos','PhotoController@Get');
Route::post('Photos/Insert/','PhotoController@Insert');
Route::post('Photos/Delete/{Id}','PhotoController@Delete');
Route::post('Photos/DeleteWeb','PhotoController@DeleteWeb');
Route::post('Photos/Update/{Id}','PhotoController@Update');


Route::post('Attendances/GetProfilePicture','AttendanceController@GetProfilePicture');
Route::get('Attendances','AttendanceController@GetAdv');
//Route::get('Attendances','AttendanceController@GetAdv2');
Route::post('Attendances/Import','AttendanceController@Import');
Route::post('Attendances/InsertWeb','AttendanceController@Insert');
Route::post('Attendances/Insert/','AttendanceController@InsertBasicService');
Route::post('Attendances/Delete/{Id}','AttendanceController@Delete');
Route::post('Attendances/DeleteAttendances','AttendanceController@DeleteAttendancesWeb');
Route::post('Attendances/UntagAttendances','AttendanceController@unTagService');

Route::post('Attendances/DeleteService/{Id}','AttendanceController@DeleteService');

Route::post('Attendances/Update/{Id}','AttendanceController@Update');
//Route::get('Attendances', 'AttendanceController@Get');
Route::post('Attendance/GetDetectedFaces','AttendanceController@GetFaces');
Route::get('JsonUpdate/{idSchedule}/{idPhoto}/{Name}','AttendanceController@MarkedAttendanceInJSON');



Route::post('Enrollments/BulkImport','EnrollmentController@BulkImport');
Route::post('Enrollments/Import','EnrollmentController@Import');
Route::post('Enrollments/GetEnrolledStudent','EnrollmentController@GetEnrolledStudent');
Route::get('Enrollments','EnrollmentController@Get');
Route::post('Enrollments/Insert/','EnrollmentController@Insert');
Route::post('Enrollments/Delete/{Id}','EnrollmentController@Delete');
Route::post('Enrollments/Update/{Id}','EnrollmentController@Update');

Route::get('SchedulePhotos','SchedulePhotoController@Get');
Route::post('SchedulePhotos/InsertService','SchedulePhotoController@InsertService');
Route::post('SchedulePhotos/InsertWeb','SchedulePhotoController@InsertWeb');

Route::get('SchedulePhotos/temp/{Id}','SchedulePhotoController@InsertRevised');
Route::post('SchedulePhotos/Insert/','SchedulePhotoController@InsertRevised');
Route::post('SchedulePhotos/Delete/{Id}','SchedulePhotoController@Delete');
Route::post('SchedulePhotos/Update/{Id}','SchedulePhotoController@Update');
Route::post('SchedulePhotos/ShowPhoto/{Id}', 'SchedulePhotoController@ShowPhoto');


Route::get('Engagements','EngagementController@Get');
Route::get('EngagementLecturer','EngagementController@GetLecturer');
Route::get('EngagementStudent','EngagementController@GetStudent');


Route::post('Engagements/FilterUserByClass','EngagementController@FilterUserByClass');
Route::post('Engagements/FilterScheduleByClass','EngagementController@FilterScheduleByClass');
Route::get('Engagements/GetEngagementGlobal','EngagementController@GetEngagementGlobal');
Route::post('Engagements/GetEngagementService','EngagementController@getEngagementServiceClassSum');
Route::post('Engagements/GetEngagementByEnrollment','EngagementController@getEngagementByEnrollmentServiceSum');

Route::post('Engagements/GetSemesterReport','EngagementController@getSemesterEngagementReport');
Route::post('Engagements/GetSemesterLecturerReport','EngagementController@getSemesterLecturerReport');
Route::post('Engagements/GetSemesterLecturerReportWeb','EngagementController@getSemesterLecturerReportWeb');


Route::post('Engagements/InsertService','EngagementController@InsertService');
Route::post('Engagements/Insert','EngagementController@Insert');
Route::post('Engagements/Delete/{Id}','EngagementController@Delete');
Route::post('Engagements/Update','EngagementController@Update');

/*
Route::get('Announcements','AnnouncementController@Get');
Route::get('Announcements/Insert','AnnouncementController@Insert');
Route::get('Announcements/Delete/{Id}','AnnouncementController@Delete');
Route::get('AnnouncementLecturer','AnnouncementController@GetLecturer');
Route::get('Announcements/InsertService','AnnouncementController@InsertService');
Route::get('Announcements/DeleteService','AnnouncementController@DeleteService');
*/

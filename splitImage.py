import sys
import cv2
import numpy as np
import os
import json
import mysql.connector

CASCADE_DIR = "../haarcascades"
FACE_CLSF="haarcascade_frontalface_alt2.xml"
ROOT_DIR = "laravel/public/public_images/"
OUTPUT_DIR = "output/"
PUBLIC_OUTPUT_DIR = "/output/"

'''connection initialization '''
mydb = mysql.connector.connect(
    host='localhost',
    user='testUser',
    passwd='bose12345*',
    database='IBDB'
)

def detect_faces(filepath, classifier,file_name, id_schedPhoto, scaleFactor=1.1, minNeighbors=5):
    '''Detect faces in an image and save the detection as json file'''
    public_filepath = "http://124.81.122.93/laravel/public/" + filepath
    local_filepath = "/var/www/html/public/" + filepath

    # load image file
    #print ("\nfile_to_read "  + filepath + file_name)
    file_total_path =  '../public/' + filepath + file_name;

    if(os.path.isfile(file_total_path)):
        print('\nfile found : ' + file_total_path)
    else :
        print('\nfile not found :  ' + file_total_path)

    img = cv2.imread(file_total_path)

    # convert image to grayscale
    gray_img = cv2.cvtColor(src=img, code=cv2.COLOR_BGR2GRAY)

    # detect faces
    faces = classifier.detectMultiScale(image=gray_img,
                                        scaleFactor=scaleFactor,
                                        minNeighbors=minNeighbors)

    #harus mengubah filepath jadi public_filepath
    (name,delimit,ext) = file_name.rpartition('.')

    print('\npublic_filepath ' + public_filepath);
    print('\nlocal_filepath ' + local_filepath);


    # record detected face areas into a list of dictionary
    faces_to_json = []
    i=0
    for (x, y, w, h) in faces:
        cropped = img[y:y+h, x:x+w]
        i = i+1
        output_path =  '../public/' + filepath +  OUTPUT_DIR + name + '-' + str(i) + '.jpg'

        if (not os.path.isdir('../public/' + filepath +  OUTPUT_DIR)):
            os.mkdir('../public/' + filepath +  OUTPUT_DIR) #create directory output apabila diperlukan

        #print ('output path : ' + output_path)
        cv2.imwrite( output_path, cropped) #cropped image saved to output_folder

        '''this needs to be checked ini buat ngesql photo and schedulephoto'''
        output_path =public_filepath + OUTPUT_DIR + name + '-' + str(i) + '.jpg' #diset ulang untuk file output

        my_cursor = mydb.cursor()
        sql ="insert into photos (Path) values('"+ output_path +"')"
        print sql
        my_cursor.execute(sql)#execute insert photo

        sql="insert into schedule_photos_detail (Id_SchedulePhoto, Id_Photo) values( '"+id_schedPhoto+ "','" + str(my_cursor.lastrowid)   +  "' )"
        print sql
        my_cursor.execute(sql)

        '''   '''

        rect = {'color': '#00ff00', 'x': int(x), 'y': int(y),
                'width': int(w), 'height': int(h)}
        faces_to_json.append(rect)
    # save detected face areas as json file
    with open( '../public/' + filepath  + name + '.json', 'w') as outfile:
        json.dump(obj=faces_to_json, fp=outfile, indent=4)

    #create meta file
    sql = "select sj.SubjectId, sj.SubjectName, c.ClassNumber from classes c \
	   join lessons l on (c.Id_Lesson = l.Id) \
       join subjects sj on (l.Id_Subject = sj.Id)\
	   join schedules s on (s.Id_Class = c.Id)\
       join schedule_photos sp on (sp.id_Schedule = s.Id) where sp.Id=" + id_schedPhoto
    print sql
    my_cursor.execute(sql)
    myresult = my_cursor.fetchone()
    meta = {'SubjectId': myresult[0] , 'SubjectName':myresult[1] ,'ClassNumber': myresult[2]}
    with open( '../public/' + filepath + 'meta.json', 'w') as outfile:
        json.dump(obj=meta, fp=outfile, indent=4)

    mydb.commit();
    my_cursor.close()

def main():
    '''main function'''
    arg_length = sys.argv
    destinationPath = sys.argv[1];
    file_name = sys.argv[2];
    idSchedulePhoto = sys.argv[3];

    print 'destination path : ' +  destinationPath + "\n filename path :  " + file_name + "\n"
    if (os.path.isfile(os.path.join(CASCADE_DIR, FACE_CLSF))):
        print('classfier found')
    face_classifier = cv2.CascadeClassifier(os.path.join(CASCADE_DIR,
                                                         FACE_CLSF))
    detect_faces(destinationPath, face_classifier,file_name, idSchedulePhoto)

    mydb.close()

if __name__ == "__main__":
    main()
